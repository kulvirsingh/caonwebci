<?php

class Pages extends CI_Controller {

	public function index() {

		$data['title'] = "Chartered accountants, Company secretaries, Tax Experts: CAONWEB";
		$data['countries'] = $this ->Expert_Search_Model->get_countries();
		$data['services'] = $this ->Expert_Search_Model->default_services();
		$data['service'] = $this ->Expert_Search_Model->default_service();
		$data['currentcountrys'] = $this->Allexpert_Model->get_currentcountry();
		$this-> load ->view("include/header");
		$this-> load ->view("pages/index",$data);
		$this-> load ->view("include/index-footer");
	}

	/*public function ExpertSerch($loadMore) {
		
		$this->Expert_Search_Model->get_expert($loadMore);
	}*/

	public function ExpertSearch() {
		
		$data['sponsoredExpert'] = $this->Expert_Search_Model->get_sponsored_expert();
		$data['countries'] = $this ->Expert_Search_Model->get_countries();
		$data['services'] = $this ->Expert_Search_Model->get_services();
		$data['experts'] = $this->Expert_Search_Model->get_expert();
		
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-search.php", $data);
		$this-> load ->view("include/expert-search-footer");
	}

	public function expertDetails($id) {
		//echo 'hello world '. $slug . ' ' . $id;
		$data['experts'] = $this->Expert_Search_Model->get_expert_by_id($id);
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-details", $data);
		$this-> load ->view("include/footer");
	}

	public function contact() {

		$this-> load ->view("include/header");
		$this-> load ->view("pages/contact");
		$this-> load ->view("include/footer");
	}

	public function expertTerms() {
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-terms-service.php");
		$this-> load ->view("include/footer");
	}

	public function expertPolicy() {
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-privacy-policy.php");
		$this-> load ->view("include/footer");
	}

	public function about() {
		$this-> load ->view("include/header");
		$this-> load ->view("pages/about.php");
		$this-> load ->view("include/footer");
	}

	public function expertBooking() {
		$this->Expert_Search_Model->submitBooking();
	}
	
	public function topexpertindia() {
		
		$data['sponsoredexpert'] = $this->Allexpert_Model->sponsored_expert();
		
		$data['countries'] = $this ->Expert_Search_Model->get_countries();
		
		$data['allservices'] = $this ->Allexpert_Model->get_allservices();
		
		$data['allexpertse'] = $this->Allexpert_Model->get_allexpertss();
		
		$this-> load ->view("include/header");
		$this-> load ->view("pages/top-expert-india.php", $data);
		$this-> load ->view("include/expert-search-footer");
	}

	public function expertbycountry() {
		
		$page_url = $_SERVER['REQUEST_URI'];
		
		//echo $page_url ;
		
		$page_title = explode('/', $page_url);

        $page_title = $page_title[sizeof($page_title)-1];

		$page_title = rtrim($page_title, ".html");
		
		$page_title = str_replace('-', ' ', $page_title);
		
		//echo $page_title ;
		
		 $data['findexpertbycountery'] = $this->Allexpert_Model->get_expertbycountry($page_title);
		 
		$data['sponsoredexpert'] = $this->Allexpert_Model->sponsored_expert();
		
		$data['countries'] = $this ->Expert_Search_Model->get_countries();
		
		$data['allservices'] = $this ->Allexpert_Model->get_allservices();
		
		$data['allexpertse'] = $this->Allexpert_Model->get_allexpertss();
		
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-by-country.php",$data);
		$this-> load ->view("include/expert-search-footer");
	}
	
	
		public function expertbyservice() {
			
		$page_url = $_SERVER['REQUEST_URI'];
		
		//echo $page_url ;
		
		$page_title = explode('/', $page_url);

        $page_title = $page_title[sizeof($page_title)-1];

		$page_title = rtrim($page_title, ".html");
		
	    $page_title = str_replace('-', ' ', $page_title);
		
		$data['allservices'] = $this ->Allexpert_Model->get_allservices();
		$data['sponsoredexpert'] = $this->Allexpert_Model->sponsored_expert();
		
		$data['findexpertbyservice'] = $this->Expert_Search_Model->get_expertbyservice($page_title);
		
		$data['expertservice'] = $this->Expert_Search_Model->get_expert_service();
		$data['expertcity'] = $this->Expert_Search_Model->get_expert_citys();
		$this-> load ->view("include/header");
		$this-> load ->view("pages/expert-by-service.php", $data);
		$this-> load ->view("include/expert-search-footer");
	}
	
	
	
		public function addreview($id) {
		
		$slug='';
		//echo 'hello world '. $slug . ' ' . $id;
		
		$data['experts'] = $this->Expert_Search_Model->get_expert_by_id($id);
		
		$data['title'] = "Chartered accountants, Company secretaries, Tax Experts: CAONWEB";
		$this-> load ->view("include/header");
		$this-> load ->view("pages/add-review.php", $data);
		$this-> load ->view("include/expert-search-footer");
	
	}
	

}









