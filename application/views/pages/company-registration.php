
    <!-- End Navigation -->

<div class="clearfix"></div>
<section class=" landing-page">
		<div class="landing-page-bg">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-sm-6">
							 <h1>Company Registration</h1>
							 <p>The first & foremost requirement of start-up registration is to choose the favorable business structure and also to register a company name in India.</p>
							<div>
								<ul>
									<li><img src="<?= base_url();?>images/checked.png">Private Limited Company</li>
									<li><img src="<?= base_url();?>images/checked.png">One Person Company</li>
									<li><img src="<?= base_url();?>images/checked.png">Limited Liability Partnership</li>
									<li><img src="<?= base_url();?>images/checked.png">Partnership Firm</li>
									<li><img src="<?= base_url();?>images/checked.png">Proprietorship Firm</li>
									<li><img src="<?= base_url();?>images/checked.png">MSME Registration</li>
								</ul>
							
						</div>
						</div>
						
						<?php include APPPATH.'/views/include/submit-form.php';?>

					</div>
				</div>
			</div>
		</div>
		   </section>
    
     
		 
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Company Registration – How to Register a Company in India</h2>
						 
						<p>The first & foremost requirement of start-up registration is to choose the favorable business structure and also to register a company name in India. The choice of right business structure will impact many factors from your business name, to your liability towards business, to how you file your taxes & statutory dues. The operational and financial success of the company also depends on the business structure, accordingly online company registration in India process can be followed.</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>How Do We Help With Company Registration</h2>
						 
						<p>We, CAONWEB is providing the online CA directory for start-up registration or say online company registration in India through professional like CA’s, CS’s and Lawyers as a ONE-STOP solution for all pre & post incorporation compliances & licensing requirements. The company registration process is cumbersome which involves legal obligation & can be compiled properly with the help of experienced professionals. Promisingly, we are providing options of the best company Registration Consultants in India who can help you in the online firm registration process in India, Company Incorporation, start-up registration , proprietorship firm registration, online company registration in India, etc.</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Online company registration in india</h2>
						 
						<p>CAONWEB has developed the digital platform i.e. online service marketplace which serves as the online directory where one can find the professional according to their requirements & budget for the online company registration in India and also for start-up registration. CAONWEB provides you the option to answer your questions like how to register a company in India? Or how to do startup registration? Or how to avail the services like company Incorporation in India?</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Startup Registration</h2>
						 
						<p>startups which is also one of the significant reason behind the increasing number of startup registration in India. We, CAONWEB is providing the online directory where you can search the chartered accountant, company secretaries, tax consultants, business consultants, etc to cater to the needs of proprietorship firm registration, startup registration. The verified profiles of the professionals registered on our digital platform are ready to serve you & answer all your queries related to startup registration, online company registration in India and also how to register a company in India?</p>
						
						<p>The cost of company incorporation depends on two factors like which type of company you are getting registered. Start a Company & get online company registration in India with experts of CAONWEB with competitive Company Registration Services & fees, who can also help you in the register a company name in India & startup registration or online company registration in India</p>
						
					</div>
					
				</div>
			</div>
			 	 
		<div style="clear:both;"></div>		 
				 
				 <section class="third-section">
					<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Documents Required for Company Registration</h2>
					<ul>
								<li><img src="<?= base_url();?>images/tick.png ">Copy of PAN Card of directors</li>
								<li><img src="<?= base_url();?>images/tick.png ">Passport size photograph of directors</li>
								<li><img src="<?= base_url();?>images/tick.png ">Copy of Aadhaar Card/ Voter identity card of directors</li>
								<li><img src="<?= base_url();?>images/tick.png ">Copy of Rent agreement (If rented property)</li>
								<li><img src="<?= base_url();?>images/tick.png">Electricity/ Water bill (Business Place)</li>
								<li><img src="<?= base_url();?>images/tick.png">Copy of Property papers(If owned property)</li>
								<li><img src="<?= base_url();?>images/tick.png">Landlord NOC (Format will be provided)</li>
							</ul>
						</div>
						
							<div class="col-md-6">
							 
							 <iframe width="100%" height="350" src="https://www.youtube.com/embed/MnvzaRNTsqQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe> 	 
						</div> 	
						
					</div>
					</div>
				 </section>
			 	    
    
 
    
    
			 	 <div style="clear:both;"></div> 	 	 
		
    
    <section class="pricing py-5">
  <div class="container">
    <div class="row">
	<div class="col-md-12">
	<h2>Requirements for Start Up Incorporation</h2>
	</div>
      
      <div class="col-lg-3">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
          
            <h6 class="card-price text-center">One Person Company</h6>
            <hr>
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Shareholders</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Directors</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Nominee</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>Minimum Authorised Share Capital 1 Lakh</li>
						 
						</ul>
			 <a href="https://www.caonweb.com/one-person-company.php" target="_blank" title="One Person Company Registration Process" class="btn btn-block btn-primary text-uppercase">Read More</a>
			 
			 
			 
			 
             
          </div>
        </div>
      </div>
      <!-- Plus Tier -->
      <div class="col-lg-3">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
           <h6 class="card-price text-center">Private Limited Company</h6>
            <hr>
			
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span> 2 Shareholders</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> 2 Directors</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Minimum Authorised Share Capital 1 Lakh</li>
					 
						
						</ul>
			<a href="https://www.caonweb.com/limited-liability-partnership.php" target="_blank" title="Online Private Limited Company Registration - Fees, Documents & Procedure" class="btn btn-block btn-primary text-uppercase">Read More</a>
			 
			 
             
          </div>
        </div>
      </div>
      <!-- Pro Tier -->
      <div class="col-lg-3">
        <div class="card">
          <div class="card-body">
           <h6 class="card-price text-center">Limited Liability Partnership</h6>
            <hr>
			
			
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span>2 Designated Partners</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>Capital Contribution 10000/-</li>
						 
						</ul>
			<a href="https://www.caonweb.com/limited-liability-partnership.php" target="_blank" title="Limited Liability Partnership (LLP) Registration in India" class="btn btn-block btn-primary text-uppercase">Read More</a>
			
			
			 
             
          </div>
        </div>
      </div>
	  
	   <div class="col-lg-3">
        <div class="card">
          <div class="card-body">
           <h6 class="card-price text-center">Proprietorship</h6>
            <hr>
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span> PAN</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Adhaar</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Bank Details</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Business Details</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Investment minimum 1 Lakh</li>
						 
						</ul>
			 
            <a href="https://caonweb.com/proprietorship.php" target="_blank" title="How to Register a Proprietorship Firm in India" class="btn btn-block btn-primary text-uppercase">Read More</a>
          </div>
        </div>
      </div> 
	  
	  
	  
    </div>
  </div>
</section>
    
     
				 <section class=" fifth-section">
					<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>How to register a company in india</h2>
					<ul>
								<li>
									<span>1</span>
										<h6>Name approval</h6>
										<p>The first step of incorporating the company is to decide about the name of your company which will be applied to MCA for approval. Names given for approval should ideally be unique and related to business activities of the company.</p>
								</li>
								<li>
									<span>2</span>
										<h6>Application of DSC & DPIN</h6>
										<p>The next step for startup registration is to apply for Digital signature and DPIN. A digital signature is an online signature used for signing the e-forms and DPIN refer to Director's identification number issued by Registrar.</p>
								</li>
								<li>
									<span>3</span>
										<h6>MOA & AOA submission</h6>
										<p>Once the name is approved, Memorandum of association and Articles of Association needs to be prepared which includes the rules & by-laws of the company that regulates the operation of the business. Both MOA and AOA are filed with the MCA with the subscription statement for the authentication & approval.</p>
								</li>
								
								<li>
									<span>4</span>
										<h6>Prepare form & documents</h6>
										<p>Fill the application forms duly, attach the documents, get the same verified by professional then file the form to ROC then make the payment.</p>
								</li>
								
								<li>
									<span>5</span>
										<h6> Get incorporation certificate</h6>
										<p>Once all the documentation is done & form is filed with the department, registrar issues the incorporation certificate. The certificate of incorporation mentions significant information about the company such as CIN number, the name of the company, date of incorporation, etc.</p>
								</li>
								
								<li>
									<span>6</span>
										<h6>Apply for Bank account</h6>
										<p>After the receipt of incorporation certificate, you can submit the copy of Incorporation certificate, MOA, AOA and PAN along with the account opening form of the bank to open your bank account.</p>
								</li>
								 
								
								 
 							</ul>
						</div>
						 
						
					</div>
					</div>
				 </section>
    
     <div style="clear:both;"></div> 	 
    
    
    <section class="bg-grey faq">
    
	
		<div class="container">
		<h2>Frequently Asked Questions</h2>
		 
			 <div class='animatedParent' data-sequence='500'>
				<div class='  animated bounceInLeft slower'   data-id='1'>
				<div class="col-md-6">
					<!-- Tab -->
					<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
									What Is The Minimum Requirement Of Director For One Person Company Registration?
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The minimum requirement of directors for a one person company is one (1), additionally, there is a requirement of one member &amp; nominee.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									Which Form Of Entity Is Favourable To Start A New Business? 
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true" style="">
								<div class="panel-body">
									<p>There are various types of business entities in India such as sole proprietorship, partnership firms, Co-operative Societies, Companies, etc. The choice of suitable business structure for any particular business depends on factors like ownership, availability of capital resources, the scale of business, etc.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									How &amp; Where I Have To Apply For Online Company Registration In India?
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The application has to be submitted online on the department portal in the prescribed format along with the applicable documents. In case of any discrepancy or approval, you will get the email from the department.</p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
					
				</div><!-- Column -->
				</div><!-- Column -->
				<div class='  animated bounceInRight slower' data-id='2'>
				<div class="col-md-6 margin-top-991-30" >
				
					<!-- Tab -->
					<div class="panel-group accordion dark" id="accordion2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse5" aria-expanded="false" aria-controls="collapse5" class="collapsed">
									What Are The Documents Required For Online Company Registration In India? 
									</a>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The significant documents required for company registration are Pan Card &amp; aadhar card of directors, passport size photos of directors, conveyance deed or rent agreement, utility bills etc. along with the prescribed documents which can be prepared with the help of professional.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse6" aria-expanded="false" aria-controls="collapseTwo">
									Can Person Resident Outside India/ NRI/ Foreigner Become A Director In Private Limited Company?
									</a>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6" aria-expanded="true" style="">
								<div class="panel-body">
									<p>Yes, there is no restriction on person resident outside India/ NRI or Foreign National for becoming a Director in a Private Limited Company but at least one Director in the Board must be an Indian Resident.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading7">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse8" aria-expanded="false" aria-controls="collapseThree">
								Can I Incorporate Company By Myself? 
									</a>
								</h4>
							</div>
							<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7" aria-expanded="false">
								<div class="panel-body">
									<p>Yes, you can complete all the documentation of the company by yourself, but it is recommended to take help of professional as verification of the chartered accountant or company secretary is mandatory for certifying that all the required compliances have been made. The professional can also help you in post compliances of <strong>startup registration</strong> or <strong>online company registration in India.</strong></p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
				</div><!-- Column -->
				</div><!-- Column -->
				</div><!-- Column -->
			</div><!-- Row -->
		
	 
    </section>
    
    
    
    
 
