<!-- start of filter section -->

<div id="navbar">
	<div class="container">
		<div class="expert-filter">
			<div class="location-icon">
				<button type="button" onclick="get_Location();"><img src="<?= base_url();?>/assets/images/detect-location.png"></button>
			</div>
			<div class="filter-label">
			City:
			</div>
  			<div class="filter-box">
	  			<select id="selCity" name="city" onchange="showServicesByCity(this.value)">
	  				<!-- <?php foreach ($countries as $country) :?>
	  					<option value="<?=$country['country_id']?>"><?=$country['country']?></option>
	  				<?php endforeach?> -->
	  				<option>select city</option>
	  				<option value="Noida">Noida</option>
	  				<option value="Delhi">Delhi</option>
	  				<option value="Mumbai">Mumbai</option>
	  				<option value="Jaipur">Jaipur</option>
	  			</select>
  			</div>
		</div>

		<div class="expert-filter">
	  		<div class="filter-label">
				Services:
			</div>
	  		<div class="filter-box" id="service">
	  			<select id="selUser" onchange="showServicesByService(this.value)">
				
	  				<?php 
	  					$defaultService = $_GET['services'];
	  					echo '<option value="'. $defaultService . '">' . $defaultService . '</option>';
	  				?>
					
	  				<?php foreach ($allservices as $service) :?>
					
	  					<option value="<?=$service['service_name']?>"><?=$service['service_name']?></option>
	  				<?php endforeach?>
	  			</select>
	  		</div>
  		</div>
	</div>
</div>

<!-- end of filter section -->

<div class="container found">
	<div class="row">
		<div class="col-md-12">
			<p> Matches found for : Expert in
			<?php 
			$page_url = $_SERVER['REQUEST_URI'];
		    $page_title = explode('/', $page_url);
            $page_title = $page_title[sizeof($page_title)-1];
            $page_title = rtrim($page_title, ".html");
		    $url_service = str_replace('-', ' ', $page_title);
			?>  
			<strong><? //=$url_country ?></strong> for 
			<strong><?= $url_service?></strong></p>
		</div>
	</div>
</div>

<div class="container mb-30">
	<div class="row">
		<div class="col-md-9 main-div1">
				
		<div id="display"></div>
		
		<!-- start of sponsored section -->
<?php foreach($sponsoredexpert as $sE) :?>
		<div class="col-md-12 expert-section">
			<?php
				$fullName = $sE['name'] . $sE['last_name'];
				$slug = strtolower(url_title($fullName));
				$services = explode(',', $sE['service']);  
			?>
			<?php 
				$expertProfilePic = $sE['profile_photo'];
				$expertGender = strtolower($sE['gender']);
				if(empty($expertProfilePic)) {
					if($expertGender == 'male'){
						$expertProfilePic = 'avatar_default_male.png';
					}else{
						$expertProfilePic = 'avatar_default_female.png';
					}	
				}
			?>
			<div class="col-md-1 expert-pics new-pad-0">
											<img src="<?= base_url();?>profilepic/<?= $expertProfilePic;?>">
									</div>
			<div class="col-md-8 expert-details">
				<h3><?=$sE['name'] . $sE['last_name']?></h3>
				    <p><?=$sE['eduction']?></p>
					<p><?=$sE['experience']?> Years Exp.</p>
				
				
				<div style="clear:both;"></div>
				<div class="client-feild">
												<ul>
												<li>FDI Compliance</li>
												<li>Company Formation Registration</li>
													<!--<li style="background: none;"><a href="">View all 47 services</a></li>-->
					</ul>
				</div>
				<div class="expert-about">
				     <?php if($sE['summary'] != null) :?>
				     	<p> <?=substr($sE['summary'], 0, 100)?><a href="expert/<?= $slug . '/' . $sE['id'] ?>" target="_blank" class="view-new-profile"> Read More</a></p>
					 <?php endif?>
					
				</div>
			</div>
			<div class="col-md-3 rhs-details">
				<p><i class="fas fa-map-marker-alt"></i><?=$sE['city'] . ' , ' . $sE['state'] . ', '. $sE['country']?></p>
					<p><a href="expert/<?= $slug . '/' . $sE['id'] ?>" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i>  2 Feedback </a></p>		
			
				<p><a href="https://www.caonweb.com/add-review.php?id=<?=$sE['id']?>" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i> Add Reveiw </a></p>
							
						 
					<hr style="background-color: #f0f0f5;height: 0px; margin: 16px 0px 16px 0px;">
							<!--<h4>Prime <i class="fas fa-check"></i>
								<span>Max. 15 mins wait + Verified details</span>
							</h4>-->
			</div>
			<div class="col-md-12 fl-right-new">
				<div class="col-md-4 fl-left-new" style="width: auto;">
				<span class="badge badge-pill badge-secondary text-left">Sponsored</span>
				</div>
			<a href="<?= base_url();?>expert/<?= $slug . '/' . $sE['id'] ?>" target="_blank" class="view-new-profile">View Profile</a>
				 
				<button type="button" class="btn btn-success po" data-toggle="popover" title="" data-content="Contact-Now - <?=$sE['phone']?>" data-original-title="Ext. No : <?=$sE['id']?>">Contact Now</button>
			 
				<a href="#small-dialog" data-toggle="modal" data-target=".query-ser" class="appoint" type="button" data-id="<?=$sE['id']?>" id="bookid" onclick="bookNow($(this).attr('data-id'))">Book Appointment</a>
			</div>
		</div>
<!-- start of sponsored booking section -->

		<div class="col-md-12 booking-section" id="dynamic-book<?=$sE['id']?>">
			<span class="successField<?=$sE['id']?>"></span>
			<h4>Please fill up the below details to confirm the appointment.</h4>
			<form class="contact-form myform" name="RegForm<?=$sE['id']?>" id="RegForm<?=$sE['id']?>" data-id="<?=$sE['id']?>" action="" onsubmit="return bookingformValidation($(this).attr('data-id'))" method="post">  
		    <div class="input-group">
				 <input type="hidden" name="exp_id" value="<?=$sE['id']?>">
				 <span><i class="fa fa-user" aria-hidden="true"></i></span>
				 <input type="text" size=65 name="name" placeholder="Name">
				 <span class="nameField<?=$sE['id']?>"></span>
			</div>   
			<div class="input-group">
				<span><i class="fa fa-envelope" aria-hidden="true"></i></span>
			    <input type="text" size=65 name="email" placeholder="email">
			    <span class="emailField<?=$sE['id']?>"></span>
			</div>
			<div class="input-group">
				<span><i class="fa fa-phone" aria-hidden="true"></i></span>
				<input type="text" name="contact" placeholder="Phone">
				<span class="contactField<?=$sE['id']?>"></span>
			</div>
			<div class="input-group">
				<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
			    <input type="text" name="appointmentdate" placeholder="Booking Date" id="flatpickr">
			    <span class="contactField<?=$sE['id']?>"></span>
			</div> 
		    <div class="service-select">
				<select name="bookservice" id="selService" class="selService">
					<?php foreach ($services as $service) :?>
						<option value="<?=$service?>"><?=$service?></option>
					<?php endforeach?>
				</select>
			</div>
		    <div class="textar">
				<span> <i class="fas fa-comment-alt fa"></i></span>
				<textarea name="message" style="min-height: 100px;" placeholder="Message"></textarea>
				<span class="msgField<?=$sE['id']?>"></span>
			</div>
			<div class="mb-30">
				<div class="center-holder">
					<button type="submit">SUBMIT</button>
				</div>
			</div>     
		</form>
		</div>
<!-- end of sponsored booking section -->

<!-- end of sponsored section -->	

<?php endforeach?>
<!-- check filter start -->

<div id="expert-section">
	
</div>

<!-- check filter end -->
				
<!--						 start of expert section 						-->
<div id="default-section">

<?php foreach ($findexpertbyservice as $expert) : ?>
<div class="expertBox">
	<?php
		$fullName = $expert['name'] . $expert['last_name'];
		$slug = strtolower(url_title($fullName)); 
		$services = explode(',', $expert['service']); 
	?>
	<div class="col-md-12 expert-section" id="expertBox">
		<div class="col-md-1 expert-pics new-pad-0">
		<?php 
			$expertProfilePic = $expert['profile_photo'];
			$expertGender = strtolower($expert['gender']);
			if(empty($expertProfilePic)) {
				if($expertGender == 'male'){
					$expertProfilePic = 'avatar_default_male.png';
				}else{
					$expertProfilePic = 'avatar_default_female.png';
				}	
			}
		?>
		<img src="<?= base_url();?>/profilepic/<?= $expertProfilePic;?>">
		</div>

		<div class="col-md-8 expert-details">
			<h3><?=$expert['name'] . $expert['last_name']?></h3>
			    <p><?=$expert['eduction']?></p>
				<p><?=$expert['experience']?> Years Exp.</p>
					
			<div style="clear:both;"></div>
			<div class="client-feild">
				<ul>
				<li>FDI Compliance</li>
				<li>Company Formation Registration</li>
				<!--<li style="background: none;"><a href="">View all 47 services</a></li>-->
				</ul>
			</div>
			<div class="expert-about">
					<?php if($expert['summary'] != null) :?>
				     <p> <?=substr($expert['summary'], 0, 100)?><a href="expert/reetu/6/" target="_blank" class="view-new-profile"> Read More</a></p>
					<?php endif?>
			</div>
		</div>
		<div class="col-md-3 rhs-details">
			<p><i class="fas fa-map-marker-alt"></i> <?=$expert['city'] . ' , ' . $expert['state'] . ', '. $expert['country']?></p>
				<p><a href="https://www.caonweb.com/expert/reetu/6/" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i>  2 Feedback </a></p>		
		
			<p><a href="https://www.caonweb.com/add-review.php?id=6" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i> Add Reveiw </a></p>
						
					 
			<hr style="background-color: #f0f0f5; height: 0px; margin: 16px 0px 16px 0px;">
			<!--<h4>Prime <i class="fas fa-check"></i>
				<span>Max. 15 mins wait + Verified details</span>
			</h4>-->
		</div>
		<div class="col-md-12 fl-right-new">
		<a href="<?= base_url();?>expert/<?= $slug . '/' . $sE['id'] ?>" target="_blank" class="view-new-profile">View Profile</a>
			 
			<button type="button" class="btn btn-success po" data-toggle="popover" title="" data-content="Contact-Now - <?= $expert['phone'] ?>" data-original-title="Ext. No : <?= $expert['state_id'] ?>">Contact Now</button>

		 
			<a href="#small-dialog" data-toggle="modal" data-target=".query-ser" class="appoint" type="button" data-id="<?=$expert['id'] ?>" id="bookid" onclick="bookNow($(this).attr('data-id'))">Book Appointment</a>
		</div>

	</div>
</div>

<!-- new booking section -->

<div class="col-md-12 booking-section" id="dynamic-book<?=$expert['id']?>" style="display: none;">
	<span class="successField<?=$expert['id']?>"></span>
	<h4>Please fill up the below details to confirm the appointment.</h4>
	<form class="contact-form myform" name="RegForm<?=$expert['id']?>" id="RegForm<?=$expert['id']?>" data-id="<?=$expert['id']?>" action="" onsubmit="return bookingformValidation($(this).attr('data-id'))" method="post"> 
    <div class="input-group">
		 <input type="hidden" name="exp_id" value="<?=$expert['id']?>">
		 <span><i class="fa fa-user" aria-hidden="true"></i></span>
		 <input type="text" size=65 name="name" placeholder="Name">
		 <span class="nameField<?=$expert['id']?>"></span>
	</div>   
	<div class="input-group">
		<span><i class="fa fa-envelope" aria-hidden="true"></i></span>
	    <input type="text" size=65 name="email" placeholder="email">
	    <span class="emailField<?=$expert['id']?>"></span>
	</div>
	<div class="input-group">
		<span><i class="fa fa-phone" aria-hidden="true"></i></span>
		<input type="text" name="contact" placeholder="Phone">
		<span class="contactField<?=$expert['id']?>"></span>
	</div>
	<div class="input-group">
		<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    <input type="text" name="appointmentdate" placeholder="Booking Date" id="flatpickr">
	    <span class="dateField<?=$expert['id']?>"></span>
	</div> 
    <div class="service-select">
		<select name="bookservice" id="selService" class="selService">
			<?php foreach ($services as $service) :?>
				<option value="<?=$service?>"><?=$service?></option>
			<?php endforeach?>
		</select>
	</div>
    <div class="textar">
		<span> <i class="fas fa-comment-alt fa"></i></span>
		<textarea name="message" style="min-height: 100px;" placeholder="Message"></textarea>
		<span class="msgField<?=$expert['id']?>"></span>
	</div>
	<div class="mb-30">
		<div class="center-holder">
			<button type="submit">SUBMIT</button>
		</div>
	</div>     
</form>
</div>
		<!-- end of booking section -->

<?php endforeach?>

<div class="button-container text-center">
		<button class="booking-box-button btn btn-primary" id="loadMore"> Load More </button>
</div>
	
</div>
<!--end of expert section -->

<!-- page list -->
				
</div>
			
		<div class="col-md-3 ">
		<div class="rhs-details1">
			<h3>Submit your detail. Expert will connect with you instantly.</h3>
			<p></p>
			<form method="post" action="https://www.caonweb.com/post-a-job-submit.php" class="contact-form">
				<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
						<input type="text" name="name" placeholder="Name" required="">
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<input type="email" name="email" placeholder="E-mail" required="">
					</div>
					<div class="col-md-12">
						<input type="text" name="contact" placeholder="Phone" required="">
					</div>
								
					<div class="col-md-12">
						<select name="services" required="">
						<option>Select Service</option>
						<option value="Director kyc">Director kyc</option>
						<option value="FDI Compliance">FDI Compliance</option>
						<option value="Company Formation Registration">Company Formation Registration</option>
						<option value="Statutory Voluntary audits">Statutory Voluntary audits</option>
						<option value="Book keeping Outsourcing">Book keeping Outsourcing</option>
						<option value="GST Registration Filing">GST Registration Filing</option>
						<option value="Tax Filing Expert">Tax Filing Expert</option>
						<option value="Other">Other</option>
						<option value="GST Return">GST Return</option>
						<option value="FSSAI Registration">FSSAI Registration</option>
						<option value="ROC Filing">ROC Filing</option>
						<option value="Trust Formation">Trust Formation</option>
						<option value="ISO Registration">ISO Registration</option>
						<option value="DSC">DSC</option>
						<option value="NIR Registration">NIR Registration</option>
						<option value="MSME Registration">MSME Registration</option>
						<option value="IEC Registration">IEC Registration</option>
						<option value="LLP Annual Filing">LLP Annual Filing</option>
						<option value="Company Annual filing">Company Annual filing</option>
						<option value="CA Certification">CA Certification</option>
						<option value="Startup Recognition">Startup Recognition</option>
						<option value="Organic Product Certification">Organic Product Certification</option>
						<option value="Trademark registration">Trademark registration</option>
						<option value="APEDA Registration">APEDA Registration</option>
						<option value="Food License">Food License</option>
						<option value="Risk management Advisory">Risk management Advisory</option>
						<option value="Income Tax Return">Income Tax Return</option>
						<option value="FDI Advisory">FDI Advisory</option>
						<option value="Foreign Taxation">Foreign Taxation</option>
						<option value="Investment Advisory">Investment Advisory</option>
						<option value="Payroll Consulting">Payroll Consulting</option>
						<option value="TDS Consulting">TDS Consulting</option>
						<option value="GST Consulting">GST Consulting</option>
						<option value="Shop License">Shop License</option>
						<option value="Wealth creation and management">Wealth creation and management</option>
						<option value="E-commerce startup consulting">E-commerce startup consulting</option>
						<option value="Appointment and resignation of directors">Appointment and resignation of directors</option>
						<option value="Change in Share capital of company">Change in Share capital of company</option>
						<option value="Income Tax consulting">Income Tax consulting</option>
						<option value="Financial Reporting">Financial Reporting</option>
						<option value="Financial Planning">Financial Planning</option>
						<option value="Family business consulting">Family business consulting</option>
						<option value="Due Diligence">Due Diligence</option>
						<option value="Business Valuations">Business Valuations</option>
						<option value="Business Recovery">Business Recovery</option>
						<option value="Registered Address Change of a company">Registered Address Change of a company</option>
						<option value="Company Closure">Company Closure</option>
						<option value="Tax Planning">Tax Planning</option>
						<option value="Foreign Remittance compliance (15CA/CB, RBI filings)">Foreign Remittance compliance (15CA/CB, RBI filings)</option>
						<option value="Business analysis">Business analysis</option>
						<option value="Business consultancy">Business consultancy</option>
						<option value="Business Startup consultancy">Business Startup consultancy</option>
						<option value="Statutory Reporting">Statutory Reporting</option>
						<option value="Auditing">Auditing</option>
						<option value="Bookkeeping">Bookkeeping</option>
						<option value="Company secretarial">Company secretarial</option>
						<option value="Company formation">Company formation</option>
						</select>
						</div>
							
						<div class="col-md-12">
							<textarea name="message" placeholder="Message" style="min-height: 100px;" required=""></textarea>
						</div>
						<div class="col-md-12">
							<div class="textarea-message form-group">
				              <div class="g-recaptcha captha1" data-sitekey="6LeTkicUAAAAAGCGn6MnsPhpccAGPgaR6gb8PPhi"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LeTkicUAAAAAGCGn6MnsPhpccAGPgaR6gb8PPhi&amp;co=aHR0cHM6Ly93d3cuY2FvbndlYi5jb206NDQz&amp;hl=en&amp;v=v1552285980763&amp;size=normal&amp;cb=cvvd11etrg6k" width="304" height="78" role="presentation" name="a-esff02dpw79y" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div></div>
			                </div>
			            </div>
							<script src="https://www.google.com/recaptcha/api.js"></script>
								<div class="col-md-12 mb-30">
									<div class="center-holder">
										<button type="submit" onclick="alert('Your data is safe &amp; secure. Privacy &amp; Security are our primary concern')">SUBMIT</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					
					<div class="booking-box">
						<div class="iconContainer">
						<div class="icon">
						<i class="fa fa-calendar-check-o"></i>
						</div>
						</div>
						<div class="box-info">
						<div class="booking-box-title">
						Online Bookings
						</div>
						<p>
						Want to make or receive online bookings?
						</p>
						<div class="button-container">
						<a class="booking-box-button btn btn-default" href="" target="_blank">
						Find out more
						</a>
						</div>
						</div>
					</div>
					<div class="abt-breif">
						<h3>About Us</h3>
						<p>CAONWEB is already a well-known name in the list of service providers of accounting, tax &amp; compliance areas, We have happy clientele &amp; satisfied customers, We are the one who started providing the said services through online means. We have analysed the business.</p>
					</div>
				</div>
			
			</div>
</div>