<div class="page-title-section" style="background-image: url(https://www.caonweb.com/new-images/breadcrumb-images1.png">
		<div class="container">
			<h1>Expert Profile</h1>
				 
		</div>
</div>

<?php foreach ($experts as $expert) :?>
	<div class="container">
			<div class="row">
				<div class="col-md-9">
				
				
				<div class="profile-section">
					<div class="image-location">
 						<figure>
							<img src="<?=base_url();?>profilepic/<?= $expert['profile_photo'];?>" alt="">
						</figure>
 						<h3><?=$expert['name'] . $expert['last_name']?></h3>
						<h2><?=$expert['eduction']?></h2>
						<i class="fas fa-map-marker-alt"></i> <span><?=$expert['city'] . ', ' . $expert['state'] . ', '. $expert['country']?></span>
 						<div class="rating-fl-right">
							<h3>Client Reviews</h3>
							<i class="text-warning fa fa-star"></i><i class="text-warning fa fa-star"></i><i class="text-warning fa fa-star"></i><i class="text-warning fa fa-star"></i><i class="text-warning fa fa-star"></i>
						</div>
					</div>
					<div style="clear:both;"></div>
					<div class="profile-details">
						
						<p><?=$expert['summary']?></p>

					</div>
 				</div>
				
				<div class="profile-section">
					<div class="area-of-expert">
					<h3>Area of Expert Specification</h3>
						<ul>
							<?php
								$specification = explode(',', $expert['service']);

								foreach ($specification as $spec) {
									echo "<li>" . $spec . "</li>";
								}
							?>
						</ul>
					</div>
 				</div>
				<div class="profile-section">
					<div class="review">
					
 				</div>
 				</div>
	
				</div>

<?php endforeach ?>
				
				
				
				<div class="col-md-3">
					<div class="get-in-touch">
						<h3> Book appointment<span style="float: left;margin-left: 39px;"> with this expert (No Fee)</span></h3>
						
			
						
						
					<div class="register-form-wrapper ">
 	                    <!--begin form-->
	                    <div>
 	                        <!--begin success message -->
	                        <p class="register_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
	                        <!--end success message -->
	                        
	                        <!--begin register form -->
	                        
							  <form class="register-form register" method="post" action="https://www.caonweb.com/expert-booking-submit.php">
	                            
	                
	                            <input type="hidden" name="exp_id" value="46">
								
	                            <input class="register-input white-input" required="" name="name" placeholder="Your Name*" type="text">
	                        
	                            <input class="register-input white-input" required="" name="email" placeholder="your Email*" type="email">
								
	                            <input class="register-input white-input" required="" name="contact" placeholder="your Phone*" type="text">
																<select name="service">
																
									<option value="Director kyc">Director kyc </option>
								
																
									<option value="FDI Compliance">FDI Compliance </option>
								
																
									<option value="Company Formation Registration">Company Formation Registration </option>
								
																
									<option value="Statutory Voluntary audits">Statutory Voluntary audits </option>
								
																
									<option value="Book keeping Outsourcing">Book keeping Outsourcing </option>
								
																
									<option value="GST Registration Filing">GST Registration Filing </option>
								
																
									<option value="Tax Filing Expert">Tax Filing Expert </option>
								
																
									<option value="Other">Other </option>
								
																
									<option value="GST Return">GST Return </option>
								
																
									<option value="FSSAI Registration">FSSAI Registration </option>
								
																
									<option value="ROC Filing">ROC Filing </option>
								
																
									<option value="Trust Formation">Trust Formation </option>
								
																
									<option value="ISO Registration">ISO Registration </option>
								
																
									<option value="DSC">DSC </option>
								
																
									<option value="NIR Registration">NIR Registration </option>
								
																
									<option value="MSME Registration">MSME Registration </option>
								
																
									<option value="IEC Registration">IEC Registration </option>
								
																
									<option value="LLP Annual Filing">LLP Annual Filing </option>
								
																
									<option value="Company Annual filing">Company Annual filing </option>
								
																
									<option value="CA Certification">CA Certification </option>
								
																
									<option value="Startup Recognition">Startup Recognition </option>
								
																
									<option value="Organic Product Certification">Organic Product Certification </option>
								
																
									<option value="Trademark registration">Trademark registration </option>
								
																
									<option value="APEDA Registration">APEDA Registration </option>
								
																
									<option value="Food License">Food License </option>
								
																
									<option value="Risk management Advisory">Risk management Advisory </option>
								
																
									<option value="Income Tax Return">Income Tax Return </option>
								
																
									<option value="FDI Advisory">FDI Advisory </option>
								
																
									<option value="Foreign Taxation">Foreign Taxation </option>
								
																
									<option value="Investment Advisory">Investment Advisory </option>
								
																
									<option value="Payroll Consulting">Payroll Consulting </option>
								
																
									<option value="TDS Consulting">TDS Consulting </option>
								
																
									<option value="GST Consulting">GST Consulting </option>
								
																
									<option value="Shop License">Shop License </option>
								
																
									<option value="Wealth creation and management">Wealth creation and management </option>
								
																
									<option value="E-commerce startup consulting">E-commerce startup consulting </option>
								
																
									<option value="Appointment and resignation of directors">Appointment and resignation of directors </option>
								
																
									<option value="Change in Share capital of company">Change in Share capital of company </option>
								
																
									<option value="Income Tax consulting">Income Tax consulting </option>
								
																
									<option value="Financial Reporting">Financial Reporting </option>
								
																
									<option value="Financial Planning">Financial Planning </option>
								
																
									<option value="Family business consulting">Family business consulting </option>
								
																
									<option value="Due Diligence">Due Diligence </option>
								
																
									<option value="Business Valuations">Business Valuations </option>
								
																
									<option value="Business Recovery">Business Recovery </option>
								
																
									<option value="Registered Address Change of a company">Registered Address Change of a company </option>
								
																
									<option value="Company Closure">Company Closure </option>
								
																
									<option value="Tax Planning">Tax Planning </option>
								
																
									<option value="Foreign Remittance compliance (15CA/CB, RBI filings)">Foreign Remittance compliance (15CA/CB, RBI filings) </option>
								
																
									<option value="Business analysis">Business analysis </option>
								
																
									<option value="Business consultancy">Business consultancy </option>
								
																
									<option value="Business Startup consultancy">Business Startup consultancy </option>
								
																
									<option value="Statutory Reporting">Statutory Reporting </option>
								
																
									<option value="Auditing">Auditing </option>
								
																
									<option value="Bookkeeping">Bookkeeping </option>
								
																
									<option value="Company secretarial">Company secretarial </option>
								
																
									<option value="Company formation">Company formation </option>
								
																
								</select>
	                      
								
								<input type="text" name="appointmentdate" placeholder="Booking Date" id="datepicker">
	                           
										 
								<textarea cols="20" rows="5" class="with-border" name="message" placeholder="Message"></textarea>
								
								<!--<input value="Contact Now" class=" get-in-touch-btn" type="submit">-->
	                                
								<input type="submit" style="color: green;background: #0b4a68 !important;color: #fff;border-radius: 32px !important;" value="Submit" class="inputButton">
								
	                        </form>
	                        <!--end register form -->
	                        
	                    </div>
	                    <!--end form-->

                    </div>
				
			</div>
			
		 
			
			<div class="get-in-touch write-review">
						<h3 style="text-align: center;color: #fff;margin-bottom: -1px;padding-top: 14px;">Write a Review</h3> 
					<div class="register-form-wrapper ">
 	                    <!--begin form-->
	                    <div>
 	                        <!--begin success message -->
	                        <p class="register_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
	                        <!--end success message -->
	                        
	                        <!--begin register form -->
	                        <!--<form class="register-form register" action="" method="post">-->
	                         <form enctype="multipart/form-data" class="register-form register" action="https://www.caonweb.com/expert-review-submit.php" method="POST">   
	                
					            <input class="register-input white-input" value="46" name="exp_id" type="hidden">
	                            <input class="register-input white-input" required="" name="name" placeholder="Your Name*" type="text">
	                            <input class="register-input white-input" required="" name="email" placeholder="your Email*" type="email">
								
	                            <input class="register-input white-input" required="" name="phone" placeholder="your Phone*" type="number">
								<select name="rating">
									<option>Select Star for Rating</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
 								<textarea cols="30" rows="3" name="mess" class="with-border" placeholder="Message"></textarea>
				               
							 
	                              <input type="submit" class="get-in-touch-btn" value="Submit Review" style="background: #0b4a68 !important;    color: #fff;border-radius: 32px !important;">  
	                        </form>
	                        <!--end register form -->
	                        
	                    </div>
	                    <!--end form-->

                    </div>
				
			</div>
		 </div>
		  

	</div>
</div>

