<div class="swiper-main-slider swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image:url(<?= base_url();?>assets/images/banner1.png);">
					<div class="medium-overlay"></div>
					<div class="container">
						<div class="slider-content left-holder">
							<h1 class="animated fadeInDown"> Book Appointment With Tax & Compliance Expert   </h1>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<p class="animated fadeInDown">No geographical barriers to start & manage your business</p>
									<div id="search">
			<form method="GET" action="expert-search.php" class="form-inline row" style="width: 100%;">
								<div class="form-group col-md-3 col-xs-12">

								<div class="country-box">
								<select name="country" title="All Country" class="country" id="country" onchange="showServices(this.value)">
									<option value="">Select Country</option>
									<?php foreach ($countries as $country): ?>
										<option value="<?= $country['country_id'] . ' ' . $country['country']?>"><?= ucwords($country['country']);?></option>
									<?php endforeach ?>
							   	</select>
							   </div>
								<span class="icon-search" onclick="get_Location();">
									<i class="fas fa-map-marker-alt"></i>
								</span>
								</div>
								<div class="clearContent second-input form-group col-md-3 col-xs-12">
									<div class="service-box">
										<select name="services" title="All Country" class="services" id="selUser">
				                  		<option>No Service Selected</option>
                                    </select>
									</div>		
	
									<span class="icon-search" ><i class="fas fa-dice-d6"></i></span>
									<span class="line-decor"></span>
								</div>
											<div class="col-md-2 col-xs-12">
												<button type="submit" class="btn btn-default btn-main" >
												<i class="fas fa-search"></i><span class="hide">Search</span>
												</button>
											</div>
										</form>										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<section class="top-hire">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h2>Find tax & compliance Experts</h2>
				</div>
				<div class="col-md-3">
					<h3>Find Book keeping & Auditing Experts </h3>
					
				</div>
				<div class="col-md-3">
					<h3> Find GST Registration & Filing Experts</h3>
					
				</div>
				<div class="col-md-3">
					<h3>Find licenses & certifications Experts</h3>	
				</div>		
			</div>
		</div>
	</section>
		
<nav class="social1">
<ul>
<li><a href="https://api.whatsapp.com/send?phone=917065818801" target="_blank">Chat with Expert <i class="fab fa-whatsapp"></i></a></li>
<li><a href="skype:live:reetu_39?call" target="_blank">Skype Call with Expert<i class="fab fa-skype"></i></a></li>	 
</ul>
</nav>		
				
	<div class="container">
		<div class="row">
			<div class="section-heading center-holder mt">
				<h2>Online Directory of Tax/Compliance Experts</h2>
				<div class="section-heading-line"></div>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/tax-filing-expert.html" target="_blank">
					<div class="area">
						<i class="icon-target"></i>
						<h3>Tax Filing Expert</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/gst-registration-filing.html" target="_blank">
					<div class="area">
						<i class="icon-diamond"></i>
						<h3>GST Registration & Filing</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/trademark-registration.html" target="_blank">
					<div class="area">
						<i class="icon-networking"></i>
						<h3>Trademark Registration</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/book-keeping-outsourcing.html" target="_blank">
					<div class="area">
						<i class="icon-hourglass"></i>
						<h3>Book keeping & Outsourcing</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/statutory-voluntary-audits.html" target="_blank">
					<div class="area">
						<i class="icon-bank"></i>
						<h3>Statutory & Voluntary audits</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/company-formation-registration.html" target="_blank">
					<div class="area">
						<i class="icon-document"></i>
						<h3>Company Formation & Registration</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/fdi-compliance.html" target="_blank">
					<div class="area">
						<i class="icon-target"></i>
						<h3>FDI Compliance</h3>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a href="https://www.caonweb.com/directory/company-annual-filing.html" target="_blank">
					<div class="area">
						<i class="icon-hourglass"></i>
						<h3>Company Annual Filing</h3>
					</div>
				</a>
			</div>
			<div class="col-md-12" style="text-align: center;margin-top: 50px;">
			<a href="tax-and-compliance-services-all.php" target="_blank" class="primary-button button-md" style="background: #80b84d;">See all Category</a>	
			</div>		
		</div>
	</div>
		
	
		<p>&nbsp;</p>
		<p>&nbsp;</p>
 

		<div class="financial-solution-area pad100 bg-fs parallax  " style="background: whitesmoke;">
			<div class="container">
				<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 d-none d-lg-block">
						<div class="section-heading center-holder mt mt0" style="float: left;">
							<h2 >Online service marketplace</h2>
							 <div class="section-heading-line" style="float: left;margin-top: 0;margin-bottom: 25px;"> </div>
						</div> 
					</div>
					<!-- /col-->
					<div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
						<div class="inner-box text-center mb30">
							<div style="clear:both;"></div>
							<div class="box-icon">
								<img src="<?= base_url();?>assets/images/expert.svg" alt="expert">
							</div>
							
								<h3>Expert Profiles</h3>
							
							<p>All the experts have a profile which is visible to the client, As a service receiver, you get to the location and the detailed. </p>
						</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
						<div class="inner-box text-center mb30">
							<div class="box-icon">
								<img src="<?= base_url();?>assets/images/book.svg" alt="book">
							</div>
							
								<h3>Book Appointment</h3>
							
							<p> Contact a service provider on the basis of location or category of services. It’s quick and convenient.</p>
						</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
							<div class="inner-box text-center mb30">
							<div class="box-icon">
								<img src="<?= base_url();?>assets/images/blog.svg" alt="blog">
							</div>
							
								<h3>Informative blogs</h3>
						
							<p>We regularly update you with interesting, useful news, notification in Indian and Global economy. </p>
						</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
						<div class="inner-box text-center mb30">
							<div class="box-icon">
								<img src="<?= base_url();?>assets/images/secure.svg" alt="secure">
							</div>
							
								<h3>Secure Payment</h3>
						
							<p>This platform will also have an option of  safe gateway payment as per your arrangement with client.</p>
						</div>
					</div>
				 
				</div>
				<!-- /row-->
			</div>
			<!-- /container-->
		</div>

		<div class="elemnts-services section-pading2">
			<div class="container">
				<div class="row">
					<div class="section-heading center-holder mt">
						<h2>Find Tax/Compliance Expert</h2>
						<div class="section-heading-line"></div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single-p-list text-right">
							<img src="<?= base_url();?>assets/images/icon1.png" alt="icon">
							<h2>Convenience</h2>
							<p>Having serve the clients since 2012, We understand your needs therefore we will give you the best experience throughout</p>
						</div>
						<!-- end single p list -->
						<div class="single-p-list space text-right">
							<img src="<?= base_url();?>assets/images/icon2.png" alt="icon">
							<h2>Support</h2>
							<p>For both the service providers & service receivers, We are always available  to you through Chat, Email, Call or social media  </p>
						</div>
					</div>
					<!-- end single feature step -->
					<div class="col-md-6 hidden-sm hidden-xs">
						<div class="single-feature-product-step" style="text-align: center;">
							<img src="<?= base_url();?>assets/images/find-tax-compliance-expert.gif" alt="" style="width: 70%;">
						</div>
					</div>
					<!-- end single fetaure step -->
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single-p-list text-left">
							<img src="<?= base_url();?>assets/images/icon3.png" alt="icon">
							<h2>Experience</h2>
							<p> This is our first priority as it means everything that any user want to have while using online service facility.</p>
						</div>
						<!-- end single p list -->
						<div class="single-p-list space-r  text-left">
							<img src="<?= base_url();?>assets/images/icon4.png" alt="icon">
							<h2>Safety</h2>
							<p>Poor safety will lead to losing people, we very well understand this fact. Should there be NDA? Anytime!</p>
						</div>
					</div>
					<!-- end single feature step -->
				</div>
				<div class="big-spacer big-h"></div>
			</div>
		</div>
		
		<section class="intro-area bg-gray" style="background: #80b84d;">
			<div class="">
				<div class="row">
					<div class="col-xl-12">
						<div class="intro-text text-center">
							<h3 style="color: #fff;">You can compare and select from the list of experts</h3>
							<p style="color: #fff;">People forget how fast you did a job, but they remember how well you did it. –Howard W. Newton</p>
							<a href="https://www.caonweb.com/contact.php" class="primary-button button-md" style="background: #3c611a;">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
	<div class="section-block pb-0" style="background: whitesmoke; display:none;">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6 col-xs-12">
						<div class="section-heading mt-30">
							<h4>We deliver the best experience</h4>
							<div class="section-heading-line-left"></div>
						</div>
						<div class="text-content mt-25">
						<p>What this platform does- If I am willing to start a business at any part of the globe, what is my first step? It is to connect with a consultant who will help me solve my issue. This platform serves you with options to select expert from our reliable list. Starting a business or compliance or advisory or any other such services, you can contact service providers as per your convenience. The platform is managed by team of Tax compliance professionals, Engineers, Developers.  </p>
						</div>
						<div class="mt-25">
							<a href="https://caonweb.com/about.php" class="primary-button button-md">About us</a>
						</div>
					</div>
					<div class="col-md-7 col-sm-6 col-xs-12">
						<img src="<?= base_url();?>assets/images/image7.png" alt="image">
					</div>
				</div>
			</div>
	</div>
		 
		<section class="what-we-do-three sp-two grey-bg">
			<div class="container">
				<div class="row">
					
					<div class="col-md-4">
							<div class="service-block-eight">
								<div class="inner-box hvr-bounce-to-bottom">
									<div class="icon-box1">
										<img src="<?= base_url();?>assets/images/loc.svg">
									</div>
									<h4><a href="service-details.html">Current Location</a></h4>
									<div style="clear:both;"></div>
									<div class="text flag-name">
							
										<ul>
					<?php foreach ($currentcountrys as $courentcountry) :?>

		<li><img src="https://www.caonweb.com/uploads/1537869024_india.jpg"><a href="<?= base_url();?>directory/location/<?=$country['country']?>.html" target="_blank"><?=$country['country']?></a></li>

					 <?php endforeach?>					
					</ul>
									</div>
								</div>
							</div>
						</div>
					<div class="col-lg-8">
					
					<div class="col-md-6">
							<div class="service-block-eight">
								<div class="inner-box hvr-bounce-to-bottom">
									<div class="icon-box1">
										<img src="<?= base_url();?>assets/images/loc.svg">
									</div>
									<h4><a href="service-details.html">Upcoming Location</a></h4>
									<div style="clear:both;"></div>
									<div class="text flag-name">
									<ul>
									

	<li><img src="https://www.caonweb.com/uploads/1537869024_india.jpg"><a href="" target="_blank">India</a></li>
										    
											
										</ul>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="service-block-eight">
								<div class="inner-box hvr-bounce-to-bottom">
									<div class="icon-box1">
										<img src="<?= base_url();?>assets/images/loc.svg">
									</div>
									<h4><a href="service-details.html">Future Location</a></h4>
									<div style="clear:both;"></div>
									<div class="text flag-name">
									   
										<ul>
											
		<li><img src="https://www.caonweb.com/uploads/1537869024_india.jpg"><a href="" target="_blank">India</a></li>
										
										</ul>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>
		
<section class="skills-services ">
			<div class="container">
				<div class="row">
					<div class="section-heading center-holder mt">
						<h3>Find Experts near you for services</h3>
						<div class="section-heading-line"></div>
					</div>
					<div class="expert-ul">
					
						<ul>
							<?php foreach ($service as $service): ?>
						    <?php $url = $service['service_name'];
							      $finalurl = preg_replace('#[ -]+#', '-', $url); 
							?>
				<li><a href="<?= base_url();?>directory/<?= $finalurl ;?>.html" target="_blank"><?= $service['service_name'];?></a></li>
							<?php endforeach ?>

						
						</ul>
					</div>
				</div>
			</div>
</section> 
		
<div style="clear:both;"></div>
		
		
<section style="background: #f5f5f5 !important;padding: 39px 0px;">			
	
<div class="container footer-content">
<div class="col-md-10 col-md-offset-1">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand" style="margin: 0;">
           <div class="right-arrow pull-right" style="color: #000;">+</div>
          <a href="#" style="color: #000;">Online Directory of Chartered Accountant, Company Secretary, Tax Experts for All Your Business Start-Up Tax and Compliance Needs</a>
        </h3>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
       <div class="panel-body">
		<p>How to start the business? What licenses are required for factory? How to find top <b>Chartered Accountant</b> and <b>company secretary</b>? How to save tax? What procedure is required for <a href="https://www.caonweb.com/company-registration.php" target="_blank" style="color: #80b84d;">company registration</a>?</p>

 

<p>Such questions come to mind whenever you start a business. In doing the business, there is need of lot many Tax experts and legal experts for your compliances fulfilment of the organisation out of which more significant is bookkeeping and accounting. Not all the businessmen themselves are capable of taking care of compliances as they are not able to take out extra time specifically for this, there comes the need of reliable professionals like chartered accountants who are considered as tax experts, company secretary for corporate compliance expert, business advisors, wealth managers, bookkeeping experts, income tax consultant, trademark consultant etc. who are experienced & well versed with the applicable provisions of Law. Accounting and <a href="https://www.caonweb.com/book-keeping-and-outsourcing.php" target="_blank" style="color: #80b84d;"> Bookkeeping services </a> can also be outsourced in India on affordable bookkeeping services fees. Out sourcing of bookkeeping services is very helpful for small business and start up organizations. </p>

 

<p>The biggest challenge in this service market is to find the reliable professionals such as chartered Accountant and <a href="https://www.caonweb.com/directory/company-secretarial.html" target="_blank" style="color: #80b84d;"> Company secretaries </a> within the local limits at best affordable prices for the said legal services, cost audit, tax audit, etc. So here, CAONWEB can help you in choosing professionals like Chartered Accountant, Company Secretary, Tax consultants, accountants, business advisors, income tax consultant, trademark consultant etc.  at reasonable rates to simplify the legal compliances for your business entity such as company registration, incorporation, bookkeeping and accounting, tax and regulatory services, cost audit, tax audit, different types of audit etc.</p>

 

<p>You can get the best <b> Chartered Accountant services in India </b> and other countries among the directory available on CAONWEB which is safe, secure and authentic. CAONWEB itself verify the profiles of Chartered Accountant, Company Secretary, Tax consultants, accountants, business advisors, income tax consultant, trademark consultant, etc. registered on our online directory which bridges the gap between the service provider & you. So, you can find chartered accountant nearby you or expert professional nearby you through reliable online directory at the comfort of your home with just a click.</p>

 

<p> Whenever you want to start your business or for any other legal requirement, you can use the directory of Chartered Accountants, Company Secretaries , Tax Experts and other compliance professionals maintained by <b> CAONWEB </b> for satisfactory & reliable services. </p>

 

<p><b>CAONWEB</b> is providing online marketplace where you can choose professional according to your requirements.</p>


	
<h2>Find Legal Experts Online From CAONWEB - India's Only Online Directory</h2>

<p>CAONWEB is India’s only online directory that doesn't only provide the list of professionals but also makes sure that each of the </p>

<p><a href="https://www.caonweb.com/ca-services.php" target="_blank" style="color: #80b84d;">Chartered Accountant,</a>- Company Secretary, Tax consultants, Accountants, business advisors, income tax consultant, trademark consultant .</p>

<p>etc.registered with us is authentic & providing satisfactory services</p>
		</div>
      </div>
    </div>
    
  </div> 
</div>
</div>
</section>
	

		<a href="#" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>