<div class="page-title-section" style="background-image: url(https://www.caonweb.com/new-images/breadcrumb-images1.png);">
	<div class="container">
		<h1>About Us</h1>
			<ul>
				<li><a href="index.html">Home</a></li>
				<li><a href="about.html">About</a></li>
			</ul>
	</div>
</div>

<section class="full-row about-us-2">
  <div class="container">
   <div class="start-booking">
    <div class="row">
      <div class="col-lg-8">
        <div class="bg-gray text-block-1 p-4 h-100">
          <p>CAONWEB is already a well-known name in the list of service providers of accounting, tax &amp; compliance  areas, We have happy clientele &amp; satisfied customers, We are the one who started providing the said services through online means. We have analysed the business &amp; found that still in this area business owners faces problem in finding the qualified professionals within their geographical boundaries, so we came up with the idea to ease this problem &amp; also to increase the competitiveness in the local market, by providing you the ONLINE SERVICE MARKETPLACE where you can choose professionals according to your requirements &amp; budget within the geographical boundaries of your choice. Our aim is to cater small to large businesses with the best of professional services with reasonable rates &amp; to simplify the processes for smooth running of your business. </p>
          
        </div>
      </div>
      <div class="col-lg-4">
        <div class="bg-primary p-4 text-block-2">
          <h4 class="title-area-2 text-white mb-4"> <small class="text-default">We are here</small>Start  Your Booking </h4>
          <p> CAONWEB is providing online marketplace where you can choose professional according to your requirements. Go ahead, make your choice and relax!!
</p>
          <div class="mt-25">
							<a href="https://www.caonweb.com/contact.php" class="primary-button button-md" style="margin-bottom: 18px;background: #084460 !important;">Contact Us</a>
			</div> 
			</div>
      </div>
    </div>
    </div>
    <div class="row sec-para">
      <div class="col-md-8">
        <div class="main-title-area mb-4 mt-5">
          <h2 class="title">CAONWEB provides you secure online service marketplace.</h2>
        </div>
        <p>CAONWEB is designed specifically to bridge the gap &amp; break the geographical boundaries between the professionals &amp; clients around the world in relation to tax/finance services in safe &amp; secure manner. We provide options to clients to choose professional based on requirement with minimum fees or to find the right professional with no cost. We, team of experienced professionals of Accounting and Tax, figure out the needs of the market and develop better ways to serve clients.</p>
      </div>
      <div class="col-md-4">
        <div class="experience-years ">7+ Years</div>
      </div>
    </div> 
	
 		<div class="our-differents bg-primary mt-5">
      <div class="row">
        <div class="col-lg-6">
          <div class="text-block-2 p-40">
            <h4 class="title-area-2 text-white">Why trust us?</h4>
            <p>CAONWEB assures you to choose the professionals with the help of this online platform…
 </p>
            <ul class="icon-list-12">
              <li><i class="fas fa-check"></i> Verified profiles of CA/CPAs/CS.</li>
              <li><i class="fas fa-check"></i> Detailed information on location and types of services being provided by the professionals.</li>
              <li><i class="fas fa-check"></i> Customer support services for any issues faced by Professionals or the clients</li>
              <li><i class="fas fa-check"></i> An interactive discussion community where professionals discuss wide range of topics on
accounts/tax /finance or related topics. Informative with latest news on notifications under various laws, Tips on tax saving, Tax calendar and much more Work dashboard for professionals where they can manage their work Safety and security guarantee</li>
              
            </ul>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="img-1"> <img src="https://www.caonweb.com/new-images/man.jpg" alt="Why trust CAONWEB?"> </div>
        </div>
      </div>
    </div>
 	
  </div>
</section>

<section class="intro-area bg-gray" style="background: #126992;">
	<div class="">
		<div class="row">
			<div class="col-xl-12">
				<div class="intro-text text-center">
					<h1 style="color: #fff;">Clients are looking for Experts nearby them:</h1>
					<p style="color: #fff;">Create your profile and go Live! To make your profile good, update your profile details which are relevant to clients</p>
					<a href="https://www.caonweb.com/contact.php" class="primary-button button-md" style="background: #084460 !important;">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section>