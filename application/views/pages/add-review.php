

	
		<!---Book Appointment--------->
		<div class="modal fade query-ser"   tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="contact-form-box mt-30">
					<a href="" class="req-close"><i class="fas fa-times"></i></a>
					<h3>Book Appointment</h3>
					<div id="dynamic-book"></div>
				</div>
			</div>
		</div>
		<!---------Book Appointment-------------->
<?php foreach ($experts as $expert) :?>
	 
	 <div class="container">
		<div class="row">
			<div class="col-md-12 rev-heading">
				<h3>Write a review for "<?=$expert['name'];?> (<?=$expert['last_name'];?>)"</h3>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4 faq">
				<h3>FAQs</h3>
				
<div class="accordion-container">
 
  <div class="set">
    <a href="#"  >
      Are Experts listed on CAONWEB is verified?
      <i class="fa fa-plus"></i>
    </a>
    <div class="content" style="display: block;">
      <p>Yes, the professionals listed on our website are verified. </p>
    </div>
  </div>
  <div class="set">
    <a href="#">
      What is CAONWEB role while i will be taking service from any Expert nearby me? 
      <i class="fa fa-plus"></i>
    </a>
    <div class="content">
      <p> CAONWEB will facilitate you to reach out to the expert. We provide  verified profiles of professional on our online directory who assures the best services experience to our clients, also you can provide rating or feedback to the expert after taking services. This can help other people also to avail best of tax/compliance services.</p>
    </div>
  </div>
  <div class="set">
    <a href="#">
       What if i am unable to choose expert in your online directory?
      <i class="fa fa-plus"></i>
    </a>
    <div class="content">
      <p>You can simply visit <a href="https://www.caonweb.com/contact.php" target="_blank">Contact us</a>, submit your email id and contact number. We will reach you to choose the right professional according to your  requirements.</p>
    </div>
  </div>
  <div class="set">
    <a href="#">
       How can i select the right expert on CAONWEB online directory?  
      <i class="fa fa-plus"></i> 
    </a>
    <div class="content">
      <p> CAONWEB portal helps you to find experts in various cities. The profile of professional will give you an idea of their location and the types of services they are providing, based on the nature of service you are looking for.  </p>
    </div>
  </div>
    <div class="set">
    <a href="#">
       Is my data safe on CAONWEB?
      <i class="fa fa-plus"></i> 
    </a>
    <div class="content">
      <p> Safety and security of clients information is our primary concern. Yes your data is absolutely safe. To know more about privacy policy you can visit <a href="https://www.caonweb.com/expert-privacy-policy.php" target="_blank">Privacy policy</a> </p>
    </div>
  </div>
   
  
  
</div> 


			</div>
			<div class="col-md-4 add-re">
			<div class="" tabindex="-1" role="dialog">
							
								<div class="contact-form-box ">
								<a href="" class="req-close"><i class="fas fa-times"></i></a>
								<h3>Add Review</h3>
								
										<div id="dynamic-book">
	                          <form class="contact-form" enctype="multipart/form-data" action="" method="POST">
										<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
										 <input type="hidden" name="exp_id" value="<?=$expert['id'];?>">
												<input type="text" name="name" placeholder="Name" required="">
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<input type="email" name="email" placeholder="E-mail" required="">
											</div>
											<div class="col-md-12">
												<input type="text" name="contact" placeholder="Phone" required="">
											</div>
										 
											
											<div class="col-md-12">
												<select name="service" required="">
													<option>Star Rating</option>
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
														
													</select>
											</div>
											
											<div class="col-md-12" style="width:100% !important;">
												<textarea name="message" placeholder="Message" style="min-height: 100px;" required=""> </textarea>
											</div>
											<div class="col-md-12 mb-30">
												<div class="center-holder">
													<input type="submit"  class="get-in-touch-btn" value="Submit Review" >
												</div>
											</div>
										</div>
								</form>	 
			
			
			
</div>
								</div>
						
						</div>
			</div>
		</div>
	 </div>
		 
		<?php endforeach ?>  
		
	
<script>
		$(document).ready(function() {
  $(".set > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});

</script>
