
    <!-- End Navigation -->

    <div class="clearfix"></div>
<section class=" landing-page">
		<div class="landing-page-bg">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-sm-6">
							 <h1>Now Private Limited Company Registration is Easy, Affordable & Online!</h1>
							 <p>Call or email us to get in touch with the expert to know more about the private limited company registration process. Our expert will assist you at every step to register private limited company and also help you to complete the cumbersome documentation process. As for the ease of doing business in India, the government has opened the window for online private limited company registration in India due to which you need not visit the department or our office.</p>
							<div>
							 
							
						</div>
						</div>
						<?php include APPPATH.'/views/include/submit-form.php';?>
					</div>
				</div>
			</div>
		</div>
		   </section>
    
     
		 
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Online private limited company registration in India</h2>
						 
						<p>Private Ltd Company is one of the easiest, popular & affordable ways to have legal identification for your business. Private Ltd Company is governed by the Companies Act, 2013. The Pvt Ltd company can attract funding from a bank or financial institutions easily due to legal identification.</p>
						
						<p>The private limited company registration process is easy and also it is easier to restructure or wind up the business if such need arises. You can opt for online private limited company registration in India which is cost & time savvy. Pvt Ltd Company can be incorporated online through CAONWEB.</p>
						
						
						
					</div>
					
				</div>
			</div>
			 
			 	 
		<div style="clear:both;"></div>		 
				 
				 <section class="third-section">
					<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>What will you get?</h2>
							
							<ul>
<li><img src="<?= base_url();?>images/tick.png ">	Digital Signature for 2 Director </li>
<li><img src="<?= base_url();?>images/tick.png ">	DIN Numbers for 2 Directors </li>
<li><img src="<?= base_url();?>images/tick.png ">	2 Hard Copies of MOA and AOA ( In soft copy also) </li>
<li><img src="<?= base_url();?>images/tick.png ">	All Company Incorporation Process ( Incorporation Certificate) </li>
<li><img src="<?= base_url();?>images/tick.png ">	All Government Fees Included</li>
<li><img src="<?= base_url();?>images/tick.png ">	PAN Card</li>
<li><img src="<?= base_url();?>images/tick.png ">	TAN Number ( For Deducting TDS)</li>
<li><img src="<?= base_url();?>images/tick.png ">	Bank Account Opening (Documentation Support)</li><br>
								
							</ul>
							
							
							
							
							
							
					 
						</div>
						
							<div class="col-md-6 what-will-get">
							 
							<img src="<?= base_url();?>images/what-will-get.png"> 	 
						</div> 	
						
					</div>
					</div>
				 </section>
			 	    
    
    
    
     
				 <section class=" fifth-section">
					<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>How it's Done?</h2>
							<p><strong>Duration: </strong>The duration of the Pvt Ltd company registration process will be minimum of 15 days.</p>
					<ul>
								<li>
									<span>1</span>
										<h6> Complete our Query Form</h6>
										 
								</li>
								<li>
									<span>2</span>
										<h6>Company name availability search</h6>
										 
								</li>
								<li>
									<span>3</span>
										<h6>Documents submission</h6>
										 
								</li>
								
								<li>
									<span>4</span>
										<h6>Filing of application with MCA</h6>
										 
								</li>
								
								<li>
									<span>5</span>
										<h6> Obtain Incorporation certificate within 12-15 Days</h6>
										 
								</li>
							</ul>
						</div>
						 
						
					</div>
					</div>
				 </section>
    
    
    
    
    
    
    
				 <section class="third-section">
					<div class="container">
					<div class="row">
					
							<div class="col-md-5 what-will-get  plr-flr">
							 
							<img src="<?= base_url();?>images/registration.png"> 	 
						</div> 	
						<div class="col-md-7">
							<h2>Pvt. Ltd. company registration documents to be submitted:</h2>
							
							
							
							<ul>
<li><img src="<?= base_url();?>images/tick.png "> ID Proof (PAN Card)</li>
<li><img src="<?= base_url();?>images/tick.png "> Address Proof 1(Voter ID/Passport/Driving License)</li>
<li><img src="<?= base_url();?>images/tick.png "> Address Proof 2(Electricity Bill/Telephone Bill/Bank Statement)</li>
<li><img src="<?= base_url();?>images/tick.png "> Aadhar Card</li>
<li><img src="<?= base_url();?>images/tick.png "> Photograph</li>
<li><img src="<?= base_url();?>images/tick.png "> Electricity Bill and Rent Agreement of premises</li>
<li><img src="<?= base_url();?>images/tick.png "> Client Questionnaire </li>
 							
							</ul> 
					 
						</div>
						
						
					</div>
					</div>
				 </section>
    
   
			 	 <div style="clear:both;"></div> 	 	 
		
    
    <section class="pricing py-5 blue-strip">
  <div class="container">
    <div class="row">
	<div class="col-md-12">
	<p>"There are various compliances under the Companies Act 2013 which are required to be followed after incorporation of a Company. Managing the day to day operations of your business along with complying the corporate laws can be difficult and confusing for any entrepreneur. Therefore, it is essential to take the help of a professional for corporate law compliance services. Professionals providing corporate secretarial services also ensure timely fulfillment of compliances, without any levy of interest or penalty. Nowadays you can easily find online corporate secretarial services or corporate law compliance Services through our online directory of CAONWEB."</p>
	</div>
       
	  
	  
	  
    </div>
  </div>
</section>
    
    
     <div style="clear:both;"></div> 	 
    
    
    <section class="bg-grey faq">
    
	
		<div class="container">
		<h2>Frequently Asked Questions</h2>
		 
			 <div class='animatedParent' data-sequence='500'>
				<div class='  animated bounceInLeft slower'   data-id='1'>
				<div class="col-md-6">
					<!-- Tab -->
					<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
									 Is it necessary to have 2 directors for Private Limited Company Registration?
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>Yes, it is mandatory to have at least two directors in a Pvt Ltd company. Whereas the maximum number of members can be up to 200. If you are the sole owner, you can register as a One Person Company.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									  How to register Private Limited Company?

									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true" style="">
								<div class="panel-body">
									<p>Though you can choose an online mode to register Private Limited Company in India but private limited company registration process includes documentation, certification & verification by the CA or CS or CMA. Hence it is recommended to take help of such professionals for a smooth process.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									  Is Pvt Ltd company registration process difficult?

									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>No, now you can opt for online private limited company registration in India where you can submit your application online along with the required documents.</p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
					
				</div><!-- Column -->
				</div><!-- Column -->
				<div class='  animated bounceInRight slower' data-id='2'>
				<div class="col-md-6 margin-top-991-30" >
				
					<!-- Tab -->
					<div class="panel-group accordion dark" id="accordion2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse5" aria-expanded="false" aria-controls="collapse5" class="collapsed">
									  What are the advantages of pvt ltd company?

									</a>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The advantages of&nbsp;<strong>Pvt Ltd Company</strong>&nbsp;are as follows:-</p>
									<ul>
<li>Only two Directors is required to meet the minimum eligibility criteria.</li>
<li>Easy Setup and suitable for Growing Startups.</li>
<li>The Liability of Members is limited.</li>
<li>Increased Credibility.</li>
<li>Easy to raise funds from Investors.</li>
<li>ESOP Option is also available to Motivate Employees.</li>
</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse6" aria-expanded="false" aria-controls="collapseTwo">
								  How to check Pvt Ltd company registration?

									</a>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6" aria-expanded="true" style="">
								<div class="panel-body">
									<p>You can check Pvt Ltd company registration number as follows:</p>

<p>Company’s certificate of incorporation. The company name and company number are both written on the incorporation certificate issued by MCA.
You can check the registration number in the MCA database also.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading7">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse8" aria-expanded="false" aria-controls="collapseThree">
								  How much is the Pvt Ltd company registration fees?

									</a>
								</h4>
							</div>
							<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7" aria-expanded="false">
								<div class="panel-body">
									<p>The Private limited company registration fees include ROC fees, professional service fees and certification charges.</p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
				</div><!-- Column -->
				</div><!-- Column -->
				</div><!-- Column -->
			</div><!-- Row -->
 </section>
    
    
    

