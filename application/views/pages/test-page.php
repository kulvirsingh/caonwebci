<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
	function showServices(str) {

	  if (str=="") {
	    document.getElementById("selUser").innerHTML="<option> No Service Selected </option>";
	    return;
	  }

	  if (window.XMLHttpRequest) {
	    // code for IE7+, Firefox, Chrome, Opera, Safari
	    xmlhttp=new XMLHttpRequest();
	  } else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function() {
	    if (this.readyState==4 && this.status==200) {
	      document.getElementById("selUser").innerHTML=this.responseText;
	    }
};
  xmlhttp.open("GET","./assets/js/custom/state.php?q="+str,true);
  xmlhttp.send();
}
</script>
</head>
<body>
<form action="fetch-expert" method="get">
	<select name="state" id="state"  onchange="showServices(this.value)">
		<option value="">Select Country</option>
		<option value="Delhi">Delhi</option>
		<option value="Kolkata">Kolkata</option>
		<option value="Mumbai">Mumbai</option>
	</select>
	<div class="service-box">
		<select name="services" title="All Country" class="services" id="selUser">
			<option>No Service Selected</option>
	    </select>
	</div>
	<button class="btn btn-primary" type="submit" name="submit">Submit</button>
</form>
</body>