<div class="page-title-section" style="background-image: url(https://www.caonweb.com/new-images/breadcrumb-images1.png">
		<div class="container">
			<h1>Expert Profile</h1>
				 
		</div>
</div>
<section class="pad-t80">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="feature-9 text-center wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <i class="fa fa-map"></i>
                    <h4>Address</h4>
                    <p>E-36, First Floor, Sector 8, City - Noida 201301</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="feature-9 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;">
                    <i class="fa fa-mobile"></i>
                    <h4>Call Us</h4>
                    <p>(0120) 4231116</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="feature-9 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 1s; animation-name: fadeInUp;">
                    <i class="fa fa-envelope"></i>
                    <h4>E-mail</h4>
                    <p>info@caonweb.com</p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="request-form col-md-12 pull-right ">
   <h3 class="col-md-offset-2">Submit your detail here to connect with the right professional. This platform is meant for you to connect with the right professional. Details you submit is safe and private</h3>
   
    <form method="post" action="https://www.caonweb.com/post-a-job-submit.php">
					<div class="row">
						<div class="form-group col-md-4 col-md-offset-2">
 								<input class="with-border" name="name" type="text" placeholder="Your Name" required="required">
  						</div>

						<div class="form-group col-md-4 ">
						 
								<input class="with-border" name="email" type="email" placeholder="Email Address" required="required">

						</div>

					<div class="form-group col-md-4 col-md-offset-2">
					            	<select name="services">
									<option>Select Service</option>
															  <option value="Director kyc">Director kyc </option>
							  							  <option value="FDI Compliance">FDI Compliance </option>
							  							  <option value="Company Formation Registration">Company Formation Registration </option>
							  							  <option value="Statutory Voluntary audits">Statutory Voluntary audits </option>
							  							  <option value="Book keeping Outsourcing">Book keeping Outsourcing </option>
							  							  <option value="GST Registration Filing">GST Registration Filing </option>
							  							  <option value="Tax Filing Expert">Tax Filing Expert </option>
							  							  <option value="Other">Other </option>
							  							  <option value="GST Return">GST Return </option>
							  							  <option value="FSSAI Registration">FSSAI Registration </option>
							  							  <option value="ROC Filing">ROC Filing </option>
							  							  <option value="Trust Formation">Trust Formation </option>
							  							  <option value="ISO Registration">ISO Registration </option>
							  							  <option value="DSC">DSC </option>
							  							  <option value="NIR Registration">NIR Registration </option>
							  							  <option value="MSME Registration">MSME Registration </option>
							  							  <option value="IEC Registration">IEC Registration </option>
							  							  <option value="LLP Annual Filing">LLP Annual Filing </option>
							  							  <option value="Company Annual filing">Company Annual filing </option>
							  							  <option value="CA Certification">CA Certification </option>
							  							  <option value="Startup Recognition">Startup Recognition </option>
							  							  <option value="Organic Product Certification">Organic Product Certification </option>
							  							  <option value="Trademark registration">Trademark registration </option>
							  							  <option value="APEDA Registration">APEDA Registration </option>
							  							  <option value="Food License">Food License </option>
							  							  <option value="Risk management Advisory">Risk management Advisory </option>
							  							  <option value="Income Tax Return">Income Tax Return </option>
							  							  <option value="FDI Advisory">FDI Advisory </option>
							  							  <option value="Foreign Taxation">Foreign Taxation </option>
							  							  <option value="Investment Advisory">Investment Advisory </option>
							  							  <option value="Payroll Consulting">Payroll Consulting </option>
							  							  <option value="TDS Consulting">TDS Consulting </option>
							  							  <option value="GST Consulting">GST Consulting </option>
							  							  <option value="Shop License">Shop License </option>
							  							  <option value="Wealth creation and management">Wealth creation and management </option>
							  							  <option value="E-commerce startup consulting">E-commerce startup consulting </option>
							  							  <option value="Appointment and resignation of directors">Appointment and resignation of directors </option>
							  							  <option value="Change in Share capital of company">Change in Share capital of company </option>
							  							  <option value="Income Tax consulting">Income Tax consulting </option>
							  							  <option value="Financial Reporting">Financial Reporting </option>
							  							  <option value="Financial Planning">Financial Planning </option>
							  							  <option value="Family business consulting">Family business consulting </option>
							  							  <option value="Due Diligence">Due Diligence </option>
							  							  <option value="Business Valuations">Business Valuations </option>
							  							  <option value="Business Recovery">Business Recovery </option>
							  							  <option value="Registered Address Change of a company">Registered Address Change of a company </option>
							  							  <option value="Company Closure">Company Closure </option>
							  							  <option value="Tax Planning">Tax Planning </option>
							  							  <option value="Foreign Remittance compliance (15CA/CB, RBI filings)">Foreign Remittance compliance (15CA/CB, RBI filings) </option>
							  							  <option value="Business analysis">Business analysis </option>
							  							  <option value="Business consultancy">Business consultancy </option>
							  							  <option value="Business Startup consultancy">Business Startup consultancy </option>
							  							  <option value="Statutory Reporting">Statutory Reporting </option>
							  							  <option value="Auditing">Auditing </option>
							  							  <option value="Bookkeeping">Bookkeeping </option>
							  							  <option value="Company secretarial">Company secretarial </option>
							  							  <option value="Company formation">Company formation </option>
							  	
							  </select>
						 
					</div>
					
					
					<div class="form-group col-md-4  ">
						<input type="Ph. No." name="contact" placeholder="Ph. No."> 
					</div> 

					<div class="form-group col-md-8 col-md-offset-2">
					<textarea name="message" cols="40" rows="5" placeholder="Message" spellcheck="true" required="required"></textarea>
					</div>
                           <div class="form-group col-md-8 col-md-offset-2">
							<div class="textarea-message form-group">
							<div class="g-recaptcha" data-sitekey="6LeTkicUAAAAAGCGn6MnsPhpccAGPgaR6gb8PPhi"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LeTkicUAAAAAGCGn6MnsPhpccAGPgaR6gb8PPhi&amp;co=aHR0cHM6Ly93d3cuY2FvbndlYi5jb206NDQz&amp;hl=en&amp;v=v1552285980763&amp;size=normal&amp;cb=hkfjkiqn7z5x" width="304" height="78" role="presentation" name="a-hyuc1aim2ouf" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div></div>
						    </div>
							<button class="primary-button button-md " style="border: none;margin-bottom: 32px;" type="submit">Submit Details</button>
						    </div>
							<script src="https://www.google.com/recaptcha/api.js"></script>
							<div style="clear-both;"></div>		
   </div>
</form>
</div>
  <div class="container-fluid">
   <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12 pl-0 pr-0">
         <div class="contact-country-one" style="background-image: url('<?= base_url();?>assets/images/city_4.jpg');">
            <img src="<?= base_url();?>assets/images/india-flag.jpg" alt="CAONWEB in India"> 
            <h4>India</h4>
            <ul class="contact-country">
               <li><i class="fa fa-map-marker"></i>E-36, First Floor, Sector 8, <span>City - Noida 201301</span></li>
               <li><i class="fa fa-phone"></i>(0120) 4231116</li>
               <li><i class="fa fa-clock-o"></i>Mon-Sat 09:30 - 18:00</li>
            </ul>
         </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
         <div class="contact-country-one" style="background-image: url('<?= base_url();?>assets/images/city_2.jpg');">
            <img src="<?= base_url();?>assets/images/hong-flag.gif" alt="CAONWEB in Hongkong">
            <h4>Hongkong</h4>
            <ul class="contact-country">
               <li><i class="fa fa-map-marker"></i>Coming Soon</li>
               <li><i class="fa fa-phone"></i>(0120) 4231116</li>
               <li><i class="fa fa-clock-o"></i>Mon-Sat 09:30 - 18:00</li>
            </ul>
         </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
         <div class="contact-country-one" style="background-image: url('<?= base_url();?>assets/images/city_5.jpg');">
             <img src="<?= base_url();?>assets/images/singa-flag.gif" alt="CAONWEB in Sigapore">
            <h4>Singapore</h4>
            <ul class="contact-country">
               <li><i class="fa fa-map-marker"></i>Coming Soon</li>
               <li><i class="fa fa-phone"></i>(0120) 4231116</li>
               <li><i class="fa fa-clock-o"></i>Mon-Sat 09:30 - 18:00</li>
            </ul>
         </div>
      </div>
   </div>
</div>