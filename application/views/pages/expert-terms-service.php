<div class="page-title-section" style="background-image: url(https://www.caonweb.com/new-images/breadcrumb-images1.png);">
	<div class="container">
		<h1>Terms of Service</h1>
			<ul>
				<li><a href="https://www.caonweb.com/">Home</a></li>
				<li><a href="">Terms of Service</a></li>
			</ul>
	</div>
</div>
<div class="container">
		<div class="row">
		<div class="tab">
  <!--<button class="tablinks" onclick="openCity(event, 'trust')" >Trust & Saftey</button>-->
  <button class="tablinks active" onclick="openCity(event, 'terms')" id="defaultOpen">Terms of Service</button>
  <button class="tablinks" onclick="openCity(event, 'privacy')">Privacy Policy</button>
</div>

<div id="trust" class="tabcontent" style="display: none;">

<h3>Trust and Safety</h3>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
 
</div>

<div id="terms" class="tabcontent" style="display: block;">
 <h3> Terms of Service</h3>
<p>In order to use the Services, you must first agree to the Terms. You may not use the Services if you do not accept the Terms.</p>

 <h3>Provision of the Services by Caonweb </h3>
<ul>
<li>CAONWEB is constantly innovating in order to provide the best possible experience for its users. The form and nature of the Services which CAONWEB provides may change from time to time without prior notice to you.</li>

<li>As part of this continuing innovation, you acknowledge and agree that CAONWEB may stop (permanently or temporarily) providing the Services (or any features within the Services) to you or to users generally at CAONWEB's sole discretion, without prior notice to you. You may stop using the Services at any time. You do not need to specifically inform CAONWEB when you stop using the Services.</li>


<li>You acknowledge and agree that if CAONWEB disables access to your account, you may be prevented from accessing the Services, your account details or any files or other content which is contained in your account.</li>

<li>You agree to allow us to add you as a professional in our website to be listed as an expert for the services you have specified.</li>

</ul>

 <h3 style="margin-top: 40px;">Use of the Services by you (For Professionals)</h3>
 <ul>
<li>You agree to use the Services only for purposes that are permitted by (a) the Terms and (b) any applicable law, regulation or generally accepted practices or guidelines in the relevant jurisdictions (including any laws regarding the export of data or software to and from India).</li>
<li>You agree not to access (or attempt to access) any of the Services by any means other than through the interface that is provided by CAONWEB, unless you have been specifically allowed to do so in a separate agreement with CAONWEB. You specifically agree not to access (or attempt to access) any of the Services through any automated means (including use of scripts or web crawlers) and shall ensure that you comply with the instructions set out in any robots.txt file present on the Services.</li>
<li>	You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services).</li>
<li>	Unless you have been specifically permitted to do so in a separate agreement with CAONWEB, you agree that you will not reproduce, duplicate, copy, sell, trade or resell the Services for any purpose.</li>
<li>	You agree that you are solely responsible for (and that CAONWEB has no responsibility to you or to any third party for) any breach of your obligations under the Terms and for the consequences (including any loss or damage which CAONWEB may suffer) of any such breach.
If you provide any kinds of accounting/tax/compliance/similar nature of services , you should be doing so with the consensus of that individual/legal entity after fully understanding the implications of this action. CAONWEB will not be held responsible if you are doing this without the knowledge / consensus of that person concerned.</li>
</ul>

<h3 style="margin-top: 40px;">Your passwords and account security</h3>
<ul>
<li>	You agree and understand that you are responsible for maintaining the confidentiality of passwords associated with any account you use to access the Services.</li>
<li>	Accordingly, you agree that you will be solely responsible to CAONWEB for all activities that occur under your account.</li>
<li>	If you become aware of any unauthorized use of your password or of your account, you agree to notify CAONWEB immediately</li>

</ul>



<h3 style="margin-top: 40px;">LIMITATION OF LIABILITY</h3>
<ul>
<li>	Subject to overall provision in paragraph above, you expressly understand and agree that caonweb, its subsidiaries and affiliates, and its licensors shall not be liable to you for:</li>

<li>	Any direct, indirect, incidental, special consequential or exemplary damages which may be incurred by you, however caused and under any theory of liability. This shall include, but not be limited to, any loss of profit (whether incurred directly or indirectly), any loss of goodwill or business reputation, any loss of data suffered, cost of procurement of substitute goods or services, or other intangible loss;</li>

<li>	Any loss or damage which may be incurred by you, including but not limited to loss or damage as a result of:</li>


<li> 	Any reliance placed by you on the completeness, accuracy or existence of any advertising, or as a result of any relationship or transaction between you and any advertiser or sponsor whose advertising appears on the services;</li>
<li>	Any changes which caonweb may make to the services, or for any permanent or temporary cessation in the provision of the services (or any features within the services);</li>
<li>	The deletion of, corruption of, or failure to store, any content and other communications data maintained or transmitted by or through your use of the services;</li>
<li>	Your failure to provide caonweb with accurate account information;</li>
<li>	Your failure to keep your password or account details secure and confidential;</li>

</ul>

</div>

<div id="privacy" class="tabcontent" style="display: none;">
 <h3>Privacy Policy</h3>
<p>CAONWEB website does not collect Personal Information about you when you visit the site. You can generally visit the site without revealing Personal Information, when we ask for personal information such as name, email id and contact number, we let you know the specific purpose for which we are asking for the same.</p>

<p>CAONWEB does not share any personal information that we have of the visitor (Which the visitor has an option to disclose or not) to any third party. Any information that is provided to us will be protected from misuse, loss, unauthorised access or disclosure, alteration, or destruction.</p>

<p>CAONWEB gather information such as IP address, domain name, type of browser, operating system, the date and time of visit and the pages visited. We make no attempt to link these addresses with the identity of individuals visiting this website unless an attempt to damage the site has been detected.</p>
</div>
</div>
</div>
<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<section class="intro-area bg-gray" style="background: #126992;">
	<div class="">
		<div class="row">
			<div class="col-xl-12">
				<div class="intro-text text-center">
					<h1 style="color: #fff;">We are always with you to make your project</h1>
					<p style="color: #fff;">Explain to you how all this mistaken idea of denouncing pleasure and praising <br> pain was born
						and I will give you a complete
					</p>
					<a href="https://www.caonweb.com/contact.php" class="primary-button button-md" style="background: #084460 !important;">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section>
