<!DOCTYPE HTML>
  <html class="no-js" lang="de">  
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php //foreach($landmeta as $meta): ?>
	<title><?=$landmeta;?></title>
	<meta name="description" content="<?=$description;?>">
	<meta name="keywords" content="<?=$keyword;?>">

  <?php //endforeach ?>

    <!-- CSS -->
    <link href="<?= base_url();?>landingcss/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
     <link href="<?= base_url();?>landingcss/animate.css" rel="stylesheet">
    <link href="<?= base_url();?>landingcss/bootsnav.css" rel="stylesheet">
    <link href="<?= base_url();?>landingcss/style.css" rel="stylesheet">
    <link href="<?= base_url();?>landingcss/animations.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
   </head>
<body>     
        
    <!-- Start Navigation -->
    <nav class="navbar navbar-default bootsnav navbar-fixed-top">

        <!-- Start Top Search -->
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container">            
            <!-- Start Atribute Navigation -->
             

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
     <a class="navbar-brand" href="<?= base_url();?>"><img src="<?= base_url();?>images/logo.png" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li class="dropdown megamenu-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 1</h6>
                                        <div class="content">
                                          
											<ul class="menu-col">
														<li><a href="https://www.caonweb.com/tds-return.php" target="_blank">TDS Return</a></li>
														<li><a href="https://www.caonweb.com/msme-ssi-registration.php" target="_blank">MSME Registration</a></li>
														<li><a href="https://www.caonweb.com/import-export-code.php" target="_blank">Import Export Code</a></li>
														<li><a href="https://www.caonweb.com/gst-registration.php" target="_blank">GST Registration</a></li>
														<li><a href="https://caonweb.com/gst-filing.php" target="_blank">GST Filing</a></li>
														<li><a href="https://www.caonweb.com/digital-signature-certificate.php" target="_blank">Digital Signature Certificates</a></li>
														<li><a href="https://www.caonweb.com/audit-and-assurance-services.php" target="_blank">Audit and Assurance Services</a></li>
													</ul>
                                        </div>
                                    </div><!-- end col-3 -->
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 2</h6>
                                        <div class="content">
                                            
											<ul class="menu-col">
													
														<li><a href="https://www.caonweb.com/company-registration.php" target="_blank">Company Registration</a></li>
														<li><a href="https://www.caonweb.com/book-keeping-and-outsourcing.php" target="_blank">Book Keeping &amp; Outsourcing</a></li>
														<li><a href="https://www.caonweb.com/apeda-registration.php" target="_blank">APEDA Registration</a></li>
														<li><a href="https://www.caonweb.com/fssai-registration.php" target="_blank">FSSAI Registration</a></li>
														<li><a href="https://www.caonweb.com/trademark-registration.php" target="_blank">Trademark Registration</a></li>
														<li><a href="https://www.caonweb.com/income-tax-return.php" target="_blank">Income Tax Return</a></li>
														<li><a href="https://www.caonweb.com/doing-business-in-india.php" target="_blank">Doing Business in India</a></li>
														<li><a href="https://www.caonweb.com/warehouse-registration.php" target="_blank">Warehouse Registration</a></li>
														
														
													</ul>
                                        </div>
                                    </div><!-- end col-3 -->
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 3</h6>
                                        <div class="content">
                                           
											<ul class="menu-col">
														<li><a href="https://www.caonweb.com/cross-border-transaction.php" target="_blank">Cross Boarder Transaction- Structuring &amp; Taxation</a></li>
														<li><a href="https://www.caonweb.com/fdi-in-india.php" target="_blank">FDI in India</a></li>
														<li><a href="https://www.caonweb.com/nri-services.php" target="_blank">NRI Services</a></li>
														<li><a href="https://www.caonweb.com/iso-registration.php" target="_blank">ISO Registration</a></li>
														<li><a href="https://www.caonweb.com/ngo-services.php" target="_blank">NGO Services</a></li>
													    <li><a href="https://www.caonweb.com/company-secretarial-services.php" target="_blank">Compliance by company</a></li>
													    <li><a href="https://www.caonweb.com/annual-filing.php" target="_blank">ROC / Annual Filling</a></li>
													    <li><a href="https://www.caonweb.com/ca-services.php" target="_blank">All CA Services</a></li>
														 <!--<li><a href="https://www.caonweb.com/importer-of-record-service.php" target="_blank">Importer of Record Service (IOR Service)</a></li>-->
														</ul>
                                        </div>
                                    </div>    
                                </div><!-- end row -->
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Contact US</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>   
 
        <!-- End Side Menu -->
    </nav>