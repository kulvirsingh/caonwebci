<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Chartered accountants, Company secretaries, Tax Experts: CAONWEB</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale= 1.0"/>
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
		<meta name="description" content="CAONWEB is providing free online legal directory where you can find best business tax consultants, chartered accountant & company secretaries in India.">
		
<meta name="Classification" content="Business"/>
<meta name="author" content="info@caonweb.com"/>
<meta name="reply-to" content="info@caonweb.com"/>
<meta name="url" content="https://www.caonweb.com/"/>
<meta name="identifier-URL" content="https://www.caonweb.com"/>
<meta name="category" content="Chartered Accountant"/>
<meta name="coverage" content="Worldwide"/>
<meta name="distribution" content="Global"/>
<meta name="rating" content="General"/>
<meta name="revisit-after" content="7 days"/>
<meta name="target" content="all"/>
<meta name="HandheldFriendly" content="True"/>
<meta name="og:url" content="https://www.caonweb.com"/>
<meta name="og:image" content="https://www.caonweb.com/expert-images/logo4.png"/>
<meta name="og:site_name" content="caonweb"/>
<meta name="og:email" content="info@caonweb.com"/>
<meta name="og:region" content="IN"/>
<meta name="og:country-name" content="INDIA"/>
<meta name="language" content="english"/>
<meta name="subject" content="Company Registration"/>
<meta name="copyright" content="caonweb.com"/>
<meta name="robots" content="index,follow" />
<link rel="canonical" href="https://www.caonweb.com/"/>

		
	<link rel="shortcut icon" href="./assets/images/logo-shortcut.png"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/everything.css">
<!--     <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/icomoon.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/slider.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/animate.css">  
	
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/default.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/new-css/styles.css" id="colors"> -->
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	<link href="<?= base_url();?>assets/css/new-fonts.css" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src='<?= base_url();?>assets/js/jquery-3.2.1.min.js' type='text/javascript'></script>
<script src='<?= base_url();?>assets/js/select2.min.js' type='text/javascript'></script>
<link href='<?= base_url();?>assets/css/select2.min.css' rel='stylesheet' type='text/css'>

<link href="<?= base_url();?>assets/css/custom/drop/animate.css" rel="stylesheet">
<link href="<?= base_url();?>assets/css/custom/drop/bootsnav.css" rel="stylesheet">
<link href="<?= base_url();?>assets/css/custom/drop/style.css" rel="stylesheet">
		
<meta name="google-site-verification" content="hCnjrbGZAgfO1cyjBZ5BoJX5jWThacfTotd-jC4iokM" />
<meta name="msvalidate.01" content="1F9B93060520BC886501ABBC06092967" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-101772897-1"></script>
<link href='<?= base_url();?>assets/css/custom/custom.css' rel='stylesheet' type='text/css'>
<link href='<?= base_url();?>assets/css/custom/custom.css' rel='stylesheet' type='text/css'>


		<!------------------------->
<script type="text/javascript">

function get_Location() {
		//body...
$.get("https://ipinfo.io", function(response) {
        
        $('#selCity').append('<option value="'+response.city+ '"' + ' selected="selected">'+ response.city + '</option>');
        //console.log(response['ip']);
        }, "jsonp")
}

function bookingformValidation(str)                              
{
    var id = "RegForm"+str;
    var name = document.forms[id]["name"];            
    var email = document.forms[id]["email"]; 
    var phone = document.forms[id]["contact"]; 
    var date = document.forms[id]["appointmentdate"];
    var message = document.forms[id]["message"];

    if (name.value == "")                                
    { 
        $error = document.querySelector('.nameField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter your name.";
        name.focus();
        return false; 
    } 

    if (email.value == "")                             
    { 
        $error = document.querySelector('.emailField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter your e-mail address."; 
        name.focus(); 
        return false; 
    }    

    if (email.value.indexOf("@", 0) < 0)                 
    { 
        $error = document.querySelector('.emailField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter a valid e-mail address."; 
        email.focus(); 
        return false; 
    } 

    if (email.value.indexOf(".", 0) < 0)                 
    { 
        $error = document.querySelector('.emailField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter a valid e-mail address."; 
        email.focus(); 
        return false; 
    } 

    if (phone.value == "")                       
    { 
        $error = document.querySelector('.contactField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter your telephone number."; 
        phone.focus(); 
        return false; 
    }

    if (isNaN(phone.value))                       
    { 
        $error = document.querySelector('.contactField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter a valid telephone number."; 
        phone.focus(); 
        return false; 
    }

    if (phone.value.length <10 || phone.value.length > 15)                       
    { 
        $error = document.querySelector('.contactField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter a valid telephone number."; 
        phone.focus(); 
        return false; 
    } 

    if (date.value == "")                    
    { 
        $error = document.querySelector('.dateField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please enter appointment date"; 
        date.focus(); 
        return false; 
    } 

    if (message.value == "")                    
    { 
        $error = document.querySelector('.msgField'+str);
        $error.classList.add('error');
        $error.innerHTML = "Please say something!"; 
        message.focus(); 
        return false; 
    }

    // if (what.selectedIndex < 1)              
    // { 
    //     alert("Please enter your course."); 
    //     what.focus(); 
    //     return false; 
    // }
    var form_id = '#RegForm'+str;
    // $(form_id).on('submit', function (e) {debugger;

    //       e.preventDefault();
    
  $.ajax({
    type: 'post',
    url: '<?php echo site_url('expert-booking-submit'); ?>',
    data: $(form_id).serialize(),
    success: function () {
    $success = document.querySelector('.successField'+str);
    $success.classList.add("success");
    $success.innerHTML = "Form Submitted Successfully";

    document.querySelector('.nameField'+str).innerHTML = "";
    document.querySelector('.emailField'+str).innerHTML = "";
    document.querySelector('.contactField'+str).innerHTML = "";
    document.querySelector('.dateField'+str).innerHTML = "";
    document.querySelector('.msgField'+str).innerHTML = "";
    }
  });
    return false; 
}
</script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/596dd6bb6edc1c10b03467dd/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script--> 
<script src='<?= base_url();?>assets/js/custom/custom.js' type='text/javascript'></script>
</head>
<body onload="consolee()">
	<link href="https://www.caonweb.com/new-css/drop/animate.css" rel="stylesheet">
    <link href="https://www.caonweb.com/new-css/drop/bootsnav.css" rel="stylesheet">
    <link href="https://www.caonweb.com/new-css/drop/style.css" rel="stylesheet">
<div id="preloader">
			<div class="row loader">
				<div class="loader-icon"></div>
			</div>
		</div>
		<div id="top-bar" class="">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-xs-12">
						<div class="top-bar-info">
							<ul>
							 
								<li><i class="fa fa-phone"></i><a href="tel:0120-4231116"  style="color:#fff;">(0120) 4231116</a>
								</li>
								<li><i class="fab fa-whatsapp"></i><a href="https://api.whatsapp.com/send?phone=917065818801" style="color:#fff;" target="_blank">(+91) 7065 818 801</a>
								</li>
								<li class="hidden-xs"><i class="fa fa-envelope"></i><a href="mailto:info@caonweb.com" target="_top" style="color:#fff;" >info@caonweb.com</a>
								</li>
							</ul>
						</div>
						<span class="app" href="https://www.caonweb.com/download-app.php" style="color:#fff;"><i  class="fab fa-android"></i> <a href="https://www.caonweb.com/download-app.php" style="color:#fff;" class="hidden-xs">Download App</a></span>
					</div>
					<div class="col-md-4 col-xs-12 hidden-xs">
						<ul class="social-icons hidden-sm">
							<li><a href="https://www.facebook.com/caonwebonline/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://twitter.com/caonwebonline" target="_blank"><i class="fab fa-twitter"></i></a></li>
							<li><a href="https://www.youtube.com/caonwebonline" target="_blank"><i class="fab fa-youtube"></i></a></li>
							<li><a href="https://plus.google.com/u/0/111339198844639794253" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="https://www.linkedin.com/in/caonweb-online-169b84147/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<header>
			  
    <!-- Start Navigation -->
    <nav class="navbar navbar-default bootsnav  ">

        <!-- Start Top Search -->
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container"> 
<div class="attr-nav">
<ul class="  navbar-right  right-si">
									<li><a href="https://www.caonweb.com/expert-login.php" style="float: left;">Expert Sign IN /</a>
									<a href="https://www.caonweb.com/expert-signup.php" style="float: left;margin-left: -12px;margin-top: -1px;">Sign Up</a></li>
								</ul>
              
            </div>		
            <!-- Start Atribute Navigation -->
             
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?= base_url();?>"><img src="https://www.caonweb.com/expert-images/logo4.png" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                   <li><a href="<?= base_url();?>top-expert-india.php" target="_blank">Book Appointment</a></li>
                   <li><a href="<?= base_url();?>contact.php">Contact us</a></li>
                <!--  <li><a href="">Chat with Expert</a></li>-->
                   
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Other</a>
                        <ul class="dropdown-menu">
                            <li><a href="https://blog.caonweb.com/">Blog</a></li>
                            <!--<li><a href="#">Latest Tax Rates</a></li>
                            
                            <li><a href="#">Latest updates/notifications</a></li>-->
                           
                        </ul>
                    </li> 
					  <li><a href="https://www.caonweb.com/gallery">Gallery</a></li> 
					<li><a href="<?= base_url();?>company-registration.php" class="dir1" target="_blank" >Company Registration</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>   
 
        <!-- End Side Menu -->
    </nav>
			
	

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M37M9BQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/596dd6bb6edc1c10b03467dd/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


		</header>
