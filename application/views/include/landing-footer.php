  <section class="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12">
			 <h2>Need Help? Call Us 24/7</h2><span>0120 - 4231116</span>
			</div>
			<div class="col-md-6 col-xs-12">
				<input type="text" placeholder="Enter Email ID"><button type="submit">SUBMIT</button>
			</div>
		</div>
	</div>
</section>   
    
    
    
  <footer class="footer page-section-pt black-bg">
 <div class="container">
  <div class="row">
      <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Navigation</h6>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Service</a></li>
          <li><a href="#">Team</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Useful Link</h6>
        <ul>
          <li><a href="#">Create Account</a></li>
          <li><a href="#">Company Philosophy</a></li>
          <li><a href="#">Corporate Culture</a></li>
          <li><a href="#">Portfolio</a></li>
          <li><a href="#">Client Management</a></li>
        </ul>
      </div>
    </div>
	
	<div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Services</h6>
        <ul>
          <li><a href="#">TDS Return</a></li>
          <li><a href="#">GST Registration</a></li>
          <li><a href="#">GST Filing</a></li>
          <li><a href="#">FSSAI Registration</a></li>
          <li><a href="#">FDI in India</a></li>
        </ul>
      </div>
    </div>
    
	 <div class="col-lg-4 col-sm-6 xs-mb-30">
    <h6 class="text-white mb-30 mt-10 text-uppercase">Contact Us</h6>
    <ul class="addresss-info"> 
        <li><i class="fa fa-map-marker"></i> <p>Address: E-36, Sector 8 Noida UP 201301</p> </li>
        <li><i class="fa fa-phone"></i> <a href="tel:01204231116"> <span>0120 - 4231116 </span> </a> </li>
        <li><i class="far fa-envelope"></i>Email: info@caonweb.com</li>
      </ul>
    </div> 
	
	<div style="clear:both;"></div>
      
      <div class="footer-widget mt-20">
         
          <div class="col-lg-6 col-md-6">
           <p class="mt-15"> ©Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2019</span> <a href="#"> CAONWEB </a> All Rights Reserved </p>
          </div>
          <div class="col-lg-6 col-md-6 text-left text-md-right">
            <div class="social-icons color-hover mt-10">
             <ul> 
              <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li class="social-dribbble"><a href="#"><i class="fab fa-linkedin-in"></i> </a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-youtube"></i></a></li>
             </ul>
           </div>
          </div>
          
      </div>
  </div> </div>
</footer>  

    <!-- Placed at the end of the document so the pages load faster -->
   
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://www.caonweb.com/js/lib/bootstrap.min.js"></script>
<script src="<?= base_url();?>landingjs/bootstrap.min.js"></script>
<!-- Bootsnavs -->
<script src="<?= base_url();?>landingjs/bootsnav.js"></script>
<script type="text/javascript" src="<?= base_url();?>landingjs/css3-animate-it.js"></script>
 
 
 
 
</body>
</html>