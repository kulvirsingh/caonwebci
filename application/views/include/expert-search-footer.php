	<footer class="bg-img background-size-contain" style="background-image: url('https://www.caonweb.com/new-images/foot-map.jpg');">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="border-right">
							<h3 class="footer-title-style7 text-white">Contact</h3>
							<ul class="footer-list">
								<li> <i class="fas fa-map-marker-alt"></i>&nbsp; E-36, First Floor, Sector 8, City - Noida 201301</li>
								<li><i class="fas fa-mobile-alt text-theme-color"></i>&nbsp;<a href="tel:0120-4231116">&nbsp; (0120) 4231116 </li>
								<li>   <i class="far fa-envelope text-theme-color"></i> &nbsp; <a href="mailto:info@caonweb.com" target="_top" > info@caonweb.com </a></li>
								<li> <i class="fas fa-globe text-theme-color"></i> &nbsp; <a href="https://www.caonweb.com/"> www.caonweb.com </a></li>
							</ul>
							<div class="footer-social-icons">
								<ul>
									<li><a href="https://www.facebook.com/caonwebonline/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="https://twitter.com/caonwebonline" target="_blank"><i class="fab fa-twitter"></i></a></li>
									<li><a href="https://plus.google.com/u/0/+caonwebonline" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
									<li><a href="https://www.youtube.com/caonwebonline" target="_blank"><i class="fab fa-youtube"></i></a></li>
									<li><a href="https://www.linkedin.com/in/caonweb-online-169b84147/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="height-100 border-right">
							<h3 class="footer-title-style7 text-white">Quick Links</h3>
							<ul class="footer-list list-none">
							<li><a href="https://caonweb.com/about.php" target="_blank">About us</a></li>
							<li><a href="https://blog.caonweb.com/" target="_blank">Blog</a></li>
							 
								<!--<li><a href="https://caonweb.com/expert-trust-safety.php" target="_blank">Trust & Saftey</a></li>-->
 								<li><a href="https://www.caonweb.com/expert-terms-service.php" target="_blank">Terms of Service</a></li>
								<li><a href="https://www.caonweb.com/expert-privacy-policy.php" target="_blank">Privacy Policy </a></li> 
								<li><a href="https://www.caonweb.com/contact.php" target="_blank">Contact Us </a></li> 
							 
							 
								<li>&nbsp;</li>
								<!--<li><a href="" target="_blank">Doing Business in India</a></li>
								<li><a href="https://www.caonweb.com/tax-rates.php" target="_blank">Tax rates in India</a></li>
								<li><a href="https://www.caonweb.com/corporate-compliance.php" target="_blank">Corporate Compliance</a></li> -->
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group footer-subscribe ">
							<h3 class="footer-title-style7 text-white">Sign Up For a Newsletter</h3>
							<p class=" ">Weekly breaking news, analysis and cutting edge advices on job searching.</p>
				   <div  style="color: #81b84d;"><b id="response"></b></div>
				   <form action="https://www.caonweb.com/expert-newsletter-send.php" method="post" id="newsletter" name="newsletter" class="width-85" style="position:relative;">
                    <input type="email" name="signup-email" id="signup-email" value="" placeholder="Insert email here" class="form-control">
                    <input type="submit" value="Subscribe" name="signup-button" id="signup-button" style="background: #81b84d;" class="butn theme grey-hover bt">
                    <span class="arrow"></span>
                   </form>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bar bg-transparent border-top border-color-light-white position-relative z-index-1">
				<div class="container">
					<p>© 2018 CA ON WEB PVT.LTD.. All Rights Reserved.</p>
				</div>
			</div>
		</footer>
		
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>

<script>
        $(document).ready(function(){
            
            // Initialize select2
            $("#selUser").select2();
            $("#selCity").select2();
			$(".selService").select2();
            $(".select2-container").css("width","100%");

            $(".select2-container--default.select2-container--open").removeAttribute("style");

            $("sapn.select2-search:first-child").attr("placeholder", "Search Services");
            // $('#expert-section').val() = "";

        });

        $(document).on("mouseover mouseout",".appoint", function(){
        	$('.selService').select2();
        	$(".select2-container").css("width","100%");

            $(".select2-container--default.select2-container--open").removeAttribute("style");

            $("sapn.select2-search:first-child").attr("placeholder", "Search Services");

            bookingformValidation(str);
  			// alert("success");
		});

		$(document).on('mouseover mouseout', window, function(){
			// alert('hello world');debugger;
		$("div.expert-search-filter").slice(0,3).show();
		});

		$(document).on('click', '.appoint', function(){
			var example = flatpickr('#flatpickr');
		});

		// $(document).on('click', '#loadMore', function(){
  //       	if($("div#expertBox:hidden").length > 0){ // check if any hidden divs still exist
  //       	$("div#expertBox:hidden").slice(0, 3).show(); // select next 5 hidden divs and show them
  //       	}else {
  //       	alert("No more Experts"); // alert if there are none left
  //       	}
		// });

</script>

<script>

$(function(){

	if( $('#expert-section').val() === "") {
		$("div.expertBox").slice(0, 5).show(); // select the first five
	    $("#loadMore").click(function(e){ // click event for load more
	        e.preventDefault();
	        if($("div.expertBox:hidden").length > 0){// check if any hidden divs still exist
	        	$("div.expertBox:hidden").slice(0, 5).show(); // select next 5 hidden divs and show them
	        }else {
	        	alert("No more Experts"); // alert if there are none left
	        }
	    });
	} 
	
	$(document).on("click","#filterMore", function(){
	if($("div.expert-search-filter:hidden").length > 0){
		$("div.expert-search-filter:hidden").slice(0, 3).show();
	}else {
		alert("No more Experts");
	}

	});

	// $('#bootap1').submit(function (evt) {
 //    evt.preventDefault();
 //    return false;
 //    // window.history.back();
	// });
});
</script>
<script src="<?= base_url();?>assets/src/flatpickr.js">debugger;</script>
<script>var example = flatpickr('#flatpickr');</script>		
<a href="#" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
<!-- <script src="<?= base_url();?>assets/js/waypoints.min.js"></script> -->
<script src="<?= base_url();?>assets/js/drop/drop.js"></script>
<script src="<?= base_url();?>assets/js/main.js"></script>

</body>

		<!----------schema--------------->

		<script src="<?= base_url();?>assets/js/main.js"></script>
		
<!-- <script src="jquery-2.1.3.min.js" type="text/javascript"  ></script> -->
</html>