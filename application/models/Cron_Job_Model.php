<?php

class Cron_Job_Model extends CI_Model
{

	public function expertTotalQueries($exp_id) {

		$total = $this->db->query("SELECT * FROM expert_book_appointment WHERE exp_id =" . $exp_id);

		return $total->num_rows($exp_id);
	}

	public function expertAcceptedQueries ($exp_id) {

		$total = $this->db->query("SELECT * FROM appointment_query_status
								  WHERE exp_id =" . $exp_id ."  AND (status = 'complete' OR status = 'in progress')");

		return $total->num_rows();
	}

	public function expertExpiredQueries ($exp_id) {

		$total = $this->db->query("SELECT * FROM appointment_query_status
								  WHERE exp_id =" . $exp_id ."  AND status = 'expired'");

		return $total->num_rows();
	}

	public function expertRejectedQueries ($exp_id) {

		$total = $this->db->query("SELECT * FROM expert_book_appointment
								  WHERE exp_id =" . $exp_id ."  AND status = 'rejected'");

		return $total->num_rows();
	}


	public function crawler($exp_id) {

		$totalQueries = $this->expertTotalQueries($exp_id);
		$totalAcceptedQueries = $this->expertAcceptedQueries($exp_id);
		$totalExpiredQueries = $this->expertExpiredQueries($exp_id);
		$totalRejectedQueries = $this->expertRejectedQueries($exp_id);

		echo "Total Queries: " .$totalQueries . " Accepted Queries: " . $totalAcceptedQueries . "<br/>";
		echo "Expired Queries: " .$totalExpiredQueries . " Rejected Queries: " . $totalRejectedQueries . "<br/>";
	}

}