<?php
	
	class Expert_Search_Model extends CI_Model{
		public function __construct(){
			$this-> load-> database();
		}

		public function get_countries() {
			$this->db->select('DISTINCT (country)');
			$this->db->select('country_id');
			$query = $this->db->get('expert_service_country');
			return $query->result_array();
		}

		public function get_services() {
			$country = htmlentities($this->input->get('country'));
			$country = explode(' ', $country);
			$country_id = $country[0];
			$this->db->select('DISTINCT (service)');
			$this->db->where('country_id', $country_id);
			$query = $this->db->get('expert_service_country');
			return $query->result_array();
		}

		public function default_services() {
			
			$this->db->select('DISTINCT (service)');
			$query = $this->db->get('expert_service_country');
			return $query->result_array();
		}

		public function default_service() {
			
			$this->db->select('DISTINCT (service_name)');
			$query = $this->db->get('service');
			return $query->result_array();
		}
		
		public function get_expert() {

			// $service = array('service' => $service_name);
			$country = htmlentities($this->input->get('country'));
			$service_name = htmlentities($this->input->get('services'));

			$country = explode(' ', $country);
			$country_id = $country[0];

			// $where = array('country_id' => $country_id, 'service' => "%"."TDS Consulting"."%");
			$where = 'country_id =' . ' "' . $country_id .'"' . ' && service LIKE'. '"%' . $service_name . '%"';
			//WHERE country_id = "14" && service LIKE "%TDS Consulting%"
			$this->db->select('*');
			$this->db->where($where);
			$query = $this->db->get('expert_login_table');
			return $query->result_array();
		}

		public function get_sponsored_expert() {

            // $service = array('service' => $service_name);
               $country = htmlentities($this->input->get('country'));
            // $service_name = htmlentities($this->input->get('services')); // future update if required

            $country = explode(' ', $country);
            $country_id = $country[0];
 
			// future update if required (filter sponsor country and sevices wise)

			//$where = 'country_id =' . ' "' . $country_id .'"' . ' && service LIKE'. '"%' . $service_name . '%"';

			// future update if required

            $this->db->select('*');
            $this->db->where('country_id', $country_id);
            $query = $this->db->get('sponsored_expert_table');
            return $query->result_array();
        }

		public function submitBooking() {
			
			$data = array(
			'exp_id' => $this-> input->post('exp_id'),
			'name' => $this-> input->post('name'),
			'email' => $this-> input->post('email'),
			'appointmentdate' => $this-> input->post('appointmentdate'),
			'service' => $this-> input ->post('bookservice'),
			'contact' => $this-> input->post('contact'),
			'message' => $this-> input->post('message'),
			'date' => date('Y-m-d H:i:s')
			);
			$this->send_client_mail($data);
			$this->send_expert_mail($data);
			return $this->db->insert('expert_book_appointment',$data);
		}



		// ====================== Client Mail Start=========================== // 

		public function send_client_mail($data) {

			/*============= Fetch Expert Detail =====================*/
			$expert = array();
			$this->db->select('name');
			$this->db->select('email');
            $this->db->where('id', $data['exp_id']);
            $query = $this->db->get('expert_login_table');
            $expert = $query->result_array();

            /*============= Fetch Expert Detail =====================*/

            /*============= Send Client Mail =====================*/
            $config = Array(
		    'protocol' => 'smtp',
		    // 'smtp_host' => 'mail.caonweb.com',
		    'useragent' => 'CAONWEB',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'it@caonweb.com',
		    'smtp_pass' => 'iit@caonweb',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			// $this->load->library('email');

			$this->email->from('noreply@caonweb.com');
			$this->email->to($data['email'], $data['name']);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');

			$this->email->subject('Book Appointment Request | CAONWEB');


			$message.="<body style='Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ececec;'>";
			$message.="<center class='wrapper' style='width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ececec;'>";
			$message.="<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background-color:#ececec;' bgcolor='#ececec;'>";
			$message.="<tr>";
			$message.="<td width='100%'><div class='webkit' style='max-width:600px;Margin:0 auto;'>";
			$message.="<table class='outer' align='center' cellpadding='0' cellspacing='0' border='0' style='border-spacing:0;Margin:0 auto;width:100%;max-width:600px;'>";
			$message.="<tr>";
			$message.="<td style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>";
			$message.="<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
			$message.="<tr>";
			$message.="<td><table style='width:100%;' cellpadding='0' cellspacing='0' border='0'>";
			$message.="<tbody>";
			$message.="<tr>";
			$message.="<td align='center'><center>";
			$message.="<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0' style='Margin: 0 auto;'>";
			$message.="<tbody>";
			$message.="<tr>";
			$message.="<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>";
			$message.="<table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-spacing:0'>";
			$message.="<tr>";
			$message.="<td height='6' bgcolor='#2e71f0' class='contents' style='width:100%; border-top-left-radius:10px; border-top-right-radius:10px'></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</td>";
			$message.="</tr>";
			$message.="</tbody>";
			$message.="</table>";
			$message.="</center>";
			$message.="</td>";
			$message.="</tr>";
			$message.="</tbody>";
			$message.="</table>";
			$message.="</td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
			$message.="<tr>";
			$message.="<td><table style='width:100%;' cellpadding='0' cellspacing='0' border='0'>";
			$message.="<tbody>";
			$message.="<tr>";
			$message.="<td align='center'><center>";
			$message.="<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0' style='Margin: 0 auto;'>";
			$message.="<tbody>";
			$message.="<tr>";
			$message.="<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' bgcolor='#FFFFFF'>";
			$message.="<table cellpadding='0' cellspacing='0' border='0' width='100%'>";
			$message.="<tr>";
			$message.="<td class='two-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:1px;text-align:center;font-size:0;'>";
			$message.="<div class='column' style='width:100%;max-width:185px;display:inline-block;vertical-align:top;'>";
			$message.="<table class='contents' style='border-spacing:0; width:100%'  bgcolor='#ffffff' >";
			$message.="<tr>";
			$message.="<td style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' align='left'><a href='#' target='_blank'><img src='https://www.caonweb.com/vs/mailer-ds/images/website-logo.png' alt='' style='border-width:0; width:100%;height:auto; display:block' /></a></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</div>";
			$message.="</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' style='padding-left:40px'><table border='0' cellpadding='0' cellspacing='0' style='border-bottom:2px solid #2e71f0' align='center'>";
			$message.="<tr>";
			$message.="<td height='20' width='30' style='font-size: 20px; line-height: 20px;'>&nbsp;</td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td>&nbsp;</td>";
			$message.="</tr>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="</tbody>";
			$message.="</table>";
			$message.="</center></td>";
			$message.="</tr>";
			$message.="</tbody>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="<table class='one-column' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-spacing:0' bgcolor='#FFFFFF'>";
			$message.="<tr>";
			$message.="<td align='left' style='padding:0px 40px 40px 40px'><p style='color:#000; font-size:22px; text-align:left; font-family: Verdana, Geneva, sans-serif'>Dear ". $data['name'] ." , </p>";
			$message.="<p style='color:#5b5f65; font-size:15px; text-align:left; font-family: Trebuchet MS; line-height: 20px'>Thank you for reaching out to CAONWEB and approaching CA ". $expert[0]['name'] . " for " . $data['service'] . " ,</p>";
			$message.="<b style='color:#5b5f65; font-size:18px; text-align:left; font-family: font-weight: bold; Trebuchet MS; line-height: 20px'><b style='font-size:18px;'>Details of your booking request:</b>";
			$message.="<br>";
			$message.="<br>";
			$message.="<table cellpadding='3'cellspacing='0' border='0' width='100%'>";
			$message.="<tr>";
			$message.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Name</label></td>";
			$message.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['name'] . "</td>";
			$message.="</tr>";
			$message.="<br>";
			$message.="<tr>";
			$message.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Booking date</label></td>";
			$message.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['appointmentdate'] . "</td>";
			$message.="</tr>";
			$message.="<br>";
			$message.="<tr>";
			$message.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Service Type</label></td>";
			$message.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['service'] . "</td>";
			$message.="</tr>";
			$message.="<br>";
			$message.="<tr>";
			$message.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Query</label></td>";
			$message.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['message'] . " </td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="<br>";
			$message.="<p style='color:#5b5f65; font-size:15px; text-align:left; font-family: Trebuchet MS; line-height: 20px'><b>CA ON WEB PVT LTD</b> is the best way to find Tax & Accounting Experts online with more than 1000 Experts registered  .
								  At this platform, we are helping to create business success stories.<br />
			                        <br />
			                      <b>Regards,</b><br><br>
			                       CA ON WEB PVT LTD</p>";
			$message.="<center>";
			$message.="<table cellpadding='0' cellspacing='0' border='0' width='100%'>";
			$message.="<tr>";
			$message.="<td><table border='0' cellpadding='0' cellspacing='0'>";
			$message.="<tr>";
			$message.="<td height='20' width='100%' style='font-size: 20px; line-height: 20px;'>&nbsp;</td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="<table border='0' align='center' cellpadding='0' cellspacing='0' style='Margin:0 auto;'>";
			$message.="<tbody>";
			$message.="<tr>";
			$message.="<td align='center'><table border='0' cellpadding='0' cellspacing='0' style='Margin:0 auto;'>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="</tbody>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</center>";
			$message.="</td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			$message.="<tr>";
			$message.="<td><table width='100%' cellpadding='0' cellspacing='0' border='0'  bgcolor='#2e71f0'>";
			$message.="<tr>";
			$message.="<td height='40' align='center' bgcolor='#2e71f0' class='one-column'>&nbsp;</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>CA ON WEB PVT LTD,|NOIDA |DELHI </font></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:5px;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>Mail at <u><a href='mailto:support@caonweb.com' target='_blank' style='color: #fff;'>info@caonweb.com</a></u></font></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:5px;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>Contact: +91 1204231116</font></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'><table width='150' border='0' cellspacing='0' cellpadding='0'>";
			$message.="<tr>";

			$message.="<td width='33' align='center'><a href='https://www.facebook.com/caonwebonline/' target='_blank'>

			<img src='http://www.caservicesonline.com/social-icon/ico1.png' alt='facebook' width='32' height='32' border='0'/></a></td>";

			$message.="<td width='34' align='center'><a href='https://twitter.com/caonwebonline' target='_blank'>

			<img src='http://www.caservicesonline.com/social-icon/ico2.png' alt='twitter' width='32' height='32' border='0'/></a></td>";

			$message.="<td width='33' align='center'><a href='https://www.linkedin.com/in/caonweb-online-169b84147/' target='_blank'>
			<img src='http://www.caservicesonline.com/social-icon/ico3.png' alt='linkedin' width='32' height='32' border='0'/></a></td>";

			$message.="</tr>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>All rights reserved. </font></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td height='6' bgcolor='#2e71f0' class='contents1' style='width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px'></td>";
			$message.="</tr>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td><table width='100%' cellpadding='0' cellspacing='0' border='0'>";
			$message.="<tr>";
			$message.="<td height='6' bgcolor='#2e71f0' class='contents' style='width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px'></td>";
			$message.="</tr>";
			$message.="<tr>";
			$message.="<td>&nbsp;</td>";
			$message.="</tr>";
			$message.="</table></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</div></td>";
			$message.="</tr>";
			$message.="</table>";
			$message.="</center>";
			$message.="</body>";

			$this->email->message($message);
			$this->email->send();
			$this->client_sms($data, $expert);
		}

		// ====================== Client Mail End =========================== //


		// ====================== Send Expert Mail Start ==================== //

		public function send_expert_mail($data) {

			/*============= Fetch Expert Detail =====================*/
			$expert = array();
			$this->db->select('name');
			$this->db->select('email');
			$this->db->select('phone');
            $this->db->where('id', $data['exp_id']);
            $query = $this->db->get('expert_login_table');
            $expert = $query->result_array();

            /*============= Fetch Expert Detail =====================*/

            /*============= Send Expert Mail =====================*/
            $config = Array(
		    'protocol' => 'smtp',
		    // 'smtp_host' => 'mail.caonweb.com',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'it@caonweb.com',
		    'smtp_pass' => 'iit@caonweb',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			// $this->load->library('email');

			$this->email->from('noreply@caonweb.com');
			$this->email->to($expert[0]['email'], $expert[0]['name']);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');

			$this->email->subject('Book Appointment Request | CAONWEB');

			$emessage.="<body style='Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#ececec;'>";
			$emessage.="<center class='wrapper' style='width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ececec;'>";
			$emessage.="<table width='100%' cellpadding='0' cellspacing='0' border='0' style='background-color:#ececec;' bgcolor='#ececec;'>";
			$emessage.="<tr>";
			$emessage.="<td width='100%'><div class='webkit' style='max-width:600px;Margin:0 auto;'>";
			$emessage.="<table class='outer' align='center' cellpadding='0' cellspacing='0' border='0' style='border-spacing:0;Margin:0 auto;width:100%;max-width:600px;'>";
			$emessage.="<tr>";
			$emessage.="<td style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>";
			$emessage.="<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
			$emessage.="<tr>";
			$emessage.="<td><table style='width:100%;' cellpadding='0' cellspacing='0' border='0'>";
			$emessage.="<tbody>";
			$emessage.="<tr>";
			$emessage.="<td align='center'><center>";
			$emessage.="<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0' style='Margin: 0 auto;'>";
			$emessage.="<tbody>";
			$emessage.="<tr>";
			$emessage.="<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>";
			$emessage.="<table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-spacing:0'>";
			$emessage.="<tr>";
			$emessage.="<td height='6' bgcolor='#2e71f0' class='contents' style='width:100%; border-top-left-radius:10px; border-top-right-radius:10px'></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="</tbody>";
			$emessage.="</table>";
			$emessage.="</center>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="</tbody>";
			$emessage.="</table>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
			$emessage.="<tr>";
			$emessage.="<td><table style='width:100%;' cellpadding='0' cellspacing='0' border='0'>";
			$emessage.="<tbody>";
			$emessage.="<tr>";
			$emessage.="<td align='center'><center>";
			$emessage.="<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0' style='Margin: 0 auto;'>";
			$emessage.="<tbody>";
			$emessage.="<tr>";
			$emessage.="<td class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' bgcolor='#FFFFFF'>";
			$emessage.="<table cellpadding='0' cellspacing='0' border='0' width='100%'>";
			$emessage.="<tr>";
			$emessage.="<td class='two-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:1px;text-align:center;font-size:0;'>";
			$emessage.="<div class='column' style='width:100%;max-width:185px;display:inline-block;vertical-align:top;'>";
			$emessage.="<table class='contents' style='border-spacing:0; width:100%'  bgcolor='#ffffff' >";
			$emessage.="<tr>";
			$emessage.="<td style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;' align='left'><a href='#' target='_blank'><img src='https://www.caonweb.com/vs/mailer-ds/images/website-logo.png' alt='' style='border-width:0; width:100%;height:auto; display:block' /></a></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</div>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' style='padding-left:40px'><table border='0' cellpadding='0' cellspacing='0' style='border-bottom:2px solid #2e71f0' align='center'>";
			$emessage.="<tr>";
			$emessage.="<td height='20' width='30' style='font-size: 20px; line-height: 20px;'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="</tbody>";
			$emessage.="</table>";
			$emessage.="</center></td>";
			$emessage.="</tr>";
			$emessage.="</tbody>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="<table class='one-column' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-spacing:0' bgcolor='#FFFFFF'>";
			$emessage.="<tr>";
			$emessage.="<td align='left' style='padding:0px 40px 40px 40px'><p style='color:#000; font-size:22px; text-align:left; font-family: Verdana, Geneva, sans-serif'>Dear Expert" . $expert[0]['name'] . " , </p>";
			$emessage.="<p style='color:#5b5f65; font-size:15px; text-align:left; font-family: Trebuchet MS; line-height: 20px'>Please login to CAONWEB panel to proceed with your new appointment ,</p>";
			$emessage.="<b style='color:#5b5f65; font-size:18px; text-align:left; font-family: font-weight: bold; Trebuchet MS; line-height: 20px'><b style='font-size:18px;'>Details of your booking request:</b>";
			$emessage.="<br>";
			$emessage.="<br>";
			$emessage.="<table cellpadding='3'cellspacing='0' border='0' width='100%'>";
			$emessage.="<br>";
			$emessage.="<tr>";
			$emessage.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Booking date</label></td>";
			$emessage.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['appointmentdate'] . "</td>";
			$emessage.="</tr>";
			$emessage.="<br>";
			$emessage.="<tr>";
			$emessage.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Service Type</label></td>";
			$emessage.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['services'] . "</td>";
			$emessage.="</tr>";
			$emessage.="<br>";
			$emessage.="<tr>";
			$emessage.="<td width='20%' style='color:#000; font-size:14px; text-align:left;font-weight: bold; font-family: Trebuchet MS'><label>Query</label></td>";
			$emessage.="<td style='color:#5b5f65; font-size:14px; text-align:left; font-family: Trebuchet MS'>" . $data['message'] . " </td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="<br>";
			$emessage.="<p style='color:#5b5f65; font-size:15px; text-align:left; font-family: Trebuchet MS; line-height: 20px'><b>CA ON WEB PVT LTD</b> is the best way to find Tax & Accounting Experts online with more than 1000 Experts registered  .
								  At this platform, we are helping to create business success stories.<br />
			                        <br />
			                      <b>Regards,</b><br><br>
			                       CA ON WEB PVT LTD</p>";
			$emessage.="<center>";
			$emessage.="<table cellpadding='0' cellspacing='0' border='0' width='100%'>";
			$emessage.="<tr>";
			$emessage.="<td><table border='0' cellpadding='0' cellspacing='0'>";
			$emessage.="<tr>";
			$emessage.="<td height='20' width='100%' style='font-size: 20px; line-height: 20px;'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="<table border='0' align='center' cellpadding='0' cellspacing='0' style='Margin:0 auto;'>";
			$emessage.="<tbody>";
			$emessage.="<tr>";
			$emessage.="<td align='center'><table border='0' cellpadding='0' cellspacing='0' style='Margin:0 auto;'>";
			$emessage.="<tr>";
			$emessage.="<td width='250' height='60' align='center' bgcolor='#2e71f0'><a href='https://www.caonweb.com/login.php' target='_blank' style='width:250; display:block; text-decoration:none; border:0; text-align:center; font-weight:bold;font-size:18px; font-family: Arial, sans-serif; color: #ffffff; background:#2e71f0' class='button_link'>Expert Sign In</a></td>";
			$emessage.="</tr>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="</tbody>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</center>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			$emessage.="<tr>";
			$emessage.="<td><table width='100%' cellpadding='0' cellspacing='0' border='0'  bgcolor='#2e71f0'>";
			$emessage.="<tr>";
			$emessage.="<td height='40' align='center' bgcolor='#2e71f0' class='one-column'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>CA ON WEB PVT LTD,|NOIDA |DELHI  </font></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:5px;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>Mail at <u><a href='mailto:support@caonweb.com' target='_blank' style='color: #fff;'>info@caonweb.com</a></u></font></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:5px;padding-right:0;padding-left:0;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>Contact: +91 1204231116</font></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'><table width='150' border='0' cellspacing='0' cellpadding='0'>";
			$emessage.="<tr>";

			$emessage.="<td width='33' align='center'><a href='https://www.facebook.com/caonwebonline/' target='_blank'>

			<img src='http://www.caservicesonline.com/social-icon/ico1.png' alt='facebook' width='32' height='32' border='0'/></a></td>";

			$emessage.="<td width='34' align='center'><a href='https://twitter.com/caonwebonline' target='_blank'>

			<img src='http://www.caservicesonline.com/social-icon/ico2.png' alt='twitter' width='32' height='32' border='0'/></a></td>";

			$emessage.="<td width='33' align='center'><a href='https://www.linkedin.com/in/caonweb-online-169b84147/' target='_blank'>
			<img src='http://www.caservicesonline.com/social-icon/ico3.png' alt='linkedin' width='32' height='32' border='0'/></a></td>";

			$emessage.="</tr>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;'><font style='font-size:13px; text-decoration:none; color:#ffffff; font-family: Verdana, Geneva, sans-serif; text-align:center'>All rights reserved. </font></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td align='center' bgcolor='#2e71f0' class='one-column' style='padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td height='6' bgcolor='#2e71f0' class='contents1' style='width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px'></td>";
			$emessage.="</tr>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td><table width='100%' cellpadding='0' cellspacing='0' border='0'>";
			$emessage.="<tr>";
			$emessage.="<td height='6' bgcolor='#2e71f0' class='contents' style='width:100%; border-bottom-left-radius:10px; border-bottom-right-radius:10px'></td>";
			$emessage.="</tr>";
			$emessage.="<tr>";
			$emessage.="<td>&nbsp;</td>";
			$emessage.="</tr>";
			$emessage.="</table></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</div></td>";
			$emessage.="</tr>";
			$emessage.="</table>";
			$emessage.="</center>";
			$emessage.="</body>";

			$this->email->message($emessage);
			$this->email->send();
			// sleep(2);
			$this->expert_sms($data, $expert);

			}	

		// ====================== Send Expert Mail Start ==================== //



		// ==================== Client SMS ================================== //
		
		public function client_sms($data, $expert) {

		    $param['uname'] = 'yourdigisign'; 

            $param['password'] = 'yourdigisign'; 
            
            $param['sender'] = 'CAONWB'; 
            
            $param['receiver'] = $data['contact']; 
            
            $param['route'] = 'TA'; 
            
            $param['msgtype'] = 1; 
            
            $param['sms'] = 'Thank you for reaching out to CAONWEB. Our Expert, '. $expert[0]['name'] .' will get in touch with you shortly'; 
            
            $parameters = http_build_query($param); 
            
            $url="http://manage.staticking.net/index.php/Bulksmsapi/httpapi"; 
            
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch,CURLOPT_HEADER, false); 
            curl_setopt($ch, CURLOPT_POST, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); 
            curl_setopt($ch, CURLOPT_POSTFIELDS,$parameters); 
            $result = curl_exec($ch);
		}

		// ==================== Client SMS ================================== //

		// ==================== Expert SMS ================================== //

		public function expert_sms($data, $expert) {

		    $param['uname'] = 'yourdigisign'; 

            $param['password'] = 'yourdigisign'; 
            
            $param['sender'] = 'CAONWB'; 
            
            $param['receiver'] = $expert[0]['phone'];
            
            $param['route'] = 'TA'; 
            
            $param['msgtype'] = 1; 
            
            $param['sms'] = 'Please login to CAONWEB panel to proceed with your new appointment. Thank You'; 
            
            $parameters = http_build_query($param); 
            
            $url="http://manage.staticking.net/index.php/Bulksmsapi/httpapi"; 
            
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch,CURLOPT_HEADER, false); 
            curl_setopt($ch, CURLOPT_POST, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); 
            curl_setopt($ch, CURLOPT_POSTFIELDS,$parameters); 
            $result = curl_exec($ch);
		}

		// ==================== Expert SMS ================================== //

		public function get_expert_by_id($id) {
			
			$this->db->select('*');
			$this->db->where('id', $id);
			$query = $this->db->get('expert_login_table');
			return $query->result_array();
		}

		// this is a test result 

		public function get_expert_filter($city = false, $services = false) {

			// $service = array('service' => $service_name);
			// $country = htmlentities($this->input->get('country'));
			// $service_name = htmlentities($this->input->get('services'));

			$country = explode(' ', $country);
			$country_id = $country[0];

			// $where = array('country_id' => $country_id, 'service' => "%"."TDS Consulting"."%");
			$where = 'city =' . ' "' . $city .'"' . ' && service LIKE'. '"%' . $services . '%"';
			//WHERE country_id = "14" && service LIKE "%TDS Consulting%"
			$this->db->select('*');
			$this->db->where($where);
			$query = $this->db->get('expert_login_table');
			return $query->result_array();
		}
		
		public function get_expert_service() {
			
			$this->db->select('*');
			//$this->db->where('service_name', $page_title);
			$query = $this->db->get('service');
			return $query->result_array();
		}
		
		public function get_expert_citys() {
			
			$this->db->select('*');
			$this->db->where('country_id', 4);
			$query = $this->db->get('citys');
			return $query->result_array();
		}
		
		public function get_expertbyservice($page_title) {
			
			$this->db->select('*');
			$this->db->where('service  like '. '"%' . $page_title . '%"' );
			$query = $this->db->get('expert_login_table');
			return $query->result_array();
			
			//print_r($query);
		}

	}