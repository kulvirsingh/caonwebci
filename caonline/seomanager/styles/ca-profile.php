
<div class="memberModalRegister modal fade" id="myModal" role="dialog">
			<div class="modal-dialog modal-member" style="width: 100%">
				<div class="modal-content">
					
				   
						<div class="container">

		<div class="col-sm-4 col-md-4 user-details">
            <div class="user-image">
                <img src="images/user.png" alt="" title="" class="img-circle">
            </div>
            <div class="user-info-block">
                <div class="user-heading">
                    <h3>Name</h3>
                    <span class="help-block">Designation</span>
                </div>
                <ul class="navigation">
                    <li class="active">
                        <a data-toggle="tab" href="#information">
                            <span class="glyphicon glyphicon-user"></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#settings">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#email">
                            <span class="glyphicon glyphicon-asterisk"></span>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#events">
                            <span class="glyphicon glyphicon-education"></span>
                        </a>
                    </li>
					 <li>
                        <a data-toggle="tab" href="#social">
                            <span class="glyphicon glyphicon-globe"></span>
                        </a>
                    </li>
                </ul>
                <div class="user-body">
                    <div class="tab-content">
                        <div id="information" class="tab-pane active">
                            <h4>Basic Information</h4>
							    <div class="form-overview">
					  
					  <form enctype="multipart/form-data" action="" method="post" >
					   <input name="id" type="hidden" value="" >
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Full Name</label>
                          <div class="col-md-12">
						 
                            <input class="form-control" name = "name" type="text" value="" id="example-text-input">
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
					
                          <label for="example-text-input" class="col-md-12 col-form-label">Gender</label>
                          <div class="col-md-12">
                            <select  name = "gender" class="form-control form-control-lg">
                              <option value="Male" >Male</option>
                              <option value="Female" >Female</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-email-input" class="col-md-12 col-form-label">E-mail</label>
                          <div class="col-md-12">
                            <input class="form-control" name = "email" type="email" value="" id="example-email-input">
                          </div>
                        </div>
						  <div class="form-group row col-md-6">
                          <label for="example-email-input" class="col-md-12 col-form-label">Alternate E-mail</label>
                          <div class="col-md-12">
                            <input class="form-control" name = "alt_email" type="email" value="" id="example-email-input">
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">Mobile Number</label>
                          <div class="col-md-12">
                            <input class="form-control" name = "contact" type="tel" value="" id="example-tel-input">
                          </div>
                        </div>
						<div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">Work Phone Number</label>
                          <div class="col-md-12">
                            <input class="form-control" name = "workphn" type="tel" value="" id="example-tel-input">
                          </div>
                        </div>
						<div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">Experience(In numbers)</label>
                          <div class="col-md-12">
                            <input class="form-control" name = "experience" type="tel" value="" id="example-tel-input">
                          </div>
                        </div>
						
						
						
                        <div class="clearfix"></div>
						
                      </div>
							
							
							
							
                        </div>
                        <div id="settings" class="tab-pane">
                            <h4>Address</h4>
							
							 <div class="form-overview">
					 
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Address Line 1</label>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name="address" value="" placeholder="Enter your Address Line 1" id="example-text-input">
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-email-input" class="col-md-12 col-form-label">Address Line 2</label>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name="area" value="" placeholder="Enter your Address Line 2" id="example-email-input">
                          </div>
                        </div>
							<div class="form-group row col-md-6">
							

							
                          <label for="example-tel-input" class="col-md-12 col-form-label">Country</label>
   
						  
						  <div class="col-md-12">
                            <select name="country" class="form-control" id="country" >
								<option value="">Select Country</option>
							
								
							</select>
                          </div>
                        </div>
						
						
							<div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">State</label>
                          <div class="col-md-12">
                            
							        <select class="form-control" name="state" id="state" >
									<option value=""></option>
									</select>
                          </div>
                        </div>
						<div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">City</label>
                          <div class="col-md-12">
                            <select class="form-control" name="city" id="city" >
									<option value=""></option>
									</select>
                          </div>
                        </div>
						
					
						
					
						
                        <div class="form-group row col-md-6">
                          <label for="example-tel-input" class="col-md-12 col-form-label">Pincode</label>
                          <div class="col-md-12">
                            <input class="form-control" type="tel" name="pincode" value="" placeholder="Enter your Pincode" id="example-tel-input">
                          </div>
                        </div>
                       
                       <!-- <div class="col-md-12">
                           <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
                        </div> -->
                        <div class="clearfix"></div>
						
                      </div>
							
							
                        </div>
                        <div id="email" class="tab-pane">
                            <h4>Expertise</h4>
							  <div class="form-overview" style="line-height: 30px;"> 
                       <div class="form-group">
                      <div class="col-sm-12">
					  
                        <div class="tick-box">
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Incorporation Modification" >
                            <span class="check-text">Company Incorporation/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Incorporation Modification">
                            <span class="check-text">LLP Incorporation/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Service Tax Registration Modification" >
                            <span class="check-text">Service Tax Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="VAT Registration Modification" >
                            <span class="check-text">VAT Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="GST Registration Modification" >
                            <span class="check-text">GST Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="IEC Registration Modification" >
                            <span class="check-text">IEC Registration/ Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trademark Application Objection" >
                            <span class="check-text">Trademark Application / Objection/ Renewal</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Income Tax Return">
                            <span class="check-text">Income Tax Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Annual Filing" >
                            <span class="check-text">Company Annual Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Annual Filing" >
                            <span class="check-text">LLP Annual Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Service Tax Return" >
                            <span class="check-text">Service Tax Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="VAT Return">
                            <span class="check-text">VAT Return Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Secretarial Compliances">
                            <span class="check-text">Secretarial Compliances</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="CA Certification">
                            <span class="check-text">CA Certification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="TDS Return" >
                            <span class="check-text">TDS Return Filing</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trust Formation">
                            <span class="check-text">Trust Formation</span></div>
							 <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Partnership"  >
                            <span class="check-text">Partnership</span></div>
                        </div>
                      </div>
                    </div>
                   
                        <div class="clearfix"></div>
                      </div>
							
							
							
                        </div>
                        <div id="events" class="tab-pane">
                            <h4>Qualifications</h4>
							
							
							
							<div class="form-overview">
                        <div class="form-group row col-md-12">
                          <label for="example-text-input" class="col-md-12 col-form-label">Educational Qualifications</label>
                          <div class="col-md-12">
                          
                         <div class="dropdown cq-dropdown open" data-name="statuses">
      <button class="btn btn-primary btn-sm drop-2 dropdown-toggle" type="button" id="dropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> Qualifications <span class="caret"></span> </button>
    
	  
	  <ul aria-labelledby="dropdown1" class="col-md-6 dropdown-menu menu-width">
       
	   <li><span><i class="fa fa-graduation-cap sm-cap" aria-hidden="true"></i></span>
          <label class="radio-btn">
            <input class="cursor-box" value="Chartered Accountant" name="qualifications[]" type="checkbox">Chartered Accountant  </label>
        </li>
        
		<li><span><i class="fa fa-graduation-cap sm-cap" aria-hidden="true"></i></span>
          <label class="radio-btn">
            <input class="cursor-box" value="Company Secretary" name="qualifications[]" type="checkbox">Company Secretary </label>
        </li>
		</ul>
        </div>
        </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Chartered Accountant Membership Number</label>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name="chart_act_member" value="" placeholder="Chartered Accountant Membership Number" id="example-text-input">
                          </div>
                        </div>
						 <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Chartered Accountant Member Since</label>
                          <div class="col-md-12">
                            <input class="form-control" type="date" name="chart_act_date" value="" id="example-text-input">
                          </div>
                        </div>
						<div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Company Secretary Membership Number</label>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name="company_secretary_membership" value="" placeholder="Company Secretary Membership Number" id="example-text-input">
                          </div>
                        </div>
						<div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Company Secretary Member Since </label>
                          <div class="col-md-12">
                            <input class="form-control" type="date" name="company_secretary_date" value="" id="example-text-input">
                          </div>
                        </div>
				
						
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Other Membership Or Highest Degree</label>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name="other_membership_number" value="" placeholder="Other Membership Or Highest Degree" id="example-text-input">
                          </div>
                        </div>
						
						 <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Year of Graduation</label>
                          <div class="col-md-12">
                            <input class="form-control" type="date" name="graduation_date" value="" id="example-text-input">
                          </div>
                        </div>
                     
                        <div class="clearfix"></div>
                      </div>
							
							
                        </div>
						<div id="social" class="tab-pane">
                            <h4>Social Media</h4>
							 <div class="form-overview">
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Facebook</label>
                          <div class="col-md-12">
                           <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-facebook"></i></span>
						
						<input type="text" class="form-control" name="facebook_url" value="" placeholder="Facebook Web Url">
					</div> 
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Linked</label>
                          <div class="col-md-12">
                            <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
						<input type="text" class="form-control" name="linkedin" value=""  placeholder="Linkedin Web Url">
					</div>
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Twiiter</label>
                          <div class="col-md-12">
                            <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-twitter"></i></span>
						<input type="text" class="form-control" name="twitter" value=""  placeholder="Twitter Web Url">
					</div>
                          </div>
                        </div>
                        <div class="form-group row col-md-6">
                          <label for="example-text-input" class="col-md-12 col-form-label">Website</label>
                          <div class="col-md-12">
                            <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-firefox"></i></span>
						<input type="text" class="form-control" name="website" value=""  placeholder="Website Web Url">
					      </div>
                          </div>
                        </div>
                        
                         <div class="clearfix"></div>
						<div class="col-md-12">
                          <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
                        </div>
						
                      </div>
							
							
                        </div>
                    </div>
                </div>
            </div>
        </div>
	
</div>
					</div>
	 </div>			</div>
		








 