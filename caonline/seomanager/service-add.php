<?php include_once('admin-header.php') ;?>

<?php
$result_arr=array();
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_services_name")," and id=$edit_key");
      }
?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Service  </h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
               
 <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'service-edit-submit.php':'service-add-submit.php';?>" method="post" data-parsley-validate>
                                        
                            <div class="panel-body">
							
							 <div class="form-group">
								<input type="hidden" name="id" id="id" value="<?=$edit_key?>" />
                                    <label class="col-sm-2 control-label">Service Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="service" class="form-control" value="<?php echo ($_POST["service"]!="")? $_POST["service"]:$res["service"];?>" required>
                                    </div>
                             </div>
								
                             
							
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save Service</button>
                            </div>
                        </form>





              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#ad').addClass('open');

});
</script>
    <?php include_once('admin-footer.php') ;?>