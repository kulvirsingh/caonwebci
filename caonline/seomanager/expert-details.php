<?php include_once('admin-header.php') ;?>

<?php
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_expert_login_table")," and id=$edit_key");  
  
}
?>

<script LANGUAGE="JavaScript" SRC="../codelibrary/js/func_ajax.js"></script>
<script src="jquery.min.js"></script>
<script src="jquery-2.1.3.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function()
{  
$('#country').on('change',function()
{
var countryID =$(this).val();

//alert(countryID);

if(countryID)

{
$.ajax
({
type:'POST',
url:'ajaxcity.php',
data:'country_id='+countryID,
success:function(html)
{
	
//alert(html);

$('#state').html(html);
$('#city').html('<option value="">Select State First</option>');
}


});
}else
{
$('#state').html('<option value="">Select Country First</option>');
$('#city').html('<option value="">Select State First</option>');



}

	})

$('#state').on('change',function()
	{
var stateID =$(this).val();
//alert(stateID);

if(stateID)

{
$.ajax
({
type:'POST',
url:'ajaxcity.php',
data:'state_id='+stateID,
success:function(html)
{
	//alert(html);
$('#city').html(html);
}


});
}else
{
$('#city').html('<option value="">Select State First</option>');

}

	})	

});

</script>
        <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4 class="heading">Professional Active</h4></div>
          
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">

          <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'professional-active.php':'#';?>" method="post" data-parsley-validate>
                                          
                            <div class="panel-body">
							  <div class="form-group">
                                        <label class="col-sm-2 control-label"><b>Profile Picture</b></label>
                                        <div class="col-sm-3">
                                    <input type="file" name="thumb_image" id="thumb_image" value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
    
                                           
                                        </div>

                                         <div class="col-sm-7">
                                        <?php if($edit_key!="" && $res["thumb_image"]!=""){?>
                             <input type="hidden" name="front_cart_edit"  value="<?php echo ($_POST["thumb_image"]!="")? $_POST["thumb_image"]:$res["thumb_image"];?>"/>
                              <img src="<?php echo UPLOADS_PATH.$res["thumb_image"];?>" width="80" height="80"/>
  
   <?php }?> 
                                    </div>
                                    </div>

                                 
				 <div class="form-group">
                          

                                    <label class="col-sm-2 control-label"><b>Full Name</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="name" class="form-control" value="<?php echo ($_POST["name"]!="")? $_POST["name"]:$res["name"];?>" >
                                    </div>
                                    
                                </div>
								
								 <div class="form-group">
                              

                                    <label class="col-sm-2 control-label"><b>Email</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" >
                                    </div>
                                    
                                </div>
								
								 <div class="form-group">
                              

                                    <label class="col-sm-2 control-label"><b>Password</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="	password" class="form-control" value="<?php echo ($_POST["password"]!="")? $_POST["password"]:$res["password"];?>" >
                                    </div>
                                    
                                </div>
									
								 <div class="form-group">
                              

                                    <label class="col-sm-2 control-label"><b>Alternate Email</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" >
                                    </div>
                                    
                                </div>
								
								
								
								 <div class="form-group">
                        

                                    <label class="col-sm-2 control-label"><b>Mobile Number</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="phone" class="form-control" value="<?php echo ($_POST["contact"]!="")? $_POST["contact"]:$res["contact"];?>" >
                                    </div>
                                    
                                </div>	
								
								 <div class="form-group">
                        

                                    <label class="col-sm-2 control-label"><b>Work Phone Number</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="phone" class="form-control" value="<?php echo ($_POST["contact"]!="")? $_POST["contact"]:$res["contact"];?>" >
                                    </div>
                                    
                                </div>	



                               <div class="form-group">
                        

                                    <label class="col-sm-2 control-label"><b>Gender</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                       <select  name = "gender" class="form-control form-control-lg">
									   <option value="" >Select Gender</option>
                                       <option <?php if ($gender == 'Male' ) echo 'selected' ; ?> value="Male" >Male</option>
                                       <option <?php if ($gender == 'Female' ) echo 'selected' ; ?> value="Female" >Female</option>
                                      </select>
                                    </div>
                                    
                                </div>									
							
							  <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Experience</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="experience" class="form-control" value="<?php echo ($_POST["experience"]!="")? $_POST["experience"]:$res["experience"];?>">
                                  </div>
                              </div>
							  
							  <div class="form-group">
                                <label class="col-sm-2 control-label"><b>About </b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea rows="4" cols="92" name="about_ca"><?php echo $res['about_ca']?></textarea>
                                    </div>
                                    
                                </div>
							  
								<div class="form-group">
                                <label class="col-sm-2 control-label"><b>Address</b><span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea rows="4" cols="92" name="address"><?php echo $res['address']?></textarea>
                                    </div>
                                    
                                </div>
		

		                     <div class="form-group">
                                  <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>
                                  <label class="col-sm-2 control-label"><b>Area</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="area" class="form-control" value="<?php echo ($_POST["area"]!="")? $_POST["area"]:$res["area"];?>" >
                                  </div>
                              </div>

		
				  

                     		
						
						<div class="form-group">
		 <input type="hidden" name="id" id="id" value="<?php echo $edit_key;?>"/>
		 <input type="hidden" name="reg_id" id="id" value="<?php echo $res['reg_id'];?>"/>
		 <input type="hidden" name="password" id="id" value="<?php echo $res['password'];?>"/>
				
		 <label class="col-sm-2 control-label"><b>Country</b><span class="text-danger">*</span></label>
					
          <?php 
             include'dbConfig.php';
             $query=$db->query("Select * From country Where status='1' ORDER BY country ASC");
             $rowcount=$query->num_rows;
	       ?>	   
						  
						 <div class="col-md-6">
                            <select name="country" class="form-control" id="country" >
								<option value="">Select Country</option>
								<?php 
								if($rowcount>0){

									while($row=$query->fetch_assoc()){
										
										 $pid=$row["country"].','.$row["id"];
										 
										 ?>
		<option value="<?php echo $pid ; ?>" <?php if($row['country']==$res['country']) echo 'selected="selected"'; ?> ><?php echo $row["country"];?></option>
										
										<?php
									}
									
								}
                                   else{
                                        echo '<option value="">Country Not Available</option>';

									}
								?>
								
							</select>
                         
                        </div>
                        </div>
      
                          <div class="form-group">
                        
						   <label class="col-sm-2 control-label"><b>State</b><span class="text-danger">*</span></label>
                          <div class="col-md-6">
                            
							        <select class="form-control" name="state" id="state" >
									<option value="<?php echo $res['state'] ; ?>"><?php echo $res['state'] ; ?></option>
									</select>
                          </div>
                        </div>	


                      <div class="form-group">
					     <label class="col-sm-2 control-label"><b>City</b><span class="text-danger">*</span></label>
                       
                          <div class="col-md-6">
                            <select class="form-control" name="city" id="city" >
									<option value="<?php echo $res['city'] ; ?>"><?php echo $res['city'] ; ?></option>
									</select>
                          </div>
                        </div>						
							   <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Pincode</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="pincode" class="form-control" value="<?php echo ($_POST["pincode"]!="")? $_POST["pincode"]:$res["pincode"];?>" >
                                  </div>
                              </div>
				 <div class="form-group">
                   
					 <label class="col-sm-2 control-label"><b>Expertise</b><span class="text-danger">*</span></label>
                      <div class="col-sm-9">
                        
						<?php   $res['service_name'] ;
                                $hby = explode(",",$res['service_name']);
						 ?>
						<div class="tick-box">
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="CA Certification"<?php if(in_array("CA Certification",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">CA Certification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Annual Filing"<?php if(in_array("Company Annual Filing",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Company Annual Filing</span></div>
                          
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Company Incorporation Modification" <?php if(in_array("Company Incorporation Modification",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Company Incorporation Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="FDI consulting and related services" <?php if(in_array("FDI consulting and related services",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">FDI consulting and related services</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="GST Registration  Modification"<?php if(in_array("GST Registration  Modification",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">GST Registration  Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="GST Return"<?php if(in_array("GST Return",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">GST Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="IEC Registration Modification" <?php if(in_array("IEC Registration Modification",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">IEC Registration Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Income Tax Return" <?php if(in_array("Income Tax Return",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Income Tax Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Annual Filing" <?php if(in_array("LLP Annual Filing",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">LLP Annual Filing</span></div>
                       
                         
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="LLP Incorporation Modification" <?php if(in_array("LLP Incorporation Modification",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">LLP Incorporation Modification</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Secretarial Compliances" <?php if(in_array("Secretarial Compliances",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Secretarial Compliances</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="TDS Return" <?php if(in_array("TDS Return",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">TDS Return</span></div>
                          <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trademark Application  Objection" <?php if(in_array("Trademark Application  Objection",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Trademark Application  Objection</span></div>
							 <div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trust Formation" <?php if(in_array("Trust Formation",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Trust Formation</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Audit" <?php if(in_array("Audit",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Audit</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Apeda Registration" <?php if(in_array("Apeda Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Apeda Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="DSC" <?php if(in_array("DSC",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">DSC</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="FSSAI Registration" <?php if(in_array("FSSAI Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">FSSAI Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="ISO Registration" <?php if(in_array("ISO Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">ISO Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="MSME/SSI Registration" <?php if(in_array("MSME/SSI Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">MSME/SSI Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="NRI Registration" <?php if(in_array("NRI Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">NRI Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Other" <?php if(in_array("Other",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Other</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Trademark Registration" <?php if(in_array("Trademark Registration",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Trademark Registration</span></div>
							<div class="col-sm-4">
                            <input type="checkbox" name="service[]" value="Book Keeping" <?php if(in_array("Book Keeping",$hby)){?> checked="checked"<?php }?>>
                            <span class="check-text">Book Keeping</span></div>
                        </div>
                      </div>
                    </div>			
						
                           
                            <div class="form-group">
                          

                                    <label class="col-sm-2 control-label"><b>Educational &nbsp;&nbsp;&nbsp; &nbsp;Qualifications<span class="text-danger">*</span></label>
                                    <div class="dropdown cq-dropdown" data-name="statuses">
      <button class="btn btn-primary btn-sm drop-2 dropdown-toggle" type="button" id="dropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> Qualifications <span class="caret"></span> </button>
    
		<?php    $res['qualifications'] ;
             $bby = explode(",",$res['qualifications']);
			
	   ?>
	  
	  <ul aria-labelledby="dropdown1" class="col-md-6 dropdown-menu menu-width">
       
	      <li><span><i class="fa fa-graduation-cap sm-cap" aria-hidden="true"></i></span>
          <label class="radio-btn">
            <input class="cursor-box" value="Chartered Accountant" name="qualifications[]" <?php if(in_array("Chartered Accountant",$bby)){?> checked="checked"<?php }?> type="checkbox">Chartered Accountant  </label>
        </li>
        
		<li><span><i class="fa fa-graduation-cap sm-cap" aria-hidden="true"></i></span>
          <label class="radio-btn">
            <input class="cursor-box" value="Company Secretary" name="qualifications[]" <?php if(in_array("Company Secretary",$bby)){?> checked="checked"<?php }?> type="checkbox">Company Secretary </label>
        </li>
        
		</ul>
     
    </div>
                            </div>
						   
                            
								
							<div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Chartered Accountant Membership Number</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="chart_act_member" class="form-control" value="<?php echo ($_POST["chart_act_member"]!="")? $_POST["chart_act_member"]:$res["chart_act_member"];?>">
                                  </div>
                              </div>
							  
							  	<div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Chartered Accountant Membership Since</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="chart_act_member" class="form-control" value="<?php echo ($_POST["chart_act_member"]!="")? $_POST["chart_act_member"]:$res["chart_act_member"];?>">
                                  </div>
                              </div>
							  
							  
							  <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Company Secretary Membership number</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="company_secretary_membership" class="form-control" value="<?php echo ($_POST["company_secretary_membership"]!="")? $_POST["company_secretary_membership"]:$res["company_secretary_membership"];?>" >
                                  </div>
                              </div>
							  
							  <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Company Secretary Membership Since</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="company_secretary_membership" class="form-control" value="<?php echo ($_POST["company_secretary_membership"]!="")? $_POST["company_secretary_membership"]:$res["company_secretary_membership"];?>" >
                                  </div>
                              </div>
							  
							   
							  
							   <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Other Membership Or Highest Degree</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="other_membership_number" class="form-control" value="<?php echo ($_POST["other_membership_number"]!="")? $_POST["other_membership_number"]:$res["other_membership_number"];?>" >
                                  </div>
                              </div>
							  
							  
							   <div class="form-group">
                                  
                                  <label class="col-sm-2 control-label"><b>Year of Graduation</b><span class="text-danger">*</span></label>
                                  <div class="col-sm-6">
                                  <input type="text" name="other_membership_number" class="form-control" value="<?php echo ($_POST["other_membership_number"]!="")? $_POST["other_membership_number"]:$res["other_membership_number"];?>" >
                                  </div>
                              </div>
							
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">professional active</button>
                            </div>
                        </form>
                   


              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->


 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#tm').addClass('open');

});
</script>


  <!-- /initialize page scripts -->
<?php include_once('admin-footer.php')?>