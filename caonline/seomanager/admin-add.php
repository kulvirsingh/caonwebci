<?php include_once('admin-header.php') ;?>

<?php
$result_arr=array();
$edit_key = $_POST["editKey"] ;
if($edit_key) {
    $res = $obj->getAnyTableWhereData($obj->getTable("var_user_login_table")," and id=$edit_key");
      }
?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
            <div class="panel-heading"><h4>Add/Edit Employe  </h4></div>
          <div class="panel-heading border">
           Please fill the forms below.
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
               
 <form enctype="multipart/form-data" class="panel panel-color-top panel-default form-horizontal form-bordered" action="<?php echo ($edit_key!="")?'admin-edit-submit.php':'admin-add-submit.php';?>" method="post" data-parsley-validate>
                                        
                            <div class="panel-body">
							
							 <div class="form-group">
								<input type="hidden" name="id" id="id" value="<?= $edit_key?>" />
                                    <label class="col-sm-2 control-label">Emp Code<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="emp_code" class="form-control" value="<?php echo ($_POST["emp_code"]!="")? $_POST["emp_code"]:$res["emp_code"];?>" required>
                                    </div>
                             </div>
								
                                <div class="form-group">
								
                                    <label class="col-sm-2 control-label">Emp Name<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="emp_name" class="form-control" value="<?php echo ($_POST["emp_name"]!="")? $_POST["emp_name"]:$res["emp_name"];?>" required>
                                    </div>
                                  
                                </div>
								
						   <div class="form-group">
								
                                    <label class="col-sm-2 control-label">Email<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="email" class="form-control" value="<?php echo ($_POST["email"]!="")? $_POST["email"]:$res["email"];?>" required>
                                    </div>
                                  
                                </div>
								
								
							<div class="form-group">
								
                                    <label class="col-sm-2 control-label">Phone <span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
									 <input type="text" name="phone" class="form-control" value="<?php echo ($_POST["phone"]!="")? $_POST["phone"]:$res["phone"];?>" required>
                                    </div>
                                  
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Company<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="company" class="form-control" value="<?php echo ($_POST["company"]!="")? $_POST["company"]:$res["company"];?>"  required>
                                    </div>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                </div>


                              

                                  
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Department<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="department" class="form-control" data-parsley-type="department" value="<?php echo ($_POST["department"]!="")? $_POST["department"]:$res["department"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
								 <div class="form-group">
                                    <label class="col-sm-2 control-label">Designation<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" name="designation" class="form-control" data-parsley-type="designation" value="<?php echo ($_POST["designation"]!="")? $_POST["designation"]:$res["designation"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="col-sm-2 control-label">Joining Date<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="date" name="joining_date" class="form-control" data-parsley-type="designation" value="<?php echo ($_POST["joining_date"]!="")? $_POST["joining_date"]:$res["joining_date"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
								
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="password" name="password" class="form-control" data-parsley-length="[5, 8]" value="<?php echo ($_POST["password"]!="")? $_POST["password"]:$res["password"];?>" required>
                                    </div>
                                    <div class="col-sm-4">
                                      
                                    </div>
                                </div>
								
				
								

                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-success">Save User</button>
                            </div>
                        </form>





              </div>
            </div>
          </div>
        </div>

       

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

$( document ).ready(function() {
  
    $('#ad').addClass('open');

});
</script>
    <?php include_once('admin-footer.php') ;?>