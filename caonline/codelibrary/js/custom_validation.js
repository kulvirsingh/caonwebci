//******************************Email sender   validation  start Here*****************************//.

$(document).ready(function(e) {
	
	$('#btn_sender').click(function(e) {
		e.preventDefault();
$("#error_msg").css('color','red');
		
				
		        $email = $('#email').val();
				$name=$('#name').val();
				$phone=$('#subject').val();
				$message=$('#message').val();
				
				

				 var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
 			     var valid = emailRegex.test($email);
		

		var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        var valid2=regexp.test($phone);
				
				if($email == ""  &&  $name=="")
				{
					$('#error_msg').html("Please Enter Required Fields");
					$('#name').focus();
				}
				
				  else  if($name == "")
				 {
					$('#error_msg').html("Please Enter Your Name");
					$('#name').focus();
				 }
				
				  else  if($email == "")
				 {
					$('#error_msg').html("Please Enter EmailID");
					$('#email').focus();
				 }
				
				else  if(!valid)
				{
					$('#error_msg').html("Please Enter Valid EmailID");
					$('#email').focus();
				 }
				 
				 else  if($phone == "")
				 {
					$('#error_msg').html("Please Enter Subject ");
					$('#subject').focus();
				 }
				 
				else if($message == "")
				{	
					$('#error_msg').html("Please enter message");
					$('#message').focus();
				}
				 
				 else
				{
					$('#error_msg').html(null);
				
				$.ajax({
					type: "POST",
					cache: false,
					url: "send-contact-mail.php",
					data: 'email='+ $email +'&name='+$name +'&message='+$message+'&web='+$phone,
					success: function(data) {
						$("#error_msg").css('color','green');
						$("#error_msg").html(data);
						$("#name").val(null);
						$("#email").val(null);
						$("#subject").val(null);
						$("#message").val(null);
					}
				});
				
				}
								
	});
});
//******************************end  validation  ends Here*****************************//.


//******************************Login validationstart Here*****************************//.

$(document).ready(function(e) {
    
    $('#login_btn').click(function(e) {
        e.preventDefault();
        //alert('hello');
                $email = $('#email1').val();
                $pass=$('#password1').val();
                
                 var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
                 var valid = emailRegex.test($email);
                
                if($email == ""  && $pass=="")
                {
                    $('#error_msg').html("Please Enter Email and Password");
                    $('#email1').focus();
                }
                else if($email == "")
                {   
                    $('#error_msg').html("Please Enter  Email");
                    $('#email1').focus();
                }
                  else if(!valid)
                {   
                    $('#error_msg').html("Please Enter valid  Email");
                    $('#email1').focus();
                }
                          
                else if($pass == "")
                {   
                    $('#error_msg').html("Please Enter  Password");
                    $('#password1').focus();
                }
                else
                {
                    $('#error_msg').html("");
                    
                    $.ajax({
                    type: "POST",
                    cache: false,
                    url: "login_submit.php",
                    data: 'email='+ $email +'&password='+$pass,
                    success: function(data) {
                        $('#email1').val(null);
                        $('#password1').val(null);
                         $("#error_msg").css('color','red');
                        $("#error_msg").html(data);
                       
                        
    
                    }
                });
                
                }
    });
    
});

//******************************Login validation Ends Here*****************************//.


//******************************registration  validation  start Here*****************************//.

$(document).ready(function(e) {
	
	$('#register_btn').click(function(e) {
		e.preventDefault();
		//alert('work');
				$fname= $('#full_name').val();
				
		        $email = $('#email').val();
				$pass=$('#password').val();
				$rpass=$('#rpassword').val();
				
				
				 var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
 			     var valid = emailRegex.test($email);
		
				
				if( $fname=="" &&   $email == ""  && $pass=="" && $rpass=="" )
				{
					$('#error_msg1').html("Please Enter Required Fields(*)");
					$('#full_name').focus();
				}
				else  if($fname == "")
				{
					$('#error_msg1').html("Please Enter Full Name");
					$('#full_name').focus();
				 }
				 else  if($email == "")
				 {
					$('#error_msg1').html("Please Enter EmailID");
					$('#email').focus();
				 }
				
				else  if(!valid)
				{
					$('#error_msg1').html("Please Enter Valid EmailID");
					$('#email').focus();
				 }
				 
				 else  if($pass == "")
				 {
					$('#error_msg1').html("Please Enter Password");
					$('#password').focus();
				 }
				 
				 else  if($rpass == "")
				 {
					$('#error_msg1').html("Please Enter Confirm Password");
					$('#rpassword').focus();
				 }
				 
				  else  if($rpass != $pass)
				 {
					$('#error_msg1').html("Password not matched");
					$('#rpassword').focus();
				 }
				  
				
				 
				 else
				{
					$('#error_msg1').html("");
					
					$.ajax({
					type: "POST",
					cache: false,
					url: "register-submit.php",
					data: 'email='+ $email +'&password='+$pass+'&full_name='+$fname,
					success: function(data) {
						$('#email').val(null);
						$('#password').val(null);
						$('#full_name').val(null);
					    $("#error_msg1").css('color','red');
						$('#rpassword').val(null);
						$("#error_msg1").html(data);
					
					}
				});
				
				}
								
	});
});
//******************************registration  validation  ends Here*****************************//.


//******************************forget  validationstart Here*****************************//.

$(document).ready(function(e) {
    
	$('#forget_btn').click(function(e) {
		e.preventDefault();
		//alert('hello');
		       
				$mail=$('#mail').val();
				
				 var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
 			     var valid = emailRegex.test($mail);
			
			 if($mail == "")
				{	
					$('#error_msg').html("Please Enter  Email");
					$('#mail').focus();
				}
				else  if(!valid)
				{
					$('#error_msg').html("Please Enter Valid EmailID");
					$('#mail').focus();
				 }
				else
				{
					$('#error_msg').html("");
					
					$.ajax({
					type: "POST",
					cache: false,
					url: "forget-submit.php",
					data: 'email='+ $mail,
					success: function(data) {
					
						$('#mail').val(null);
						$("#error_msg").html(data);
						
    
					}
				});
				
				}
	});
	
});

//*****************************foregt validation Ends Here*****************************//.


//************************foregt password form validation************************/
$(document).ready(function(e) {
	$('#reset_btn').click(function(e) {
		e.preventDefault()
		
		$pass=$('#password').val();
		
		if($pass=="" )
		{
			alert('Please enter Password1');
			$('#password').focus();
		}

		else{
					
					$('#frmfor').submit();
            }
		
		
	});
});



//******************************password form   validation  ends Here*****************************//.
$(document).ready(function(e) {
	$('#edit_password_btn').click(function(e) {
		e.preventDefault();
		//alert('hello');
		$pass=$('#password3').val();
		$rpass=$('#password2').val();
		
		if($pass=="" || $rpass=="")
		{
			alert('Please enter passwords');
			$('#password3').focus();
		}
		else if ($pass!=$rpass)
		{
			alert('Please enter same  password');
			$('#password2').focus();
		}
		else
				{
						$('#frmfor').submit();
				
				}
		
		
	});
});




//******************************Add to cart  validation  ends Here*****************************//.
$(document).ready(function(e) {
	$('#add_cart').click(function(e) {
		e.preventDefault();
		//alert('hello');
		$category_id=$('#category_id').val();
		$product_id=$('#product_id').val();
		$color_id=$('#color_id').val();
		$size_id=$('#size_id').val();
		$quantity=$('#quantity').val();


		if($category_id=="0" || $product_id=="0")
		{
			alert('Please Select Categories and Product');
			$('#category_id').focus();
		}
		else if($category_id=="0")
		{
			alert('Please Select Categories');
			$('#category_id').focus();
		}

		else if($product_id=="0")
		{
			alert('Please Select Product');
			$('#product_id').focus();
		}
		else if($color_id=="0")
		{
			alert('Please Select Color');
			$('#color_id').focus();
		}

		else if($size_id=="0")
		{
			alert('Please Select Size');
			$('#size_id').focus();
		}

		else if($quantity<= 0 || isNaN($quantity))
		{
			alert('Please Enter quantity');
			$('#quantity').focus();
		}



		else
		{ 
		  $.ajax({
					type: "POST",
					cache: false,
					url: "add-cart.php",
					data: 'category_id='+ $category_id +'&product_id='+$product_id+'&color_id='+$color_id+'&size_id='+$size_id+'&quantity='+$quantity,
					success: function(data) {
					
						$("#ttabs").html(data);
						$('#category_id').val(null);
						$('#product_id').val(null);
						$('#color_id').val(null);
						$('#size_id').val(null);
						$('#quantity').val(null);
    
					}
				});
				
		}
		
		
	});
});



