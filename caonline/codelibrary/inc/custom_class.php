<?php
/*_______________________________________________________________________
Created By	: Mrityunjay
Created On	: 20/05/2013
Modified By : 
Modified On : 
Description : This class has  function class used in both admin and user section.
_________________________________________________________________________
*/
class custom_class extends database_class
{	

 function get_zone_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_zones"),"  and id=".$id);
		return $res['zone_name'];

	}	

 function get_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_category"),"  and id=".$id);
		return $res;

	}

 function get_parent_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_category"),"  and id=".$id);
		return $res['parent_id'];

	}
	
	function get_cat_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_category"),"  and id=".$id);
		return $res['category'];

	}
	
	
	function get_product_from_cat($cid)
	{
		$res = $this->getAnyTableAllData($this->getTable("var_products"),"  and status=1 and category_id=".$cid);
		return $res;

	}
	
	function get_product_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_products"),"  and id=".$id);
		return $res;

	}
	
	function get_product_cat_from_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_products"),"  and id=".$id);
		return $res['category_id'];

	}
	
	function get_category_parent()
	{
		$res = $this->getAnyTableAllData($this->getTable("var_category")," and parent_id=0 and status='1' order by id");
		return $res;

	}
	
	function get_category_child_by_parent($id)
	{
		$res = $this->getAnyTableAllData($this->getTable("var_category")," and parent_id =". $id);
		return $res;

	}
	
	function get_category_child($id)
	{
		$output="";
		$res = $this->getAnyTableAllData($this->getTable("var_category"),"  and parent_id=$id and status='1' order by id");
		$row=count($res);
		if($row!=0)
		{
			 $output.= "<ul>";
			 foreach($res as $chd)
			 {		$id=$chd['id'];
			        $name= $chd['category'];
					$count=$this->count_child($id);
					$class=($count==0)?'':'subli';
					$href=($count==0)?'product-listing.php':'category-listing.php';
				    $output.="<li class='subline1'><a href='$href?id=$id' class='$class'>".$name."</a>";
					$output.=$this->get_category_child($chd['id']);
					
					$output.="</li>";
			 }
			 $output.= "</ul>"; 
		}
		
		return $output;
		

	}
	
function detail_breadcrumb() 
{
		
  
   $currentFile = $_SERVER["PHP_SELF"];
    $parts = explode('/', $currentFile);
    $current_URL= $parts[count($parts) - 1];
	if($current_URL!="category-listing.php" && $current_URL!="product-listing.php"&& $current_URL!="product-details.php")
	{
	
	$p=explode('.',$current_URL);
	$current_URL=$p[0];
  echo '<p><a href="index.php">Home</a> < '.ucwords($current_URL).'</p>' ;
	}
	else
	{
		$s= $_SERVER['QUERY_STRING'];
		$id=end(explode('=',$s));
				if($current_URL=="category-listing.php" || $current_URL=="product-listing.php")
				{
				//array_push($arr,$id);
				$output.= $id.":";
				$parent=$this->get_parent_from_id($id);
							while($parent!=0)
							{
//								  array_push($arr,$parent);
                                 $output.= $parent.":";
								 // $ids=$this->get_from_id($parent);
								  $parent=$this->get_parent_from_id($parent);
								  
							}
							//echo $output;
							$arr=explode(':',$output);
							$arr=array_reverse($arr);
							
							array_shift($arr);
							//var_dump($arr);
					  $display.='<p><a href="index.php">Home</a>' ;

		 				foreach($arr as $id)
						{
							 if(end($arr)==$id)
							{
								$product=$this->get_product_from_id($id);
							 $display.='> '.$this->get_cat_from_id($id).'' ;
							}
							else
							{
							 $display.='> <a href=category-listing.php?id='.$id.'>'.$this->get_cat_from_id($id).'</a>' ;
							}

						}
						$display.="</p>";
				
				}
				else if($current_URL=="product-details.php")
				{
					$count=0;
					$output.= $id.":";
					$cat_id=$this->get_product_cat_from_id($id);
					$output.= $cat_id.":";
					$parent=$this->get_parent_from_id($cat_id);

					while($parent!=0)
							{
//								  array_push($arr,$parent);
                                 $output.= $parent.":";
								 // $ids=$this->get_from_id($parent);
								  $parent=$this->get_parent_from_id($parent);
								  
							}
							$arr=explode(':',$output);
							$arr=array_reverse($arr);
							
							array_shift($arr);
							//var_dump($arr);
					  $display.='<p><a href="index.php">Home</a>' ;

		 				foreach($arr as $id)
						{  $count++;
						   $len=count($arr);
							
							if($count==$len)
							{
								$product=$this->get_product_from_id($id);
							   $display.='> '.$product['name'].'' ;
							}
							else if($count==$len-1)
							{
							 $display.='> <a href=product-listing.php?id='.$id.'>'.$this->get_cat_from_id($id).'</a>' ;

							}
							else
							{
						 $display.='> <a href=category-listing.php?id='.$id.'>'.$this->get_cat_from_id($id).'</a>' ;
							}
							
							
						}
						$display.="</p>";
				}
				
				echo $display;
		
	}
}

	
	
	function count_child($id)
	{
		
		$qry = " select * from ".$this->getTable('var_category')."  where parent_id=".$id;
		$res=mysql_query($qry);
		$rows=mysql_num_rows($res);
		return $rows;			
	}
	
}
?>