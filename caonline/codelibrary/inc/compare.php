<?php

class compare extends database_class{

	protected $sess = null;			// currnet session var
	protected $sessKey = null;		// current session key for var
	protected $lastAdded = null;
	


	// begin and construct
	public function __construct($cartVar) {
		$this->sessKey = $cartVar;
		$this->getSessionCart();
	}


	// destruct - unset cart var
	public function __destruct() {
		unset($this->sess);
	}


	/**
	 * Add one or more products of the same type (id)
	 * @param $id(int) - product id
	 * @param $quant(int) - quantity (remove product if $quant == 0)
	 * @param $addQuant(bool) - make add to current quantity
	 */


	


	public function addProduct($id, $quant=1) {
		// make cast, if you have int values
		$id = (int)$id;
		$getFromDB_product = array (
			'id'=>$id
			 
		);
		// if no quantity, delete product
		if($quant == 0) {
			unset($this->sess[$id]);
			
		} else {


			$this->lastAdded = $id;
           
			// if not yet set, add into cart

			if(count($_SESSION[$this->sessKey])<3)
			{
				if(!isset($this->sess[$id])) {
					$this->sess[$id] = array(
					
						'id'=>$getFromDB_product['id'],
						
						
						// you can have here some details from database
					);
				// if it was set, add the quantity
				} 
		  }



		}
		return $this->setSessionCart();
	}


	/**
	 * Remove product
	 * @param id(int) - product ID
	 * @param $quant(int) - no. of products to remove ( 0 == all )
	 */
	public function removeProduct($id,$quant = 0){

		
		if($quant == 0) {
			return $this->addProduct($id, 0);

		} 
	}


	// This output can be into a template and whatever.
	// You have the $this->sess var so you have all you need
	public function viewCart() {
		echo '<pre>';
		print_r($_SESSION[$this->sessKey]);
		echo '</pre>';
	}


	// get no. of products from cart
	public function cartCount() {
		return count($this->sess);
	}


	// Magic area
	// ---------------

	// magic set function
	public function __set($name, $value) {
		switch ($name) {
			case 'discount':
			case 'bonusProduct':
			// you can add more new stuff here
			// for the last added product
				$this->sess[$this->lastAdded][$name] = $value;
				$this->setSessionCart();
			break;
		}
	}


	// Private area
	// ---------------

	// Set session from object [ session = object ]
	protected function setSessionCart() {
		$_SESSION[$this->sessKey] = $this->sess;
		return true;
	}

	// Get session to object [ object = session ]
	protected function getSessionCart() {
		$this->sess = isset($_SESSION[$this->sessKey]) ? $_SESSION[$this->sessKey] : array();
		return true;
	}

}

