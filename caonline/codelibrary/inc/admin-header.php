<?php error_reporting(0) ; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Panel ---&gt;Login</title>
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/frame.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery.js" type="text/javascript"></script>
<style>
.txt {
	color: #333;
	font-size: 13px;
	font-weight: bold;
}
.txt:hover {
	color: #FFF;
}
.brd {
	box-shadow: 3px 3px 2px #ccc;
	height: 120px;
	width: 120px;
	padding-top: 5px;
	border: solid 1px #bbb;
	opacity: 0.8;
	transition: all ease-in-out 0.5s;
}
.brd:hover {
	box-shadow: 3px 3px 2px #333;
	border: solid 1px #000;
	opacity: 1;
	background: rgba(0,0,0,0.6);
	color: #fff;
	border-bottom-left-radius: 15px;
	border-top-right-radius: 15px;
}
</style>
</head>
<body >
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
  <th width="14%" align="left"  bgcolor="#fff" style="padding:10px 10px 10px 20px; border-right:1px solid white; border-left:1px solid white;border-top:1px solid white; " scope="col" ><img src="../images/logo.png" width="200px"/> </th>
  <th width="56%" align="right"  valign="bottom" bgcolor="#FFFFFF" style="padding:10px 10px 10px 20px; border-right:1px solid white; border-left:1px solid white;border-top:1px solid white; " scope="col"><h3>
      <?php //echo date("D d M Y")?>
    </h3></th>
  <th valign="bottom" width="30%" align="right"  bgcolor="#FFFFFF" style="padding:0px 20px; border-right:1px solid white; border-left:1px solid white; font-size:12px;border-top:1px solid white; text-align:right; " scope="col" ><br />
    <?php
        if($_SESSION["sess_admin_id"]!='')
		{
		?>
    <?php echo "Hi  , ". $_SESSION["SESSION_USER_NAME"];?>&nbsp; &nbsp; <a title="View Site" href="../" target="_blank"><img style="position: relative;
    top: 3px;" width="16" height="16" src="images/site.png"/></a>&nbsp;&nbsp;&nbsp; <a title="Logout" href="logout.php"><img style="position: relative;
    top: 3px;" src="images/logout.png" width="16" height="16" /></a>
    <?php
		}
		?>
  </th>
</tr>
<tr>
  <th height="30" colspan="3" align="left" class="menu-admin" scope="col"> <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:white;">
      <tr>
        <th align="left" scope="col"><div id="test">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <?php
		if($_SESSION["sess_admin_id"]!='' )
		{
		?>
              <tr>
                <nav style="width:100%;" id="navigation" class="site-navigation" role="navigation">
                  <ul class="menu">
                    <li class="menu-item"><a href="welcome.php">DashBoard</a>
                    <li class="menu-item"><a href="#">Admin MANAGEMENT</a>
                      <ul class="dropdown">
                        <li class="menu-item sub-menu"><a href="admin-manage.php">Admin Users</a></li>
                        <li class="menu-item sub-menu"><a href="setting-add.php">Global Settings</a></li>
                      </ul>
                    </li>
						

                   <li class="menu-item"><a href="service-manage.php">Services</a></li>

   <li class="menu-item"><a href="#">Catalog Management</a>
 <ul class="dropdown">
           <li class="menu-item"><a href="cat-manage.php">Category</a></li>
           <li class="menu-item"><a href="product-manage.php">Products</a></li>
            <li class="menu-item"><a href="attribute-manage.php">Attributes</a></li>
               <li class="menu-item"><a href="atoc-manage.php">Category Attributes</a></li>
             <li class="menu-item"><a href="order-manage.php">Order Management</a></li>
          
            </ul>
               </li>
					 
                    <li class="menu-item"><a href="#">Content MANAGEMENT</a>
                      <ul class="dropdown">
                        <li class="menu-item sub-menu"><a href="cms-manage.php">CMS Pages</a></li>
                          <li class="menu-item sub-menu"><a href="gallery-manage.php">Gallery</a></li>
                        <li class="menu-item sub-menu"><a href="testimonial-manage.php">Testimonials</a></li>
                      </ul>
                    </li>
                
                  
                   
                  </ul>
                </nav>
              </tr>
              <?php
		}
	  ?>
            </table>
          </div>
        </th>
      </tr>
    </table>
  </th>
</tr>
