<?php

class cart extends database_class{

	protected $sess = null;			// currnet session var
	protected $sessKey = null;		// current session key for var
	protected $lastAdded = null;
	protected $lastSize = null;		// last added product


	// begin and construct
	public function __construct($cartVar) {
		$this->sessKey = $cartVar;
		$this->getSessionCart();
	}


	// destruct - unset cart var
	public function __destruct() {
		unset($this->sess);
	}


	/**
	 * Add one or more products of the same type (id)
	 * @param $id(int) - product id
	 * @param $quant(int) - quantity (remove product if $quant == 0)
	 * @param $addQuant(bool) - make add to current quantity
	 */

	public function updateProduct($id,$quant,$size) {
		// make cast, if you have int values

		if($quant == 0) {
			unset($this->sess[$id][$size]);
		}
		else
		{

		$id = (int)$id;
		$size_id=$size; // for your security

		$this->sess[$id][$size_id]['qanty'] =$quant;
		return $this->setSessionCart();
	   }
	}



	public function addProduct($id, $quant=1, $addQuant=true,$size) {
		// make cast, if you have int values
		$id = (int)$id;
		$size_id=$size; // for your security
$pro = $this->getAnyTableWhereData($this->getTable("var_product")," and status='1' and id='$id' ");   
$size_name = $this->getAnyTableWhereData($this->getTable("var_product_size")," and  id='$size_id' ");   

		$getFromDB_product = array (
			'price'=>$pro['price'],
			'discount'=>0,
			'title'=>$pro['product_name'],
			'size'=>$size_name['size_name']   
		);

		// if no quantity, delete product
		if($quant == 0) {
			unset($this->sess[$id][$size]);

		// else set it
		} else {

			$this->lastAdded = $id;
			$this->lastSize = $size_id;

			// if not yet set, add into cart
			if(!isset($this->sess[$id][$size_id])) {
				$this->sess[$id][$size_id] = array(
					'qanty'=>$quant,
					'price'=>$getFromDB_product['price'] , // to play, set all products price to 82.5
					'discount'=>$getFromDB_product['discount'], // set as '%'
					'title'=>$getFromDB_product['title'],
					'size'=>$getFromDB_product['size'],
					// you can have here some details from database
				);
			// if it was set, add the quantity
			}
			else if($addQuant == true) {
				// for some reasons you can choose not to do this
				// so you can comment this below line
				$this->sess[$id][$size_id]['qanty'] = $this->sess[$id][$size_id]['qanty'] + $quant;
			}
		}
		return $this->setSessionCart();
	}


	/**
	 * Remove product
	 * @param id(int) - product ID
	 * @param $quant(int) - no. of products to remove ( 0 == all )
	 */
	public function removeProduct($id,$size_id,$quant=0){

		// default, remove all products
		if($quant == 0) {
			return $this->addProduct($id, 0,true,$size_id);

		// remove only $quant number of products
		} else {
			$currentQuant = isset($this->sess[$id][$size]['qanty']) ? $this->sess[$id][$size]['qanty'] : 0;
			// if current q is smaller than requested q, delete the entire product
			$productsRemaining = $currentQuant - $quant;
			if($productsRemaining <= 0) {
				return $this->removeProduct($id,$size_id);
			} else {
				$this->sess[$id][$size]['qanty'] = $productsRemaining;
				return $this->setSessionCart();
			}
		}
	}


	// This output can be into a template and whatever.
	// You have the $this->sess var so you have all you need
	public function viewCart() {
		echo '<pre>';
		print_r($_SESSION[$this->sessKey]);
		echo '</pre>';
	}


	// get no. of products from cart
	public function cartCount() {
		$count=0;
		foreach($this->sess as $a)
		{ 
			foreach($a as $aa)
			{
			$count++;
		}
		}
		return $count;
	}


	// Magic area
	// ---------------

	// magic set function
	public function __set($name, $value) {
		switch ($name) {
			case 'discount':
			case 'bonusProduct':
			// you can add more new stuff here
			// for the last added product
				$this->sess[$this->lastAdded][$name] = $value;
				$this->setSessionCart();
			break;
		}
	}


	// Private area
	// ---------------

	// Set session from object [ session = object ]
	protected function setSessionCart() {
		$_SESSION[$this->sessKey] = $this->sess;
		return true;
	}

	// Get session to object [ object = session ]
	protected function getSessionCart() {
		$this->sess = isset($_SESSION[$this->sessKey]) ? $_SESSION[$this->sessKey] : array();
		return true;
	}

	public function getArrCount ($arr, $depth=1) {
      if (!is_array($arr) || !$depth) return 0;
        
     $res=count($arr);
        
      foreach ($arr as $in_ar)
         $res+=$this->getArrCount($in_ar, $depth-1);
     
      return $res;
  } 

}

