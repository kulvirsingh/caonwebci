<?php
/*_______________________________________________________________________
Created By	: Mrityunjay
Created On	: 20/05/2013
Modified By : 
Modified On : 
Description : This class has  function class used in both admin and user section.
_________________________________________________________________________
*/
class ad_class extends database_class
{


	function get_orderstatus_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_order_status")," and id=$id");
		return $res['name'];
	}
	
 
function get_price_by_currency($currency_from,$currency_to,$currency_input,$symbol='$'){
   /* $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
    $yql_query = 'select * from yahoo.finance.xchange where pair in ("'.$currency_from.$currency_to.'")';
    $yql_query_url = $yql_base_url . "?q=" . urlencode($yql_query);
    $yql_query_url .= "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    $yql_session = curl_init($yql_query_url);
    curl_setopt($yql_session, CURLOPT_RETURNTRANSFER,true);
    $yqlexec = curl_exec($yql_session);
    $yql_json =  json_decode($yqlexec,true);*/
    //$currency_output = (float) $currency_input*$yql_json['query']['results']['rate']['Rate'];
    $currency_output = (float) $currency_input;
    return $symbol." ".$currency_output;
}
 function get_user_name($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_user_login_table")," and id=$id ","full_name");
		return $res['full_name'];
	}

	 function get_user_email($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_user_login_table")," and id=$id ","email");
		return $res['email'];
	}

	 function get_email_by_obituary($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_tribute_users")," and tribute_id=$id  and payment_for='obituary' ",'user_id');
		$email=$this->get_user_email($res['user_id']);
		return $email;
	}

		 function get_email_by_tribute($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_tribute_users")," and tribute_id=$id  and payment_for='remembrance' ",'user_id');
		$email=$this->get_user_email($res['user_id']);
		return $email;
	}



   function get_default_currency()
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_currency")," and value='1'");
		return $res;
	}

	
	function get_attribute_by_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_category_attribute")," and id='$id'");
		return $res;
	}

	function get_cattribute_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_category_attribute")," and id='$id'",'title');
		return $res['title'];
	}


function get_event_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_event")," and id='$id'",'event_name');
		return $res['event_name'];
	}


	function get_category_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_category")," and id='$id'",'category');
		return $res['category'];
	}

	function get_product_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_product")," and id='$id'",'product_name');
		return $res['product_name'];
	}


	function get_vendor_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_shop_vendors")," and id='$id'",'vendor_name');
		return $res['vendor_name'];
	}

function get_category_by_parent($id)
	{
		$res = $this->getAnyTableAllData($this->getTable("var_category")," and parent_id=$id order by id desc");
		return $res;

	}

function get_role_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_role")," and id=$id",'role');
	return $res['role'];
	}

	function get_category_by_pid($id)
	{
		$res = $this->getAnyTableAllData($this->getTable("var_product_to_category")," and product_id=$id");
	    return $res;
	}

	function get_status_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_order_status")," and id=$id",'name');
	    return $res['name'];
	}

	function get_control_by_attr_id($id)
	{	
	      $c=$this->get_attribute_by_id($id);
	 $cid=$c['control_id'];
		$res = $this->getAnyTableWhereData2($this->getTable("var_attribute_control")," and id='$cid'",'value');
		return $res['value'];
	}
	function get_parentcategory_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_category")," and id=$id",'parent_id');
		return $res['parent_id'];
	}

function get_image_by_prop_id2($id)
	{
		$res = $this->getAnyTableAllData($this->getTable("var_product_image")," and product_id=$id LIMIT 2");

		
		return $res;
	   
	    

	}
 function get_image_by_prop_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_gallery_images")," and gallery_id=$id order by id asc ",'image_name');
		$image=str_replace('../','',$res['image_name']);
		return $image;

	}

	function get_smallimage_by_prop_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_product_image")," and product_id=$id order by id desc ",'image');
		$image=str_replace('../','',$res['image']);
		$image=str_replace('thumb_','small_',$image);
		return $image;

	}


	function get_smallimage_by_prop_id2($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_tribute")," and id=$id order by id desc ",'thumb_image');
		$image=str_replace('../','',$res['thumb_image']);
		
		return "uploads/".$image;

	}

	function get_smallimage_by_prop_id3($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_obituary")," and id=$id order by id desc ",'thumb_image');
		$image=str_replace('../','',$res['thumb_image']);
		
		return "uploads/".$image;

	}


	function get_religion_by_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_religion")," and id=$id order by id desc ",'religion_name');
		return $res['religion_name'];

	}



function get_supplyimage_by_prop_id($id)
	{
		$res = $this->getAnyTableWhereData2($this->getTable("var_product_supplier")," and id=$id ");
		
		return $res['thumb_image'];

	}


	 function get_module()
	{
		$res = $this->getAnyTableAllData($this->getTable("var_module")," ");
		return $res;
	}

	 function get_widget_position_name($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_widget_position")," and id=$id ");
		return $res['name'];
	}

 function get_brand_by_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_brand")," and id=$id ");
		return $res['brand_name'];
	}


 function get_plan_by_id($id)
	{
		$res = $this->getAnyTableWhereData($this->getTable("var_package")," and id=$id ");
		return $res['package_name'];
	}

	function get_count_of_topic_by_id($id)
	{ $zero=0;
		$res = $this->getAnyTableAllData($this->getTable("var_forum_topics")," and topic_cat=$id ");
		if(!empty($res))
		{
			return count($res);
		}
		else 
		{
			return $zero;
		}
	}

	function get_last_topic_by_id($id)
	{ $zero="No Topic Found";
		$res = $this->getAnyTableWhereData($this->getTable("var_forum_topics")," and topic_cat=$id order by id desc LIMIT 1");
		if(!empty($res))
		{
			return $res['topic_subject'];
		}
		else 
		{
			return $zero;
		}
	}




function get_role_per_by_modid($id)
	{
		$crole = $this->getAnyTableWhereData2($this->getTable("var_role")," and id=".$_SESSION["sess_role_id"]."",'permission');
        $cur_permission = unserialize(html_entity_decode(base64_decode($crole['permission'])));
    foreach($cur_permission[$id-1] as $view)
     {
			if($view==1)
			{
				$show_module=true;
			}
			else
			{
				$show_module=flase;
			}

	}

return $show_module;

}


function checkUser($uid, $oauth_provider, $username,$email,$twitter_otoken="",$twitter_otoken_secret="") 
	{	
		$res = $this->getAnyTableWhereData2($this->getTable("var_user_login_table")," and oauth_uid = '$uid' and oauth_provider = '$oauth_provider'",'id');

        if ($res['id']!="") {
           $res2 = $this->getAnyTableWhereData($this->getTable("var_user_login_table")," and oauth_uid = '$uid' and oauth_provider = '$oauth_provider'");
           return $res2;
        }
      else {
          
			$_POST['oauth_provider']=$oauth_provider;
			 $_POST['oauth_uid']=$uid;
			 $_POST['username']=$username;
			 $_POST['full_name']=$username;
			 $_POST['email']=$email;

            $this->insertData($this->getTable("var_user_login_table"));
            $res2 = $this->getAnyTableWhereData($this->getTable("var_user_login_table")," and oauth_uid = '$uid' and oauth_provider = '$oauth_provider'");
         return $res2;
 			
        }
       
    }


    
	
}
?>
