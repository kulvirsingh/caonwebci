<?php

class cart_supply extends database_class{

	protected $sess = null;			// currnet session var
	protected $sessKey = null;		// current session key for var
	protected $lastAdded = null;		// last added product


	// begin and construct
	public function __construct($cartVar) {
		$this->sessKey = $cartVar;
		$this->getSessionCart();
	}


	// destruct - unset cart var
	public function __destruct() {
		unset($this->sess);
	}


	/**
	 * Add one or more products of the same type (id)
	 * @param $id(int) - product id
	 * @param $quant(int) - quantity (remove product if $quant == 0)
	 * @param $addQuant(bool) - make add to current quantity
	 */


	public function updateProduct($id,$quant) {
		// make cast, if you have int values

		if($quant == 0) {
			unset($this->sess[$id]);
		}
		else
		{

		$id = (int)$id;
		

		$this->sess[$id]['qanty'] =$quant;
		return $this->setSessionCart();
	   }
	}


	public function addProduct($id, $quant=1, $addQuant=true) {
		// make cast, if you have int values
		$id = (int)$id;
$pro = $this->getAnyTableWhereData($this->getTable("var_product_supplier")," and status='1' and id='$id' ");   
$price=$pro['price'];

		$getFromDB_product = array (
			'price'=>$price,
			'discount'=>0,
			'weight'=>$pro['weight'],
			'title'=>$pro['product_name']		 
		);
		// if no quantity, delete product
		if($quant == 0) {
			unset($this->sess[$id]);

		// else set it
		} else {

			$this->lastAdded = $id;

			// if not yet set, add into cart
			if(!isset($this->sess[$id])) {
				$this->sess[$id] = array(
					'qanty'=>$quant,
					'price'=>$getFromDB_product['price'] , // to play, set all products price to 82.5
					'discount'=>$getFromDB_product['discount'],
					'weight'=>$getFromDB_product['weight'], // set as '%'
					'title'=>$getFromDB_product['title'],
					
				);
			// if it was set, add the quantity
			} else if($addQuant == true) {
				// for some reasons you can choose not to do this
				// so you can comment this below line
				$this->sess[$id]['qanty'] = $this->sess[$id]['qanty'] + $quant;
			}
		}
		return $this->setSessionCart();
	}


	/**
	 * Remove product
	 * @param id(int) - product ID
	 * @param $quant(int) - no. of products to remove ( 0 == all )
	 */
	public function removeProduct($id, $quant=0){

		// default, remove all products
		if($quant == 0) {
			return $this->addProduct($id, 0);

		// remove only $quant number of products
		} else {
			$currentQuant = isset($this->sess[$id]['qanty']) ? $this->sess[$id]['qanty'] : 0;
			// if current q is smaller than requested q, delete the entire product
			$productsRemaining = $currentQuant - $quant;
			if($productsRemaining <= 0) {
				return $this->removeProduct($id);
			} else {
				$this->sess[$id]['qanty'] = $productsRemaining;
				return $this->setSessionCart();
			}
		}
	}


	// This output can be into a template and whatever.
	// You have the $this->sess var so you have all you need
	public function viewCart() {
		echo '<pre>';
		print_r($_SESSION[$this->sessKey]);
		echo '</pre>';
	}


	// get no. of products from cart
	public function cartCount() {
		return count($this->sess);
	}


	// Magic area
	// ---------------

	// magic set function
	public function __set($name, $value) {
		switch ($name) {
			case 'discount':
			case 'bonusProduct':
			// you can add more new stuff here
			// for the last added product
				$this->sess[$this->lastAdded][$name] = $value;
				$this->setSessionCart();
			break;
		}
	}


	// Private area
	// ---------------

	// Set session from object [ session = object ]
	protected function setSessionCart() {
		$_SESSION[$this->sessKey] = $this->sess;
		return true;
	}

	// Get session to object [ object = session ]
	protected function getSessionCart() {
		$this->sess = isset($_SESSION[$this->sessKey]) ? $_SESSION[$this->sessKey] : array();
		return true;
	}

}

