<?php

// don't forget to start the session
session_start();
// set all errors to ON for development and to 0 (zero) when public
error_reporting(E_ALL);

// require the class
require 'class.cart.inc.php';

// set cart name [session key]
$cartName = 'basicCart';

// init the cart
$cart = new cart($cartName);

// add some product and don't add 8 products every time
$cart->addProduct(23, 8, false);
// set special discount for this product
$cart->discount = 32;

// the 'true' (3rd) arg will allow you to
// increase current quantity of product 16 (if exists) with 2
$cart->addProduct(16, 2, true);

// set special discount for this too
$cart->addProduct(53, 5, false);
$cart->discount = 35;
$cart->bonusProduct = 11;

echo 'We have '.$cart->cartCount().' products';
$cart->viewCart();