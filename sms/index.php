<!DOCTYPE html>
<html>
<head>
	<title>SMS API Integration</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<div class="jumbotron">
  <h1 class="display-3">Hello, world!</h1>

  <form action="send-sms.php" method="post">
  	
  <div class="form-group">
    <label for="exampleSelect1">Message Type</label>
    <select name="msg-type" class="form-control" id="exampleSelect1">
        <option value="1">Text Messages</option>
		<option value="2">Flash Messages</option>
		<option value="3">Unicode Messages</option>
    </select>
    </div>
    <div class="form-group">
      <label>Client's Phone Number</label>
      <label for="exampleSelect2">Message Type</label>
      <textarea name="numbers" class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>
    <div class="form-group">
      <label for="exampleTextarea">Message</label>
      <textarea name="message" class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</body>
</html>