<?php

$q = $_GET['q'];
$r = $_GET['r'];
$con = mysqli_connect('localhost','root' ,'', 'caonwebf');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

$city_filter = '';
$service_filter = ''; 

if (!isset($q) || empty($q)) {
	$city_filter = null;
} else {
	$city_filter = 'city LIKE ' . '"%' . $q . '%"';
}

if (!isset($r) || empty($r)) {
	$service_filter = null;
} else {
	$service_filter = ' AND service LIKE'. '"%' . $r . '%"';
} 



// mysqli_select_db($con,"ajax_demo");
// $sql='SELECT * FROM expert_login_table WHERE city LIKE ' . '"%' . $q . '%"' . ' AND service LIKE'. '"%' . $r . '%"'; // main query

$sql='SELECT * FROM expert_login_table WHERE ' . $city_filter . $service_filter;

$result = mysqli_query($con,$sql);

?>

<?php while($expert = mysqli_fetch_array($result)) : ?>
<!-- 	<div class="expertFilterBox"> -->
	<?php
		$fullName = $expert['name'] . $expert['last_name'];
		$slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($fullName))); 
		$services = explode(',', $expert['service']);
		if($expert['id'] != 6) {
	?>
	<div class="col-md-12 expert-section expert-search-filter" id="expertBox">
		<div class="col-md-1 expert-pics new-pad-0">
		<?php 
			$expertProfilePic = $expert['profile_photo'];
			$expertGender = strtolower($expert['gender']);
			if(empty($expertProfilePic)) {
				if($expertGender == 'male'){
					$expertProfilePic = 'avatar_default_male.png';
				}else{
					$expertProfilePic = 'avatar_default_female.png';
				}	
			}
		?>
		<img src="http://13.233.112.149/profilepic/<?= $expertProfilePic;?>">
		</div>

		<div class="col-md-8 expert-details">
			<h3><?=$expert['name'] . $expert['last_name']?></h3>
			    <p><?=$expert['eduction']?></p>
				<p><?=$expert['experience']?> Years Exp.</p>	
			<div style="clear:both;"></div>
			<div class="client-feild">
											<ul>
											<?php 
												$clientFeild = explode(',', $expert['service']);
											?>
											<?php foreach (array_slice($clientFeild, 0, 3) as $cF) :?>
												<li><?=$cF?></li>
											<?php endforeach?>
											<!-- <li>FDI Compliance</li>
											<li>Company Formation Registration</li> -->
												<!--<li style="background: none;"><a href="">View all 47 services</a></li>-->
				</ul>
			</div>
			<div class="expert-about">
					<?php if($expert['summary'] != null) :?>
				     <p> <?=substr($expert['summary'], 0, 100)?><a href="expert/reetu/6/" target="_blank" class="view-new-profile"> Read More</a></p>
					<?php endif?>
			</div>
		</div>
		<div class="col-md-3 rhs-details">
			<p><i class="fas fa-map-marker-alt"></i> <?=$expert['city'] . ' , ' . $expert['state'] . ', '. $expert['country']?></p>
				<p><a href="https://www.caonweb.com/expert/reetu/6/" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i>  2 Feedback </a></p>		
		
			<p><a href="https://www.caonweb.com/add-review.php?id=6" target="_blank" class="view-new-profile" style="color: #797979;"><i class="fas fa-comment-alt"></i> Add Reveiw </a></p>
						
					 
			<hr style="background-color: #f0f0f5; height: 0px; margin: 16px 0px 16px 0px;">
			<!--<h4>Prime <i class="fas fa-check"></i>
				<span>Max. 15 mins wait + Verified details</span>
			</h4>-->
		</div>
		<div class="col-md-12 fl-right-new">
		<a href="expert/<?= $slug . '/' . $expert['id'] ?>" target="_blank" class="view-new-profile">View Profile</a>	 
			<button type="button" class="btn btn-success po" data-toggle="popover" title="" data-content="Contact-Now - <?= $expert['phone'] ?>" data-original-title="Ext. No : <?= $expert['state_id'] ?>">Contact Now</button>

		 
			<a href="#small-dialog" data-toggle="modal" data-target=".query-ser" class="appoint" type="button" data-id="<?=$expert['id']?>" id="bookid" onclick="bookNow($(this).attr('data-id'))">Book Appointment</a>
		</div>
	</div>

<div class="col-md-12 booking-section" id="dynamic-book<?=$expert['id']?>" style="display: none;">
	<span class="successField<?=$expert['id']?> "></span>
	<h4>Please fill up the below details to confirm the appointment.</h4>
	<form class="contact-form myform" name="RegForm<?=$expert['id']?>" id="RegForm<?=$expert['id']?>" data-id="<?=$expert['id']?>" action="" onsubmit="return bookingformValidation($(this).attr('data-id'))" method="post"> 
    <div class="input-group">
		 <input type="hidden" name="exp_id" value="<?=$expert['id']?>">
		 <span><i class="fa fa-user" aria-hidden="true"></i></span>
		 <input type="text" size=65 name="name" placeholder="Name">
		 <span class="nameField<?=$expert['id']?>"></span>
	</div>   
	<div class="input-group">
		<span><i class="fa fa-envelope" aria-hidden="true"></i></span>
	    <input type="text" size=65 name="email" placeholder="email">
	    <span class="emailField<?=$expert['id']?>"></span>
	</div>
	<div class="input-group">
		<span><i class="fa fa-phone" aria-hidden="true"></i></span>
		<input type="text" name="contact" placeholder="Phone">
		<span class="contactField<?=$expert['id']?>"></span>
	</div>
	<div class="input-group">
		<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
	    <input type="text" name="appointmentdate" placeholder="Booking Date" id="flatpickr">
	    <span class="dateField<?=$expert['id']?>"></span>
	</div> 
    <div class="service-select">
		<select name="bookservice" id="selService" class="selService">
			<?php foreach ($services as $service) :?>
				<option value="<?=$service?>"><?=$service?></option>
			<?php endforeach?>
		</select>
	</div>
    <div class="textar">
		<span> <i class="fas fa-comment-alt fa"></i></span>
		<textarea name="message" style="min-height: 100px;" placeholder="Message"></textarea>
		<span class="msgField<?=$expert['id']?>"></span>
	</div>
	<div class="mb-30">
		<div class="center-holder">
			<button type="submit">SUBMIT</button>
		</div>
	</div>     
</form>
</div>
<!-- </div> -->
<?php }//endif?>
<?php endwhile ?>
<?php
mysqli_close($con);
?>
<div class="button-container text-center">
		<button class="booking-box-button btn btn-primary" id="filterMore"> Load More </button>
</div>
