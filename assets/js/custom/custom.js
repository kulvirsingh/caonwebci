function showServices(str) {
	// Search Service for Index Page

	  if (str=="") {
	    document.getElementById("selUser").innerHTML="<option> No Service Selected </option>";
	    return;
	  } 
	  if (window.XMLHttpRequest) {
	    // code for IE7+, Firefox, Chrome, Opera, Safari
	    xmlhttp=new XMLHttpRequest();
	  } else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function() {
	    if (this.readyState==4 && this.status==200) {
	      document.getElementById("selUser").innerHTML=this.responseText;
	    }
};
  xmlhttp.open("GET","http://localhost/caonweb/assets/js/custom/search.php?q="+str,true);
  xmlhttp.send();
}

function showServicesByCity(str) {
	  var service = document.getElementById('selUser').value;
	  if (str=="") {
	    document.getElementById("expert-section").innerHTML="<p> No Service Selected </p>";
	    return;
	  } 
	  if (window.XMLHttpRequest) {
	    // code for IE7+, Firefox, Chrome, Opera, Safari
	    xmlhttp=new XMLHttpRequest();
	  } else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function() {
	    if (this.readyState==4 && this.status==200) {
	      document.getElementById("default-section").innerHTML='';
	      document.getElementById("expert-section").innerHTML=this.responseText;
	    }
};
  xmlhttp.open("GET","http://localhost/caonweb/assets/js/custom/expert-search-filter.php?q="+str+"&r="+service,true);
  xmlhttp.send();
}

function showServicesByService(str) {
	  var city = document.getElementById('selCity').value;
	  if (str=="") {
	    document.getElementById("expert-section").innerHTML="<p> No Service Selected </p>";
	    return;
	  } 
	  if (window.XMLHttpRequest) {
	    // code for IE7+, Firefox, Chrome, Opera, Safari
	    xmlhttp=new XMLHttpRequest();
	  } else { // code for IE6, IE5
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function() {
	    if (this.readyState==4 && this.status==200) {
	      document.getElementById("default-section").innerHTML='';
	      document.getElementById("expert-section").innerHTML=this.responseText;
	    }
};
  xmlhttp.open("GET","http://13.233.112.149/assets/js/custom/expert-search-filter.php?q="+city+"&r="+str,true);
  xmlhttp.send();
}

function bookNow(str) {

	var id = 'dynamic-book'+str;
	document.getElementById(id).classList.toggle('show');
}

// Close the dropdown if the user clicks outside of it
// window.onclick = function(event) {
//   if (!event.target.matches('.appoint')) {
//     var dropdowns = document.getElementsByClassName("booking-section");
//     var i;
//     for (i = 0; i < dropdowns.length; i++) {
//       var openDropdown = dropdowns[i];
//       if (openDropdown.classList.contains('show')) {
//         openDropdown.classList.remove('show');
//       }
//     }
//   }
// }
